
// cek email
function validateEmail(sEmail) {
var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
		return true;
	}else {
		return false;
	}
}

//form update profile merchant
function UpdateProfileM(a,b,c){
	if(a.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Nama Merchant tidak boleh kosong !</div>');
		return false;
	}
	else if(b.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Email tidak boleh kosong  !</div>');
		return false;
	}
	else if(c.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Password tidak boleh kosong  !</div>');
		return false;
	}
}

// form produk merchant

function FormDiskon(a,b,c,d,e,f,g){
	var deskripsi = $('#deskripsi').val();
	var harga = $('#harga').val();
	
	if(a.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Nama Produk tidak boleh kosong.</div>');
		return false;
	}
	else if(b.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Harga tidak boleh kosong.</div>');
		return false;
	
	}else if(!harga.match(/\d/) ){
		
	$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Harga harus format angka.</div>');
	return false;	
	}
	else if(c.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Diskon tidak boleh kosong.</div>');
		return false;
	}
	
	else if(d.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Tanggal mulai tidak boleh kosong.</div>');
		return false;
	}
	
	else if(e.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Tanggal expired tidak boleh kosong.</div>');
		return false;
	}
	else if(deskripsi.length < 1){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Deskripsi masih kosong.</div>');
		return false;	
	}
	
	else if(f.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Status harus dipilih.</div>');
		return false;
	}
	else if(g.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Broadcast harus dipilih.</div>');
		return false;	
	}
}



function FormInfo(a,b,c){
	var isi = $('#isi').val();
	
	if(a.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Judul tidak boleh kosong.</div>');
		return false;
	}
	
	else if(b.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Status belum dipilih.</div>');
		return false;
	}
	else if(c.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Broadcast belum dipilih.</div>');
		return false;
	}
	
	
}


function FormCabang(a,b,c,d,e,f){
	var alamat = $('#alamat').val();
	var telp = $('#telp').val();
	if(a.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Nama Cabang tidak boleh kosong.</div>');
		return false;
	}
	
	else if(b.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Telp tidak boleh kosong.</div>');
		return false;
	}
	else if(!telp.match(/\d/) ){
		
	$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Telp harus format angka.</div>');
	return false;	
	}
	else if(c.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Alamat tidak boleh kosong.</div>');
		return false;
	}
	else if(alamat.length < 1){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Alamat terlalu pendek.</div>');
		return false;	
	}
	else if(d.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Latitude masih kosong.</div>');
		return false;
	}
	else if(e.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Longitude masih kosong.</div>');
		return false;
	}
	
	else if(f.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Status belum dipilih.</div>');
		return false;
	}
	
}




function FormKuis(a,b,c,d,e,f,g,h,i,j){
	var pertanyaan = $('#pertanyaan').val();
	var harga = $('#harga').val();
	
	if(a.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Judul kuis tidak boleh kosong.</div>');
		return false;
	}
	else if(b.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Pertanyaan tidak boleh kosong.</div>');
		return false;

	}
	
	
	else if(c.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Pilihan A tidak boleh kosong.</div>');
		return false;
	}
	
	else if(d.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Pilihan B tidak boleh kosong.</div>');
		return false;
	}
	
	else if(e.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Pilihan C tidak boleh kosong.</div>');
		return false;
	}
	else if(f.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Pilihan D tidak boleh kosong.</div>');
		return false;	
	}
	else if(g.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Jawaban tidak boleh kosong.</div>');
		return false;	
	}
	else if(h.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Tanggal Expired masih kosong.</div>');
		return false;	
	}
	else if(i.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Status harus dipilih.</div>');
		return false;
	}
	else if(j.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Peringatan!</strong> Broadcast harus dipilih.</div>');
		return false;	
	}
}


function FormMember(a,b,c,d,e){
	//var alamat = $('#alamat').val();
	var telp = $('#telp').val();
	var sEmail = $('#email').val();
	if(a.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Nama Member tidak boleh kosong !</div>');
		return false;
	}
	else if(b.value == '0'){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Paket belum dipilih  !</div>');
		return false;
	}	
	else if(c.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Email tidak boleh kosong  !</div>');
		return false;
	
	}
	else if (!validateEmail(sEmail)) {
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Email tidak valid !</div>');
		return false;

	}
	else if(d.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> No Telp tidak boleh kosong  !</div>');
		return false;

	}
	else if(!telp.match(/\d/) ){
		
	$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Telp harus angka !</div>');
	return false;	
	}
	else if(e.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Status belum dipilih  !</div>');
		return false;
	}

}

function FormAdmin(a,b,c,d){
	//var alamat = $('#alamat').val();
	//var telp = $('#telp').val();
	var sEmail = $('#user_email').val();
	if(a.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Nama admin tidak boleh kosong !</div>');
		return false;
	}
	else if(b.value == '0'){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Akses belum dipilih  !</div>');
		return false;
	}	
	else if(c.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Email tidak boleh kosong  !</div>');
		return false;
	
	}
	else if (!validateEmail(sEmail)) {
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Email tidak valid !</div>');
		return false;

	}
	else if(d.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Status belum dipilih  !</div>');
		return false;
	}

}


function FormMerchant(a,b,c,d){
	//var alamat = $('#alamat').val();
	var telp = $('#telp').val();
	var sEmail = $('#email').val();
	if(a.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Nama Merchant tidak boleh kosong !</div>');
		return false;
	}
	
	else if(b.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Email tidak boleh kosong  !</div>');
		return false;
	
	}
	else if (!validateEmail(sEmail)) {
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Email tidak valid !</div>');
		return false;

	}
	else if(c.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> No Telp tidak boleh kosong  !</div>');
		return false;

	}
	else if(!telp.match(/\d/) ){
		
	$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Telp harus angka !</div>');
	return false;	
	}
	else if(d.value == ''){
		$('.alert_data').show().eq(0).delay(10000).fadeOut('fast').html('<div class="alt"><div class="gmbr_alt"></div> Status belum dipilih  !</div>');
		return false;
	}

}
