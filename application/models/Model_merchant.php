<?php
Class model_merchant extends CI_Model
{
	function __constuct()
	{
		parent::__constuct();  // Call the Model constructor 
		loader::database();    // Connect to current database setting.
	}


	// menu 

	function get_menu($session)
	{

		$sql 	= "SELECT * FROM tbl_menu as a  where a.session='$session' and a.fitur='all' order by judul Asc";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

	}	


	function get_menu_costume($session,$id_merchant)
	{

		$where = "";	
		
		if($session !="")
		{		
		$where .="a.session = '$session' and a.fitur='costume' ";
		}
		if($id_merchant !="")
		{		
		$where .="and a.id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_menu as a ".$where." order by judul Asc";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

	}	

// transaksi
	function get_transaksi_6bulan($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT *,DATE_ADD(create_at, INTERVAL 180 DAY) as jatuh_tempo
				  ,DATEDIFF(DATE_ADD(create_at, INTERVAL 180 DAY), CURDATE()) as selisih
					FROM tbl_transaksi ".$where."";
		
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
	}

	function get_transaksi_12bulan($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT *,DATE_ADD(create_at, INTERVAL 360 DAY) as jatuh_tempo,DATEDIFF(DATE_ADD(create_at, INTERVAL 360 DAY), CURDATE()) as selisih FROM tbl_transaksi ".$where."";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}


// 	profile merchant

	function get_profile_merchant($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_merchant as a ".$where." order by id_merchant DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_member($id_merchant)
	{
		
		$sql 	= "SELECT count(id_member) as jml FROM tbl_member where id_merchant='$id_merchant' and status='y' ";
		
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}

	function get_jml_download($id_merchant)
	{
		
		$sql 	= "SELECT count(id_member) as jml FROM tbl_member where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	function get_jml_berita($id_merchant)
	{
		$sql 	= "SELECT count(id_berita) as jml FROM tbl_berita where id_merchant='$id_merchant'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	

	
	function get_jmlkuis($id_merchant)
	{
		$sql 	= "SELECT count(id_kuis) as jml FROM tbl_kuis where id_merchant='$id_merchant'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_katalog($id_merchant)
	{
		
		$sql 	= "SELECT count(id_katalog) as jml FROM tbl_katalog where id_merchant='$id_merchant'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_greeting($id_merchant)
	{
		
		$sql 	= "SELECT count(id_greeting) as jml FROM tbl_greeting where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_cabang($id_merchant)
	{
		
		$sql 	= "SELECT count(id_cabang) as jml FROM tbl_cabang_merchant where id_merchant='$id_merchant' and status_cabang='cabang'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_komentar($id_merchant)
	{
		
		$sql 	= "SELECT count(id_komentar) as jml FROM tbl_komentar where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}

	function get_gcm_member()
	{
	
		$sql 	= "SELECT a.gcm FROM tbl_member as a order by a.id_member DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}



// data diskon


	function get_diskon_merchant($id_merchant,$id_diskon)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_diskon !="")
		{		
		$where .="and a.id_diskon = '$id_diskon' ";
		}
				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT * FROM tbl_diskon as a 
		inner join tbl_diskon_det as b 
		on a.id_diskon=b.id_diskon  
		inner join tbl_cabang_merchant as c
		on b.id_cabang=c.id_cabang
		".$where." group  by a.id_diskon DESC";
		
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
		}
		
		
		
		
		function get_id_cabangpusat($id_merchant)
		{
		$sql 	= "SELECT a.id_cabang FROM tbl_cabang_merchant as a where id_merchant='$id_merchant' and a.status_cabang ='pusat'";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();	
		return $res;	
		}
		
		
		
		
		
	
		function get_cabang($id_diskon)
		{

		$sql 	= "SELECT b.nama_cabang FROM tbl_diskon_det as a 
		inner join tbl_cabang_merchant as b on a.id_cabang=b.id_cabang 
where a.id_diskon ='$id_diskon' ";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();	
		return $res;	
		}
	

	function get_IDDiskon_merchant($id_merchant)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}


				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT a.id_diskon,a.photo,a.nama_produk,a.harga,a.diskon,a.tgl_star,a.tgl_expired,a.status,a.deskripsi,a.create_at,b.nama_merchant FROM tbl_diskon as a inner join tbl_merchant as b on a.id_merchant=b.id_merchant ".$where." order by a.id_diskon DESC Limit 1";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_mulai']= $this->modif_create($res[$i]['tgl_star']);
			$res[$i]['tgl_selesai']= $this->modif_create($res[$i]['tgl_expired']);
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;
	}
	
	


// Data Cabang

	function get_cabang_merchant($id_merchant,$id_cabang)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' and a.status_cabang ='cabang'";
		}

		if($id_cabang !="")
		{		
		$where .="and a.id_cabang = '$id_cabang' ";
		}	
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * from tbl_cabang_merchant as a ".$where." order by a.id_cabang DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function cek_data_cabang($id_merchant)
	{
		$query=$this->db->query("select * from tbl_cabang_merchant as a where id_merchant='$id_merchant' and status_cabang='pusat'");
		return $query;
	}



// diskon cabang merchant

	function get_diskon_cabang_merchant($id_merchant,$id_diskon)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_diskon !="")
		{		
		$where .="and a.id_diskon = '$id_diskon' ";
		}
				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT 
		a.id_diskon
		,b.id_cabang
		,b.nama_cabang 
		from tbl_diskon as a
		inner join tbl_cabang_merchant as b
		on a.id_merchant=b.id_merchant
		inner join tbl_diskon_det as c 
		on a.id_diskon=c.id_diskon
		
		".$where." order by a.id_diskon DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();

		return $res;
	}


	function cek_nama_produk($nama_produk)
	{
		$query=$this->db->query("select * from tbl_diskon where nama_produk='$nama_produk'");
		return $query;
	}

	function get_diskon_merchant_limit($id_merchant)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_diskon as a  ".$where." order by a.id_diskon DESC Limit 1";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}




	function delete_diskon_merchant($id_diskon)
	{
		$where = "";			
		if($id_diskon !="")
		{		
		$where .="a.id_diskon = '$id_diskon' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_diskon as a ".$where."  order by a.id_diskon DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	


// berita

	function get_info_merchant($id_merchant,$id_berita)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_berita !="")
		{		
		$where .="and a.id_berita = '$id_berita' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_berita as a ".$where."  order by a.id_berita DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
	
		return $res;
	}	


	function get_IDBerita_merchant($id_merchant)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}


				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT a.id_berita
		,a.judul
		,a.isi
		,a.photo
		,b.nama_merchant
		,a.status
		,a.create_at FROM tbl_berita as a 
		inner join tbl_merchant as b 
		on a.id_merchant=b.id_merchant 
		".$where." order by a.id_berita DESC Limit 1";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;
	}

	function cek_judul_info($judul)
	{
		$query=$this->db->query("select * from tbl_berita where judul='$judul'");
		return $query;
	}


	function delete_info_merchant($id_berita)
	{
		$where = "";	
		
		

		if($id_berita !="")
		{		
		$where .="a.id_berita = '$id_berita' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_berita as a ".$where."  order by a.id_berita DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}



// MEMBER	

	function get_member_merchant($id_merchant,$id_member)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_member !="")
		{		
		$where .="and a.id_member = '$id_member' ";
		}
				
		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_member as a  ".$where." order by id_member DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}


	function delete_member_merchant($id_member)
	{
		$where = "";	
		
		

		if($id_berita !="")
		{		
		$where .="a.id_member = '$id_member' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_member as a ".$where."  order by a.id_member DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}

	function cek_cabang($pusat)
	{
		$query=$this->db->query("select * from tbl_cabang_merchant where status_cabang ='$pusat'");
		return $query;
	}

	function get_cabang_pusat($id_merchant,$pusat)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' and a.status_cabang ='$pusat' ";
		}

		
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.id_cabang,a.nama_cabang,b.nama_merchant,a.telp,a.alamat,b.photo,b.id_merchant,a.status,a.latitude,a.longitude,a.status_cabang FROM tbl_cabang_merchant as a inner join tbl_merchant as b on a.id_merchant=b.id_merchant ".$where." order by a.id_cabang DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}



// komentar diskon
function get_komentar_diskon($id_merchant)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="d.id_merchant = '$id_merchant' and a.param ='diskon' ";
		}

		
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT  
		a.komentar
		,a.id_diskon
		,a.publish
		,a.param
		,a.id_member
		,a.id_komentar
		,a.create_at
		,b.nama_member
		,b.photo
		,c.nama_produk
		,d.nama_merchant

		 FROM  tbl_komentar as a 
		inner join tbl_member as b 
		on a.id_member=b.id_member 
		inner join tbl_diskon as c 
		on a.id_diskon=c.id_diskon 
		inner join tbl_merchant as d 
		on c.id_merchant=d.id_merchant 
		".$where." order by a.id_komentar DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	

// komentar berita

	function get_komentar_berita($id_merchant)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="d.id_merchant = '$id_merchant' and a.param ='berita' ";
		}

				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT 
		a.komentar
		,a.id_berita
		,a.publish
		,a.param
		,a.id_member
		,a.id_komentar
		,a.create_at
		,b.nama_member
		,b.photo
		,c.judul
		,d.nama_merchant 

		FROM  tbl_komentar as a 
		inner join tbl_member as b 
		on a.id_member=b.id_member 
		inner join tbl_berita as c 
		on a.id_berita=c.id_berita 
		inner join tbl_merchant as d 
		on c.id_merchant=d.id_merchant 
		".$where."  order by a.id_komentar DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}

// 
	function get_kuis_merchant($id_merchant,$id_kuis)
	{
	
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		if($id_kuis !="")
		{		
		$where .="and a.id_kuis = '$id_kuis'";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_kuis as a ".$where." order by a.id_kuis DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	
	
	}
	
	
	
	
	
	function get_jawaban_kuis($id_merchant,$id_kuis)
	{
	
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		if($id_kuis !="")
		{		
		$where .=" and a.id_kuis = '$id_kuis'";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.id_jawaban,b.nama_member,a.create_at,a.jawaban,a.id_member,a.id_kuis,a.id_merchant FROM tbl_jawaban as a 
				inner join tbl_member as b on a.id_member=b.id_member
		".$where." order by b.nama_member DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	
	
	}
	
	
	function get_pemenang_send($id_merchant,$id_jawaban)
	{
	
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		if($id_jawaban !="")
		{		
		$where .=" and a.id_jawaban = '$id_jawaban'";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "
		SELECT 
		a.id_pemenang
		,c.judul_kuis
		,a.isi
		,a.create_at
		,d.nama_merchant
		
		from tbl_pemenang as a
		
		inner join tbl_kuis as c
		on a.id_kuis=c.id_kuis
		inner join tbl_merchant as d
		on a.id_merchant=d.id_merchant
		".$where." order by a.id_pemenang DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	
	
	}
	
	function get_pemenang_view($id_merchant,$id_kuis)
	{
	
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		if($id_kuis !="")
		{		
		$where .=" and a.id_kuis = '$id_kuis'";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "
		SELECT a.id_pemenang
		,b.photo
		,b.nama_member
		,c.judul_kuis
		,a.isi,a.create_at
		,d.nama_merchant
		from tbl_pemenang as a
		inner join tbl_member as b 
		on a.id_member=b.id_member
		inner join tbl_kuis as c
		on a.id_kuis=c.id_kuis
		inner join tbl_merchant as d
		on a.id_merchant=d.id_merchant
		".$where." order by a.id_pemenang DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	
	
	}
	
	function delete_jawaban_kuis($id_jawaban)
	{
		$where = "";	
		
		if($id_jawaban !="")
		{		
		$where .="a.id_jawaban = '$id_jawaban' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_jawaban as a ".$where."  order by a.id_jawaban DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	function delete_pemenang_kuis($id_pemenang)
	{
		$where = "";	
		
		if($id_pemenang !="")
		{		
		$where .="a.id_pemenang = '$id_pemenang' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_pemenang as a ".$where."  order by a.id_pemenang DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	
	
	
	function get_pemenang_member($id_merchant,$id_member)
	{
		
		$sql 	= "
		SELECT * FROM tbl_member as a
		where a.id_merchant='$id_merchant' 
		and a.id_member ='$id_member' 
		";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

	}
	
	
	
	function get_jml_kuis($id_merchant,$id_kuis)
	{
		$sql 	= "SELECT count(id_jawaban) as jml FROM tbl_jawaban where id_merchant='$id_merchant' and id_kuis='$id_kuis' ";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_IDKuis($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.id_kuis
				,a.a
				,a.b
				,a.c
				,a.d
				,a.judul_kuis
				,a.status
				,a.create_at
				,a.tgl_expired
				,a.pertanyaan
				,a.jawab
				,a.photo
				,b.nama_merchant 
				FROM tbl_kuis as a 
				inner join tbl_merchant as b 
				on a.id_merchant=b.id_merchant ".$where." order by a.id_kuis DESC Limit 1";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;
		
		
	}
	
	function delete_kuis_merchant($id_kuis)
	{
		$where = "";	
		
		

		if($id_kuis !="")
		{		
		$where .="a.id_kuis = '$id_kuis' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_kuis as a ".$where."  order by a.id_kuis DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}


// paket merchant

	function paket_merchant($id_merchant)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
				

		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT b.nama,b.color,b.member FROM tbl_merchant as a 
		inner join f_paket as b on a.id_paket=b.id

		".$where."  order by a.id_merchant DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	


// katalog merchant

	function get_katalog_merchant($id_merchant,$id_katalog)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
				
		if($id_katalog !="")
		{		
		$where .="and a.id_katalog = '$id_katalog' ";
		}
		
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_katalog as a ".$where."  order by a.id_katalog DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}


	function delete_katalog_merchant($id_katalog)
	{
		$where = "";	
		
		

		if($id_katalog !="")
		{		
		$where .="a.id_katalog = '$id_katalog' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_katalog as a ".$where."  order by a.id_katalog DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}

	// greeting 
	
	function get_greeting_merchant($id_greeting)
	{

		$where = "";	
		
	

		if($id_greeting !="")
		{		
		$where .="a.id_greeting = '$id_greeting'";
		}
				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT a.agama
		,a.id_greeting
		,a.judul
		,a.isi
		,a.photo
		,a.tgl_greeting
		,a.status
		,a.kategori
		,b.akses
		,a.id_merchant 
		FROM tbl_greeting as a 
		inner join tbl_merchant as b on a.id_merchant=b.id_merchant		
		".$where." order  by a.id_greeting DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
		}
		
	function delete_greeting_merchant($id_greeting)
	{
		$where = "";	
		
		

		if($id_greeting !="")
		{		
		$where .="a.id_greeting = '$id_greeting' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_greeting as a ".$where."  order by a.id_greeting DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	// greeting dasboard
	
	// dasboard greeting all
	
	
	function dasboard_greeting($id_merchant,$tgl)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_greeting as a ".$where."  order by a.id_greeting DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	
	
	function get_dasboard_ultah($id_merchant,$tanggal)
	{
		
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="b.id_merchant = '$id_merchant' and a.kategori ='ulang tahun' ";
		}
		
		if($tanggal !="")
		{		
		$where .="and b.tanggal = '$tanggal'  ";
		}
		
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT a.id_greeting,a.judul,a.kategori,a.photo,b.id_member,b.tanggal,a.tgl_greeting,a.kirim
					FROM tbl_greeting as a inner join tbl_member as b
					on a.negara=b.negara
					
					".$where." group by a.tgl_greeting
					";
		//die($sql);	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();		
		return $res;
	}
	
	// jumlah yg ultah
	
	function get_count_ultah($id_merchant,$tgl)
	{
		$sql 	= "SELECT count(id_member) as jml,nama_member FROM tbl_member where id_merchant='$id_merchant' and tanggal like '%".$tgl."%'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	function get_dasboard_agama($tgl,$id_greeting)
	{
		
		$where = "";	
		
		
		
		if($tgl !="")
		{		
		$where .="a.kategori ='religi' and a.tgl_greeting = '$tgl'  ";
		}
		
		
		if($id_greeting !="")
		{		
		$where .="and id_greeting = '$id_greeting'  ";
		}
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "
		SELECT 
		a.id_greeting
		,a.judul
		,a.kategori
		,a.photo
		,a.agama
		,a.kirim
		,a.tgl_greeting
		FROM tbl_greeting as a
		".$where."
		order by a.tgl_greeting DESC
		";
		//die($sql);	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	// jumlah yg agamanya sama
	
	function get_count_agama($id_merchant,$agama)
	{
		$sql 	= "SELECT count(id_member) as jml,nama_member FROM tbl_member where id_merchant='$id_merchant' and agama like '%".$agama."%'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_dasboard_nasional($tgl,$id_greeting)
	{
		
		$where = "";	
		
		
		
		if($tgl !="")
		{		
		$where .="a.kategori ='nasionalisme' and a.tgl_greeting = '$tgl'  ";
		}
		
		
		if($id_greeting !="")
		{		
		$where .="and id_greeting = '$id_greeting'  ";
		}
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "
		SELECT 
		a.id_greeting
		,a.judul
		,a.kategori
		,a.photo
		,a.agama
		,a.kirim
		,a.tgl_greeting
		FROM tbl_greeting as a
		".$where."
		order by a.tgl_greeting DESC
		";
		//die($sql);	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_dasboard_budaya($tgl,$id_greeting)
	{
		
		$where = "";	
		
		
		
		if($tgl !="")
		{		
		$where .="a.kategori ='budaya' and a.tgl_greeting = '$tgl'  ";
		}
		
		
		if($id_greeting !="")
		{		
		$where .="and id_greeting = '$id_greeting'  ";
		}
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "
		SELECT 
		a.id_greeting
		,a.judul
		,a.kategori
		,a.photo
		,a.agama
		,a.kirim
		,a.tgl_greeting
		FROM tbl_greeting as a
		".$where."
		order by a.tgl_greeting DESC
		";
		//die($sql);	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_chekin_terbaru($id_merchant,$tgl)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="b.id_merchant = '$id_merchant' ";
		}
		
		if($tgl !="")
		{		
		$where .="and a.create_at = '$tgl' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_cekin as a inner join tbl_member as b on a.id_member=b.id_member ".$where."  order by a.id_member DESC limit 10";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// send member
	
	function get_greeting_member($id_merchant,$agama,$tanggal)
	{	
		$where = "";
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'  ";
		}
		
		if($agama !="")
		{		
		$where .="and a.agama = '$agama'";
		}
		
		if($tanggal !="")
		{		
		$where .="and a.tanggal = '$tanggal'  ";
		}
				
		if($where!="") $where =" where ".$where;
	
		$sql 	= "
		SELECT a.nama_member,a.agama,a.tanggal,a.tgl_lahir,a.gcm	
		FROM  tbl_member as a 
		 ".$where."";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

		
	}
	
	
	function get_greeting($kategori,$id_greeting)
	{	
		$where = "";
		if($kategori !="")
		{		
		$where .="a.kategori = '$kategori'";
		}
		
		if($id_greeting !="")
		{		
		$where .="and a.id_greeting = '$id_greeting'";
		}
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT * FROM tbl_greeting as a ".$where."";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;

	}
	
	
	
	
	function get_IDgretting_merchant($date)
	{	
		$sql 	= "SELECT * FROM tbl_greeting where tgl_greeting = '$date' ";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

		
	}
	
	
	
	
	
	
	
	
	function get_member_terbaru($id_merchant)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
	
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_member as a ".$where."  order by a.create_at DESC limit 9";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_member_chekin($id_merchant)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="b.id_merchant = '$id_merchant' ";
		}
		
	
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.create_at,b.nama_member,c.nama_cabang,b.photo FROM tbl_cekin as a
					inner join tbl_member as b
					on a.id_member=b.id_member
					inner join tbl_cabang_merchant as c
					on a.id_cabang=c.id_cabang
		".$where."  order by a.create_at DESC limit 9";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_member_komentar($id_merchant)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="c.id_merchant = '$id_merchant' ";
		}
		
	
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.create_at,b.nama_member,b.photo,a.komentar FROM tbl_komentar as a
					inner join tbl_member as b
					on a.id_member=b.id_member
					inner join tbl_merchant as c
					on a.id_merchant=c.id_merchant
		".$where."  order by a.create_at DESC limit 9";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	function get_testimoni()
	{

		$sql 	= "SELECT * FROM f_testimoni as a order by a.create_at DESC limit 10";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	
	function get_survey($id_merchant,$id_survey)
	{
		
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_survey !="")
		{		
			$where .="and a.id_survey = '$id_survey' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_survey as a ".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_pertanyaan($id_merchant,$id_survey,$id_polling)
	{
		
		$where = "";	
		
		

		if($id_merchant !="")
		{		
			$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_survey !="")
		{		
			$where .="and a.id_survey = '$id_survey' ";
		}
	
		if($id_polling !="")
		{		
			$where .="and a.id_polling = '$id_polling' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_polling as a ".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_jawaban_survey($id_pertanyaan)
	{
		
		$where = "";	

		
		if($id_pertanyaan !="")
		{		
		$where .="a.id_pertanyaan = '$id_pertanyaan'";
		}
		
		
		
		
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT b.id_jawaban,b.jawaban FROM  tbl_survey_pertanyaan as a inner join tbl_survey_jawaban as b on a.id_pertanyaan=b.id_pertanyaan ".$where." order by b.jawaban DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
	}
	
	
	function get_jawaban_det($id_merchant,$id_jawaban)
	{
		
		$where = "";
		
		if($id_merchant !="")
		{		
		$where .=" a.id_merchant = '$id_merchant' ";
		}
		

		if($id_jawaban !="")
		{		
		$where .="and c.id_jawaban = '$id_jawaban' ";
		}

		
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_survey as a  
		inner join tbl_survey_pertanyaan as b 
		on a.id_survey=b.id_survey 
		inner join tbl_survey_jawaban as c 
		on b.id_pertanyaan=c.id_pertanyaan 
		".$where." order by c.jawaban DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_hasil_survey($id_merchant,$id_survey)
	{
		
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_survey !="")
		{		
			$where .="and a.id_survey = '$id_survey' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT 
		b.nama_member
		,c.pertanyaan
		,c.que_a
		,c.que_b
		,c.que_c
		,c.que_d
		,c.que_e 
		,c.opt_a
		,c.opt_b
		,c.opt_c
		,c.opt_d
		,c.opt_e
		
		FROM tbl_survey_hasil as a 
		inner join tbl_member as b on a.id_member=b.id_member
		inner join tbl_polling as c
		on a.id_polling=c.id_polling	
		
		
		".$where." group by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
	}
	
	
	
	function get_pertanyaan_polling($id_merchant,$id_survey)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_survey !="")
		{		
			$where .="and a.id_survey = '$id_survey'";
		}
		
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_polling as a ".$where." order by a.id_polling DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function cek_survey($id_merchant,$id_survey)
	{
		$query=$this->db->query("select * from tbl_polling
		where id_merchant='$id_merchant' 
		and id_survey='$id_survey' 
		and opt_a ='0'
		and opt_b ='0'
		and opt_c ='0'
		and opt_d ='0'	
		");
		return $query;
	}
	
	

// hapus data

	function hapus_konten($id,$seleksi,$tabel)
	{
		$this->db->where($seleksi,$id);
		$this->db->delete($tabel);
	}



// convert tgl per detik

function dateDiff($date)
	{
  $mydate= date("Y-m-d H:i:s");
  $theDiff="";
  //echo $mydate;//2014-06-06 21:35:55
  $datetime1 = date_create($date);
  $datetime2 = date_create($mydate);
  $interval = date_diff($datetime1, $datetime2);
  //echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>";
  $min=$interval->format('%i');
  $sec=$interval->format('%s');
  $hour=$interval->format('%h');
  $mon=$interval->format('%m');
  $day=$interval->format('%d');
  $year=$interval->format('%y');
  if($interval->format('%i%h%d%m%y')=="00000")
  {
    //echo $interval->format('%i%h%d%m%y')."<br>";
    return $sec." Detik yang lalu";

  } 

else if($interval->format('%h%d%m%y')=="0000"){
   return $min." Menit yang lalu";
   }


else if($interval->format('%d%m%y')=="000"){
   return $hour." Jam yang lalu";
   }


else if($interval->format('%m%y')=="00"){
   return $day." Hari yang lalu";
   }

else if($interval->format('%y')=="0"){
   return $mon." Bulan";
   }



else{
   return $year." Tahun";
   }

}	


// convert by tanggal hari tahun

function modif_tanggal($tgl, $hari_tampil = true)
	{
		$bulan	= array("Jan"
						, "Feb"
						, "Mar"
						, "Apr"
						, "Mei"
						, "Jun"
						, "Jul"
						, "Agu"
						, "Sep"
						, "Okt"
						, "Nov"
						, "Des");
		$hari	= array("Senin"
						, "Selasa"
						, "Rabu"
						, "Kamis"
						, "Jum'at"
						, "Sabtu"
						, "Minggu");
		$tahun_split	= substr($tgl, 0, 4);
		$bulan_split	= substr($tgl, 5, 2);
		$hari_split		= substr($tgl, 8, 2);
		$tmpstamp		= mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
		$bulan_jadi 	= $bulan[date("n", $tmpstamp)-1];
		$hari_jadi 		= $hari[date("N", $tmpstamp)-1];
		if(!$hari_tampil)
		$hari_jadi="";
		return $hari_jadi." ".$hari_split." ".$bulan_jadi." ".$tahun_split;
	}
	
	
// modif nama

	function convert_name($title)
	{
		$title 	= str_replace(array(" ", "'", ",", "[", "]", "?", "!"), "-", $title);
		return strtolower($title);
	}


// tanggal versi indonesia
function modif_create($tgl, $hari_tampil = true)
	{
		$bulan	= array("Jan"
						, "Feb"
						, "Mar"
						, "Apr"
						, "Mei"
						, "Jun"
						, "Jul"
						, "Agu"
						, "Sep"
						, "Okt"
						, "Nov"
						, "Des");
		$hari	= array("Senin"
						, "Selasa"
						, "Rabu"
						, "Kamis"
						, "Jum'at"
						, "Sabtu"
						, "Minggu");
		$tahun_split	= substr($tgl, 0, 4);

		$y 				= strtotime($tgl);
		$year			= date("y", $y);
		$bulan_split	= substr($tgl, 5, 2);
		$hari_split		= substr($tgl, 8, 2);
		$tmpstamp		= mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
		$bulan_jadi 	= $bulan[date("n", $tmpstamp)-1];
		$hari_jadi 		= $hari[date("N", $tmpstamp)-1];
		if(!$hari_tampil)
		$hari_jadi="";
		return $hari_split." ".$bulan_jadi;
	}
	
	function dateExpired($date)
	{
  $mydate= date("Y-m-d H:i:s");
  $theDiff="";
  //echo $mydate;//2014-06-06 21:35:55
  $datetime1 = date_create($date);
  $datetime2 = date_create($mydate);
  $interval = date_diff($datetime1, $datetime2);
  //echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>";
  $min=$interval->format('%i');
  $sec=$interval->format('%s');
  $hour=$interval->format('%h');
  $mon=$interval->format('%m');
  $day=$interval->format('%d');
  $year=$interval->format('%y');
  if($interval->format('%i%h%d%m%y')=="00000")
  {
    //echo $interval->format('%i%h%d%m%y')."<br>";
    return $sec." Detik yang lalu";

  } 

else if($interval->format('%h%d%m%y')=="0000"){
   return $min." Menit yang lalu";
   }


else if($interval->format('%d%m%y')=="000"){
   return $hour." Jam yang lalu";
   }


else if($interval->format('%m%y')=="00"){
   return $day." Hari yang lalu";
   }

else if($interval->format('%y')=="0"){
   return $mon." Bulan";
   }



else{
   return $year." Tahun";
   }

}	


}
 
?>
