<?php
Class model_developer extends CI_Model
{
	function __constuct()
	{
		parent::__constuct();  // Call the Model constructor 
		loader::database();    // Connect to current database setting.
	}

	function get_menu($session)
	{

		$sql 	= "SELECT * FROM tbl_menu as a  where a.session='$session' and a.fitur='all' order by a.menu_optional Asc";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

	}	
	
	function get_merchant($id_merchant,$limit)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * from tbl_merchant as a ".$where." order by a.id_merchant DESC $limit";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	

	

	function get_cabang_det($id_merchant)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * from tbl_cabang_merchant as a ".$where." order by a.id_merchant DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}

	function get_notifikasi($id_merchant)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}
	
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT b.id_merchant,a.id_notifikasi,b.nama_merchant,a.notifikasi,a.deskripsi,a.create_at,a.status from tbl_notifikasi as a inner join tbl_merchant as b on a.id_merchant=b.id_merchant ".$where." order by a.id_merchant DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}

	

	function delete_merchant($id_merchant)
	{
		$where = "";	
		
		

		if($id_asset !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_merchant as a ".$where."  order by a.id_merchant DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_greeting_developer($id_admin)
	{

		$where = "";	
		
	

		if($id_admin !="")
		{		
		$where .="a.id_admin = '$id_admin'";
		}
				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT * FROM tbl_greeting as a 
		
		".$where." order  by a.id_greeting DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
		}
		
	function delete_greeting_developer($id_greeting)
	{
		$where = "";	
		
		

		if($id_greeting !="")
		{		
		$where .="a.id_greeting = '$id_greeting' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_greeting as a ".$where."  order by a.id_greeting DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	

	function cek_email_merchant($email)
	{
		$query=$this->db->query("select * from tbl_merchant where email ='$email'");
		return $query;
	}



// hapus data

	function hapus_konten($id,$seleksi,$tabel)
	{
		$this->db->where($seleksi,$id);
		$this->db->delete($tabel);
	}



// convert tgl per detik

function dateDiff($date)
	{
  $mydate= date("Y-m-d H:i:s");
  $theDiff="";
  //echo $mydate;//2014-06-06 21:35:55
  $datetime1 = date_create($date);
  $datetime2 = date_create($mydate);
  $interval = date_diff($datetime1, $datetime2);
  //echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>";
  $min=$interval->format('%i');
  $sec=$interval->format('%s');
  $hour=$interval->format('%h');
  $mon=$interval->format('%m');
  $day=$interval->format('%d');
  $year=$interval->format('%y');
  if($interval->format('%i%h%d%m%y')=="00000")
  {
    //echo $interval->format('%i%h%d%m%y')."<br>";
    return $sec." Detik yang lalu";

  } 

else if($interval->format('%h%d%m%y')=="0000"){
   return $min." Menit yang lalu";
   }


else if($interval->format('%d%m%y')=="000"){
   return $hour." Jam yang lalu";
   }


else if($interval->format('%m%y')=="00"){
   return $day." Hari yang lalu";
   }

else if($interval->format('%y')=="0"){
   return $mon." Bulan";
   }



else{
   return $year." Tahun";
   }

}	


// convert by tanggal hari tahun

function modif_tanggal($tgl, $hari_tampil = true)
	{
		$bulan	= array("Jan"
						, "Feb"
						, "Mar"
						, "Apr"
						, "Mei"
						, "Jun"
						, "Jul"
						, "Agu"
						, "Sep"
						, "Okt"
						, "Nov"
						, "Des");
		$hari	= array("Senin"
						, "Selasa"
						, "Rabu"
						, "Kamis"
						, "Jum'at"
						, "Sabtu"
						, "Minggu");
		$tahun_split	= substr($tgl, 0, 4);
		$bulan_split	= substr($tgl, 5, 2);
		$hari_split		= substr($tgl, 8, 2);
		$tmpstamp		= mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
		$bulan_jadi 	= $bulan[date("n", $tmpstamp)-1];
		$hari_jadi 		= $hari[date("N", $tmpstamp)-1];
		if(!$hari_tampil)
		$hari_jadi="";
		return $hari_jadi." ".$hari_split." ".$bulan_jadi." ".$tahun_split;
	}
	
	
// modif nama

	function convert_name($title)
	{
		$title 	= str_replace(array(" ", "'", ",", "[", "]", "?", "!"), "-", $title);
		return strtolower($title);
	}


// tanggal versi indonesia
function modif_create($tgl, $hari_tampil = true)
	{
		$bulan	= array("Jan"
						, "Feb"
						, "Mar"
						, "Apr"
						, "Mei"
						, "Jun"
						, "Jul"
						, "Agu"
						, "Sep"
						, "Okt"
						, "Nov"
						, "Des");
		$hari	= array("Senin"
						, "Selasa"
						, "Rabu"
						, "Kamis"
						, "Jum'at"
						, "Sabtu"
						, "Minggu");
		$tahun_split	= substr($tgl, 0, 4);

		$y 				= strtotime($tgl);
		$year			= date("y", $y);
		$bulan_split	= substr($tgl, 5, 2);
		$hari_split		= substr($tgl, 8, 2);
		$tmpstamp		= mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
		$bulan_jadi 	= $bulan[date("n", $tmpstamp)-1];
		$hari_jadi 		= $hari[date("N", $tmpstamp)-1];
		if(!$hari_tampil)
		$hari_jadi="";
		return $hari_split." ".$bulan_jadi;
	}

 
}
 
?>
