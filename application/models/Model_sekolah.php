<?php
Class model_sekolah extends CI_Model
{
	function __constuct()
	{
		parent::__constuct();  // Call the Model constructor 
		loader::database();    // Connect to current database setting.
	}


	// menu 
	
	
	function get_dcalendary($id_merchant,$id_cabang)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="id_merchant = '$id_merchant' ";
		}
		
		if($id_cabang !="")
		{		
		$where .=" and id_cabang = '$id_cabang' ";
		}
		
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT judul as title,tgl_mulai as start,tgl_selesai as end FROM tbl_kalender 
		
		".$where." ";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
		
	}

	function get_menu($session)
	{

		$sql 	= "SELECT * FROM tbl_menu as a  where a.session='$session' and a.fitur='all' order by a.menu_optional Asc";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

	}	


	function get_menu_costume($session,$id_merchant)
	{

		$where = "";	
		
		if($session !="")
		{		
		$where .="a.session = '$session' and a.fitur='costume' ";
		}
		if($id_merchant !="")
		{		
		$where .="and a.id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_menu as a ".$where." order by judul Asc";
		
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

	}	

// transaksi
	function get_transaksi_6bulan($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT *,DATE_ADD(create_at, INTERVAL 180 DAY) as jatuh_tempo
				  ,DATEDIFF(DATE_ADD(create_at, INTERVAL 180 DAY), CURDATE()) as selisih
					FROM tbl_transaksi ".$where."";
		
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
	}

	function get_transaksi_12bulan($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT *,DATE_ADD(create_at, INTERVAL 360 DAY) as jatuh_tempo,DATEDIFF(DATE_ADD(create_at, INTERVAL 360 DAY), CURDATE()) as selisih FROM tbl_transaksi ".$where."";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}


// 	profile merchant

	function get_profile_merchant($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_merchant as a ".$where." order by id_merchant DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_member($id_merchant)
	{
		
		$sql 	= "SELECT count(id_member) as jml FROM tbl_member where id_merchant='$id_merchant' and status='y' ";
		
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}

	function get_jml_ekskul($id_merchant)
	{
		
		$sql 	= "SELECT count(id_ekskul) as jml FROM tbl_ekskul where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	function get_jml_berita($id_merchant)
	{
		$sql 	= "SELECT count(id_berita) as jml FROM tbl_berita  where id_merchant='$id_merchant' and tipe='headline_news'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_jml_kalender($id_merchant)
	{
		$sql 	= "SELECT count(id_kalender) as jml FROM tbl_kalender where id_merchant='$id_merchant'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}

	function get_jml_notifikasi($id_merchant)
	{
		
		$sql 	= "SELECT count(id_notif) as jml FROM tbl_notif_direktorat where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_direktorat($id_merchant)
	{
		
		$sql 	= "SELECT count(id_berita) as jml FROM tbl_berita where id_merchant='$id_merchant' and tipe='direktorat'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_divisi($id_merchant)
	{
		
		$sql 	= "SELECT count(id_cabang) as jml FROM tbl_cabang_merchant where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_jml_greeting($id_merchant)
	{
		
		$sql 	= "SELECT count(id_greeting) as jml FROM tbl_greeting where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_survey($id_merchant)
	{
		
		$sql 	= "SELECT count(id_survey) as jml FROM tbl_survey where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_kepegawaian($id_merchant)
	{
		
		$sql 	= "SELECT count(id_pegawai) as jml FROM tbl_pegawai where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	function get_jml_mapel($id_merchant)
	{
		
		$sql 	= "SELECT count(id_mapel) as jml FROM tbl_mapel where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_devisi_unit($id_merchant)
	{
		
		$sql 	= "SELECT * From tbl_cabang_merchant where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_jml_anak($id_merchant)
	{
		
		$sql 	= "SELECT count(id_anak) as jml FROM tbl_anak where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_undangan($id_merchant)
	{
		
		$sql 	= "SELECT count(id_undangan) as jml FROM tbl_undangan where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_jml_prestasi_sek($id_merchant)
	{
		
		$sql 	= "SELECT count(id_prestasi) as jml FROM tbl_prestasi_sekolah where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_jml_prestasi_alumni($id_merchant)
	{
		
		$sql 	= "SELECT count(id_prestasi) as jml FROM tbl_prestasi_alumni where id_merchant='$id_merchant'";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	

	function get_gcm_member($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($where!="") $where =" where ".$where;
		
	
		$sql 	= "SELECT a.gcm,a.nama_member FROM tbl_member as a ".$where." order by a.id_member DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	// pengumuman
	// orang tua
	
	function get_orangtua($id_sekolah,$id_member)
	{
		$where = "";	
		
		if($id_sekolah !="")
		{		
		$where .="a.id_merchant = '$id_sekolah' ";
		}
		
		if($id_member !="")
		{		
		$where .="and a.id_member = '$id_member' ";
		}
		
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT * FROM tbl_member as a 
		
		".$where." order  by a.id_member DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
		
	}
	
	function get_divisi($id_merchant,$id_divisi)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_divisi !="")
		{		
		$where .="and a.id_cabang = '$id_divisi' ";
		}
		
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT * FROM tbl_cabang_merchant as a 
		
		".$where." order  by a.id_cabang DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
		
	}
	
	
	function get_kehadiran($id_sekolah,$id_member)
	{
		$where = "";	
		
		if($id_sekolah !="")
		{		
		$where .="a.id_merchant = '$id_sekolah' ";
		}
		
		if($id_member !="")
		{		
		$where .="and a.id_member = '$id_member' ";
		}
		
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT a.id_kehadiran,b.photo,b.nama_member,c.judul,a.status FROM tbl_konf_kehadiran as a 
					inner join tbl_member as b
					on a.id_member=b.id_member
					inner join tbl_undangan as c 
					on a.id_undangan=c.id_undangan
					
		".$where." order  by b.id_member DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
		
	}
	
	function get_kelas($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_kelas as a 
		
		".$where." order  by a.id_kelas DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
		
	}
	
	function get_mapel($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_mapel as a 
		
		".$where." order  by a.id_mapel DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
		
	}
	
	// send gcm kelas
	function get_kelas_anak($id_merchant,$id_kelas)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_kelas !="")
		{		
		$where .="and a.id_kelas = '$id_kelas' ";
		}
		
		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT a.id_merchant,a.nama_anak,a.id_member,a.id_kelas,b.nama_member,b.gcm FROM tbl_anak as a
					left join tbl_member as b on a.id_member=b.id_member 
		".$where." order  by a.id_anak DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
		
	}
	
	
	
	
	
	function get_IDkelas($id_kelas)
	{
			
		$where = "";	
	
		if($id_kelas !="")
		{		
		$where .="a.id_kelas = '$id_kelas'";
		}
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT * FROM tbl_kelas as a 
				  ".$where." order by a.id_kelas DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result();
		return $res;
	}
		

	
	
	// ekskul

	function get_ekskul_sekolah($id_sekolah,$id_ekskul)
	{

		$where = "";	
		
		if($id_sekolah !="")
		{		
		$where .="a.id_merchant = '$id_sekolah' ";
		}

		if($id_ekskul !="")
		{		
		$where .="and a.id_ekskul = '$id_ekskul' ";
		}
				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT * FROM tbl_ekskul as a 
		
		".$where." order  by a.id_ekskul DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
		}
		
		function get_IDekskul_sekolah($id_sekolah)
		{

		$where = "";	
		
		if($id_sekolah !="")
		{		
		$where .="a.id_merchant = '$id_sekolah' ";
		}


				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT a.id_ekskul,a.photo,a.judul,a.status,a.deskripsi,a.create_at,b.nama_merchant FROM tbl_ekskul as a inner join tbl_merchant as b on a.id_merchant=b.id_merchant ".$where." order by a.id_ekskul DESC Limit 1";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;
		}

	
		function delete_ekskul_sekolah($id_ekskul)
		{
		$where = "";			
		if($id_ekskul !="")
		{		
		$where .="a.id_ekskul = '$id_ekskul' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_ekskul as a ".$where."  order by a.id_ekskul DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
		}

	

	// greeting 
	
	function get_greeting_sekolah($id_greeting)
	{

		$where = "";	
		
	

		if($id_greeting !="")
		{		
		$where .="a.id_greeting = '$id_greeting'";
		}
				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT a.agama
		,a.id_greeting
		,a.judul
		,a.isi
		,a.photo
		,a.tgl_greeting
		,a.status
		,a.kategori
		,a.id_admin
		,a.id_merchant 
		FROM tbl_greeting as a 
				
		".$where." order  by a.id_greeting DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
		}
		
	function delete_greeting_sekolah($id_greeting)
	{
		$where = "";	
		
		

		if($id_greeting !="")
		{		
		$where .="a.id_greeting = '$id_greeting' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_greeting as a ".$where."  order by a.id_greeting DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	// greeting dasboard
	
	// dasboard greeting all
	
	
	function dasboard_greeting($id_merchant,$tgl)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_greeting as a ".$where."  order by a.id_greeting DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	
	
	function get_dasboard_ultah($id_merchant,$tanggal)
	{
		
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="b.id_merchant = '$id_merchant' and a.kategori ='ulang tahun' ";
		}
		
		if($tanggal !="")
		{		
		$where .="and b.tanggal = '$tanggal'  ";
		}
		
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT a.id_greeting,a.judul,a.kategori,a.photo,b.id_member,b.tanggal,a.tgl_greeting,a.kirim
					FROM tbl_greeting as a inner join tbl_member as b
					on a.negara=b.negara
					
					".$where." group by a.tgl_greeting
					";
		//die($sql);	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();		
		return $res;
	}
	
	// jumlah yg ultah
	
	function get_count_ultah($id_merchant,$tgl)
	{
		$sql 	= "SELECT count(id_member) as jml,nama_member FROM tbl_member where id_merchant='$id_merchant' and tanggal like '%".$tgl."%'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	function get_dasboard_agama($tgl,$id_greeting)
	{
		
		$where = "";	
		
		
		
		if($tgl !="")
		{		
		$where .="a.kategori ='religi' and a.tgl_greeting = '$tgl'  ";
		}
		
		
		if($id_greeting !="")
		{		
		$where .="and id_greeting = '$id_greeting'  ";
		}
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "
		SELECT 
		a.id_greeting
		,a.judul
		,a.kategori
		,a.photo
		,a.agama
		,a.kirim
		,a.tgl_greeting
		FROM tbl_greeting as a
		".$where."
		order by a.tgl_greeting DESC
		";
		//die($sql);	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	// jumlah yg agamanya sama
	
	function get_count_agama($id_merchant,$agama)
	{
		$sql 	= "SELECT count(id_member) as jml,nama_member FROM tbl_member where id_merchant='$id_merchant' and agama like '%".$agama."%'";
	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_dasboard_nasional($tgl,$id_greeting)
	{
		
		$where = "";	
		
		
		
		if($tgl !="")
		{		
		$where .="a.kategori ='nasionalisme' and a.tgl_greeting = '$tgl'  ";
		}
		
		
		if($id_greeting !="")
		{		
		$where .="and id_greeting = '$id_greeting'  ";
		}
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "
		SELECT 
		a.id_greeting
		,a.judul
		,a.kategori
		,a.photo
		,a.agama
		,a.kirim
		,a.tgl_greeting
		FROM tbl_greeting as a
		".$where."
		order by a.tgl_greeting DESC
		";
		//die($sql);	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_dasboard_budaya($tgl,$id_greeting)
	{
		
		$where = "";	
		
		
		
		if($tgl !="")
		{		
		$where .="a.kategori ='budaya' and a.tgl_greeting = '$tgl'  ";
		}
		
		
		if($id_greeting !="")
		{		
		$where .="and id_greeting = '$id_greeting'  ";
		}
		
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "
		SELECT 
		a.id_greeting
		,a.judul
		,a.kategori
		,a.photo
		,a.agama
		,a.kirim
		,a.tgl_greeting
		FROM tbl_greeting as a
		".$where."
		order by a.tgl_greeting DESC
		";
		//die($sql);	
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_chekin_terbaru($id_merchant,$tgl)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="b.id_merchant = '$id_merchant' ";
		}
		
		if($tgl !="")
		{		
		$where .="and a.create_at = '$tgl' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_cekin as a inner join tbl_member as b on a.id_member=b.id_member ".$where."  order by a.id_member DESC limit 10";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	
	
	function get_info_sekolah($id_merchant,$id_berita)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' and tipe='headline_news' ";
		}

		if($id_berita !="")
		{		
		$where .="and a.id_berita = '$id_berita' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_berita as a ".$where."  order by a.id_berita DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
	
		return $res;
	}	


	function get_IDBerita_sekolah($id_merchant,$id_berita)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_berita !="")
		{		
		$where .="and a.id_berita = '$id_berita' ";
		}
				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT a.id_berita
		,a.judul
		,a.isi
		,a.photo
		,b.nama_merchant
		,a.status
		,a.create_at FROM tbl_berita as a 
		inner join tbl_merchant as b 
		on a.id_merchant=b.id_merchant 
		".$where." order by a.id_berita DESC Limit 1";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;
	}

	function cek_judul_info($judul)
	{
		$query=$this->db->query("select * from tbl_berita where judul='$judul'");
		return $query;
	}
	
	function cek_no_induk($id_merchant,$no_induk)
	{
		$query=$this->db->query("select * from tbl_anak where id_merchant='$id_merchant' and nis ='$no_induk'");
		return $query;
	}
	
	
	function cek_kode_news($id_merchant,$kode)
	{
		$query=$this->db->query("select * from tbl_berita where id_merchant='$id_merchant' and kode ='$kode'");
		return $query;
	}

	function delete_info_sekolah($id_berita)
	{
		$where = "";	
		
		

		if($id_berita !="")
		{		
		$where .="a.id_berita = '$id_berita' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_berita as a ".$where."  order by a.id_berita DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_direktorat($id_merchant,$id_berita)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' and a.tipe='direktorat' ";
		}

		if($id_berita !="")
		{		
		$where .="and a.id_berita = '$id_berita' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_berita as a ".$where."  order by a.id_berita DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create'] = $this->tgl_bln_thn($res[$i]['create_at']);
			
		}
		return $res;
	}	
	
	function get_notifikasi_direktorat($id_merchant,$id_notif,$limit)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		if($id_notif !="")
		{		
		$where .="and a.id_notif = '$id_notif' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_notif_direktorat as a inner join tbl_merchant as b on a.id_merchant=b.id_merchant ".$where."  order by a.id_notif DESC $limit";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
	
		return $res;
	}
	
	
	function get_IDnotifikasi_direktorat($id_merchant,$id_notif)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}

		if($id_notif !="")
		{		
		$where .="and a.id_notif = '$id_notif' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.id_notif,a.judul,a.isi,a.tgl_mulai as tgl_ml,a.set_waktu,a.create_at,b.nama_merchant,a.status FROM tbl_notif_direktorat as a inner join tbl_merchant as b on a.id_merchant=b.id_merchant ".$where."  order by a.id_notif DESC limit 1";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create'] = $this->modif_create($res[$i]['create_at']);
			$res[$i]['tgl_mulai'] = $this->modif_create($res[$i]['tgl_ml']);
		}
		return $res;
	}
	
	
	
	function get_kalender_sekolah($id_merchant,$id_kalender)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_kalender !="")
		{		
		$where .="and a.id_kalender = '$id_kalender' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_kalender as a ".$where."  order by a.id_kalender DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['description']= strip_tags($res[$i]['deskripsi'],'<p>');
			
		}
		return $res;
	}	
	
	
	
	
	function removeTag($str, $tag)
	{ 
	$str = preg_replace("#\<".$tag."(.*)/".$tag.">#iUs", "", $str);
	return $str;
	}
	
	
	function strip_only($str, $tags, $stripContent = false) {
    $content = '';
    if(!is_array($tags)) {
        $tags = (strpos($str, '>') !== false
                 ? explode('>', str_replace('<', '', $tags))
                 : array($tags));
        if(end($tags) == '') array_pop($tags);
    }
    foreach($tags as $tag) {
        if ($stripContent)
             $content = '(.+</'.$tag.'[^>]*>|)';
         $str = preg_replace('#</?'.$tag.'[^>]*>'.$content.'#is', '', $str);
    }
    return $str;
}


	
	
	// send member
	
	function get_greeting_member($id_merchant,$agama,$tanggal)
	{	
		$where = "";
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'  ";
		}
		
		if($agama !="")
		{		
		$where .="and a.agama = '$agama'";
		}
		
		if($tanggal !="")
		{		
		$where .="and a.tanggal = '$tanggal'  ";
		}
				
		if($where!="") $where =" where ".$where;
	
		$sql 	= "
		SELECT a.nama_member,a.agama,a.tanggal,a.tgl_lahir,a.gcm	
		FROM  tbl_member as a 
		 ".$where."";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

		
	}
	
	
	function get_greeting($kategori,$id_greeting)
	{	
		$where = "";
		if($kategori !="")
		{		
		$where .="a.kategori = '$kategori'";
		}
		
		if($id_greeting !="")
		{		
		$where .="and a.id_greeting = '$id_greeting'";
		}
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT * FROM tbl_greeting as a ".$where."";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;

	}
	
	
	
	
	function get_IDgretting_merchant($date)
	{	
		$sql 	= "SELECT * FROM tbl_greeting where tgl_greeting = '$date' ";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;

		
	}
	
	
	
	// paket merchant

	function paket_merchant($id_merchant)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
				

		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT b.nama,b.color,b.member FROM tbl_merchant as a 
		inner join f_paket as b on a.id_paket=b.id

		".$where."  order by a.id_merchant DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	
	
	
	function get_member_terbaru($id_merchant)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
	
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_member as a ".$where."  order by a.create_at DESC limit 9";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_member_chekin($id_merchant)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="b.id_merchant = '$id_merchant' ";
		}
		
	
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.create_at,b.nama_member,c.nama_cabang,b.photo FROM tbl_cekin as a
					inner join tbl_member as b
					on a.id_member=b.id_member
					inner join tbl_cabang_merchant as c
					on a.id_cabang=c.id_cabang
		".$where."  order by a.create_at DESC limit 9";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_member_komentar($id_merchant)
	{
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="c.id_merchant = '$id_merchant' ";
		}
		
	
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.create_at,b.nama_member,b.photo,a.komentar FROM tbl_komentar as a
					inner join tbl_member as b
					on a.id_member=b.id_member
					inner join tbl_merchant as c
					on a.id_merchant=c.id_merchant
		".$where."  order by a.create_at DESC limit 9";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	function get_testimoni()
	{

		$sql 	= "SELECT * FROM f_testimoni as a order by a.create_at DESC limit 10";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	
	function get_survey($id_merchant,$id_survey)
	{
		
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_survey !="")
		{		
			$where .="and a.id_survey = '$id_survey' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_survey as a ".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_pertanyaan($id_merchant,$id_survey,$id_polling)
	{
		
		$where = "";	
		
		

		if($id_merchant !="")
		{		
			$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_survey !="")
		{		
			$where .="and a.id_survey = '$id_survey' ";
		}
	
		if($id_polling !="")
		{		
			$where .="and a.id_polling = '$id_polling' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_polling as a ".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_jawaban_survey($id_pertanyaan)
	{
		
		$where = "";	

		
		if($id_pertanyaan !="")
		{		
		$where .="a.id_pertanyaan = '$id_pertanyaan'";
		}
		
		
		
		
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT b.id_jawaban,b.jawaban FROM  tbl_survey_pertanyaan as a inner join tbl_survey_jawaban as b on a.id_pertanyaan=b.id_pertanyaan ".$where." order by b.jawaban DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
	}
	
	
	function get_jawaban_det($id_merchant,$id_jawaban)
	{
		
		$where = "";
		
		if($id_merchant !="")
		{		
		$where .=" a.id_merchant = '$id_merchant' ";
		}
		

		if($id_jawaban !="")
		{		
		$where .="and c.id_jawaban = '$id_jawaban' ";
		}

		
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_survey as a  
		inner join tbl_survey_pertanyaan as b 
		on a.id_survey=b.id_survey 
		inner join tbl_survey_jawaban as c 
		on b.id_pertanyaan=c.id_pertanyaan 
		".$where." order by c.jawaban DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function get_hasil_survey($id_merchant,$id_survey)
	{
		
		$where = "";	
		
		

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_survey !="")
		{		
			$where .="and a.id_survey = '$id_survey' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT 
		b.nama_member
		,c.pertanyaan
		
		,c.que_a
		,c.que_b
		,c.que_c
		,c.que_d
		,c.que_e 
		,c.opt_a
		,c.opt_b
		,c.opt_c
		,c.opt_d
		,c.opt_e
		
		FROM tbl_polling_det as a 
		inner join tbl_member as b on a.id_member=b.id_member
		inner join tbl_polling as c
		on a.id_polling=c.id_polling	
		
		
		".$where." group by a.id_polling DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
	
		return $res;
	}
	
	
	
	function get_pertanyaan_polling($id_merchant,$id_survey,$id_member)
	{
		$where = "";	

		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_survey !="")
		{		
			$where .="and a.id_survey = '$id_survey'";
		}
		
		
		
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_polling as a
		".$where." order by a.id_polling DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
			if(!empty($this->get_hasil_polling($res[$i]['id_polling'],$id_member)))
			{
				
				$status = $this->get_hasil_polling($res[$i]['id_polling'],$id_member);
				if($status[0]->status == "y"){
					
					$res[$i]['msg']	= true;	
					
				}else{
					
					$res[$i]['msg']	= false	;
					
				}

				
			}
			else
			{
				$res[$i]['msg']	= false	;
			}
		}
		return $res;
	}
	
	
		function get_hasil_polling($id_polling,$id_member)
		{
			
			$where = "";	
		
			if($id_polling !="")
			{		
			$where .="a.id_polling = '$id_polling' ";
			}
			
			if($id_member !="")
			{		
			$where .="and a.id_member = '$id_member' ";
			}
			
			if($where!="") $where =" where ".$where;
			
			$sql 	= "SELECT a.status
					FROM tbl_polling_det as a 
					inner join tbl_member as b 
					on a.id_member=b.id_member
					
					".$where." order by a.id_polling_det DESC";
			$rec	= $this->db->query($sql);
			$res	= $rec->result();
			return $res;
		}
	
	
	function cek_hasil_survey($id_merchant,$id_survey,$id_polling)
	{
		$query=$this->db->query("select * FROM tbl_survey_hasil where id_merchant='$id_merchant' and id_survey ='$id_survey' and id_polling='$id_polling'");
		return $query;
	}
	
	
	function cek_pertanyaan($id_member)
	{
		$query=$this->db->query("select * FROM tbl_polling_det where id_member='$id_member'");
		return $query;
	}

	
	
	function cek_survey($id_merchant,$id_survey)
	{
		$query=$this->db->query("select * from tbl_polling
		where id_merchant='$id_merchant' 
		and id_survey='$id_survey' 
		and opt_a ='0'
		and opt_b ='0'
		and opt_c ='0'
		and opt_d ='0'	
		");
		return $query;
	}

	function get_anak_didik($id_merchant,$id_anak)
	{
		
		$where = "";	
		
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_anak !="")
		{		
			$where .="and a.id_anak = '$id_anak' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.nis,a.tgl_lahir,b.id_kelas,a.id_anak,a.nama_anak,b.nama_kelas,a.status,a.photo,c.nama_member,a.create_at FROM tbl_anak as a 
		inner join tbl_kelas as b 
		on a.id_kelas=b.id_kelas
		inner join tbl_member as c 
		on a.id_member=c.id_member	
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	function delete_anak_sekolah($id_anak)
	{
		$where = "";	
		
		

		if($id_anak !="")
		{		
		$where .="a.id_anak = '$id_anak' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_anak as a ".$where."  order by a.id_anak DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}

	
	
	function get_prestasi_sekolah($id_merchant,$id_prestasi)
	{
		
		$where = "";	
		
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_prestasi !="")
		{		
			$where .="and a.id_prestasi = '$id_prestasi' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_prestasi_sekolah as a	
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function delete_prestasi_sekolah($id_prestasi)
	{
		$where = "";	
		
		

		if($id_prestasi !="")
		{		
		$where .="a.id_prestasi = '$id_prestasi' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_prestasi_sekolah as a ".$where."  order by a.id_prestasi DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	function get_prestasi_alumni($id_merchant,$id_prestasi)
	{
		
		$where = "";	
		
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_prestasi !="")
		{		
			$where .="and a.id_prestasi = '$id_prestasi' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_prestasi_alumni as a	
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	function delete_prestasi_alumni($id_prestasi)
	{
		$where = "";	
		
		

		if($id_prestasi !="")
		{		
		$where .="a.id_prestasi = '$id_prestasi' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_prestasi as a ".$where."  order by a.id_prestasi DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	
	// pegawai
	function get_guru_staff($id_merchant,$id_pegawai)
	{
		
		$where = "";	
		
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_pegawai !="")
		{		
			$where .="and a.id_pegawai = '$id_pegawai' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.nip,a.email,a.telp,a.pendidikan,a.tipe,a.nama_pegawai,a.id_pegawai,a.jabatan,a.alamat,a.photo,b.nama_merchant,a.status,a.create_at FROM tbl_pegawai as a 
		inner join tbl_merchant as b 
		on a.id_merchant=b.id_merchant
			
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	function get_nama_pengajar($id_merchant)
	{
		
		$where = "";	
		
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.id_pengajar,c.id_pegawai,c.nama_pegawai,a.status,c.photo,a.create_at FROM tbl_pengajar as a 
		inner join tbl_merchant as b 
		on a.id_merchant=b.id_merchant
		inner join tbl_pegawai as c
		on a.id_pegawai=c.id_pegawai
		".$where." order by c.nama_pegawai DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	
	// mapel
	
	
	
	function get_mapel_pengajar($id_merchant,$id_pegawai,$id_pengajar)
	{
		
		$where = "";	
		
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' and e.tipe='pengajar' ";
		}
		
		if($id_pegawai !="")
		{		
			$where .="and e.id_pegawai = '$id_pegawai' ";
		}
		
		if($id_pengajar !="")
		{		
			$where .="and d.id_pengajar = '$id_pengajar' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT b.id_pengajar_det,f.nama_kelas,d.id_pengajar,a.nama_mapel,d.create_at From tbl_mapel as a
		inner join tbl_pengajar_det as b 
		on a.id_mapel=b.id_mapel
		inner join tbl_pengajar as d
		on b.id_pegawai=d.id_pegawai
		inner join tbl_pegawai as e 
		on d.id_pegawai=e.id_pegawai
		inner join tbl_kelas as f 
		on b.id_kelas=f.id_kelas
		
		".$where." order by d.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	function cek_nama_guru($id_pegawai,$id_merchant)
	{
		$query=$this->db->query("select * from tbl_pengajar where id_pegawai ='$id_pegawai' and id_merchant ='$id_merchant'");
		return $query;
	}

	
	
	function cek_email_guru($email,$id_merchant)
	{
		$query=$this->db->query("select * from tbl_pegawai where email ='$email' and id_merchant ='$id_merchant'");
		return $query;
	}

	function cek_telp_guru($telp,$id_merchant)
	{
		$query=$this->db->query("select * from tbl_pegawai where telp ='$telp' and id_merchant ='$id_merchant'");
		return $query;
	}

	function delete_guru_sekolah($id_pegawai)
	{
		$where = "";	
		
		

		if($id_pegawai !="")
		{		
		$where .="a.id_pegawai = '$id_pegawai' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_pegawai as a ".$where."  order by a.id_pegawai DESC";
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	// pilih guru
	function get_pilih_guru($id_merchant)
	{
		
		$where = "";	
		
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' and a.tipe='pengajar'";
		}
		
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.nama_pegawai,a.id_pegawai FROM tbl_pegawai as a 
		inner join tbl_merchant as b 
		on a.id_merchant=b.id_merchant
		
		".$where." order by a.nama_pegawai DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
	}
	
	
	
	
	
	
	
// undangan	
	
	function get_undangan($id_merchant,$id_undangan)
	{
		
		$where = "";	
		
	
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant'";
		}
		
		if($id_undangan !="")
		{		
			$where .="and a.id_undangan = '$id_undangan' ";
		}
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT * FROM tbl_undangan as a 
		
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			$res[$i]['tgl_create']	= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	function get_IDundangan_sekolah($id_merchant,$id_undangan)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_undangan !="")
		{		
		$where .="and a.id_undangan = '$id_undangan' ";
		}
				
		if($where!="") $where =" where ".$where;


		$sql 	= "SELECT a.id_undangan
		,a.judul
		,a.deskripsi
		
		,b.nama_merchant
		,a.status
		,a.create_at FROM tbl_undangan as a 
		inner join tbl_merchant as b 
		on a.id_merchant=b.id_merchant 
		".$where." order by a.id_undangan DESC Limit 1";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	
	
	function get_kelas_sekolah($id_merchant,$id_kelas)
	{
		
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_kelas !="")
		{		
		$where .="and a.id_kelas = '$id_kelas' ";
		}
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT * FROM tbl_kelas as a	
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;

	}
	
	
	function get_wali_kelas($id_kelas)
	{
		
		$where = "";	
		
		
		
		if($id_kelas !="")
		{		
		$where .="a.id_kelas = '$id_kelas' ";
		}
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT a.id_wali_kelas,b.nama_pegawai,a.create_at FROM tbl_wali_kelas as a 
				inner join tbl_pegawai as b on a.id_pegawai=b.id_pegawai
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;

	}
	
	
	function get_mapel_sekolah($id_merchant,$id_mapel)
	{
		
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_mapel !="")
		{		
		$where .="and a.id_mapel = '$id_mapel' ";
		}
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT * FROM tbl_mapel as a 
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;

	}
	
	
	function get_kehadiran_wali($id_merchant,$id_undangan)
	{
		
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}
		
		if($id_undangan !="")
		{		
		$where .="and a.id_undangan = '$id_undangan' ";
		}
		
		if($where!="") $where =" where ".$where;
		
		$sql 	= "SELECT * FROM tbl_konf_kehadiran as a inner join tbl_member as b on a.id_member=b.id_member
		
		".$where." order by a.create_at DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->modif_create($res[$i]['create_at']);
		}
		return $res;

	}
	
	
	function get_cabang_pusat($id_merchant,$pusat)
	{

		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' and a.status_cabang ='$pusat' ";
		}

		
				
		if($where!="") $where =" where ".$where;

		$sql 	= "SELECT a.id_cabang,a.nama_cabang,b.nama_merchant,a.telp,a.alamat,b.photo,b.id_merchant,a.status,a.latitude,a.longitude,a.status_cabang FROM tbl_cabang_merchant as a inner join tbl_merchant as b on a.id_merchant=b.id_merchant ".$where." order by a.id_cabang DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		return $res;
	}
	
	function get_member_merchant($id_merchant,$id_member)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_member !="")
		{		
		$where .="and a.id_member = '$id_member' ";
		}
				
		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_member as a  ".$where." order by id_member DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	function get_pegawai_direktorat($id_merchant,$id_member)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

		if($id_member !="")
		{		
		$where .="and a.id_member = '$id_member' ";
		}
				
		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_member as a ".$where." order by a.id_member DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		foreach($res	as $i=>$r)
		{
			
			$res[$i]['tgl_create']= $this->dateDiff($res[$i]['create_at']);
		}
		return $res;
	}
	
	
	function get_unit_direktorat($id_merchant)
	{
		$where = "";	
		
		if($id_merchant !="")
		{		
		$where .="a.id_merchant = '$id_merchant' ";
		}

	
				
		if($where!="") $where =" where ".$where;
		$sql 	= "SELECT * FROM tbl_cabang_merchant as a ".$where." order by a.id_merchant DESC";
		//die($sql);
		$rec	= $this->db->query($sql);
		$res	= $rec->result_array();
		
		return $res;
	}
	
	function cek_data_cabang($id_merchant)
	{
		$query=$this->db->query("select * from tbl_cabang_merchant as a where id_merchant='$id_merchant' and status_cabang='pusat'");
		return $query;
	}


// hapus data

	function hapus_konten($id,$seleksi,$tabel)
	{
		$this->db->where($seleksi,$id);
		$this->db->delete($tabel);
	}



// convert tgl per detik

function dateDiff($date)
	{
  $mydate= date("Y-m-d H:i:s");
  $theDiff="";
  //echo $mydate;//2014-06-06 21:35:55
  $datetime1 = date_create($date);
  $datetime2 = date_create($mydate);
  $interval = date_diff($datetime1, $datetime2);
  //echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>";
  $min=$interval->format('%i');
  $sec=$interval->format('%s');
  $hour=$interval->format('%h');
  $mon=$interval->format('%m');
  $day=$interval->format('%d');
  $year=$interval->format('%y');
  if($interval->format('%i%h%d%m%y')=="00000")
  {
    //echo $interval->format('%i%h%d%m%y')."<br>";
    return $sec." Detik yang lalu";

  } 

else if($interval->format('%h%d%m%y')=="0000"){
   return $min." Menit yang lalu";
   }


else if($interval->format('%d%m%y')=="000"){
   return $hour." Jam yang lalu";
   }


else if($interval->format('%m%y')=="00"){
   return $day." Hari yang lalu";
   }

else if($interval->format('%y')=="0"){
   return $mon." Bulan";
   }



else{
   return $year." Tahun";
   }

}	

	


// convert by tanggal hari tahun

	function modif_tanggal($tgl, $hari_tampil = true)
	{
		$bulan	= array("Jan"
						, "Feb"
						, "Mar"
						, "Apr"
						, "Mei"
						, "Jun"
						, "Jul"
						, "Agu"
						, "Sep"
						, "Okt"
						, "Nov"
						, "Des");
		$hari	= array("Senin"
						, "Selasa"
						, "Rabu"
						, "Kamis"
						, "Jum'at"
						, "Sabtu"
						, "Minggu");
		$tahun_split	= substr($tgl, 0, 4);
		$bulan_split	= substr($tgl, 5, 2);
		$hari_split		= substr($tgl, 8, 2);
		$tmpstamp		= mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
		$bulan_jadi 	= $bulan[date("n", $tmpstamp)-1];
		$hari_jadi 		= $hari[date("N", $tmpstamp)-1];
		if(!$hari_tampil)
		$hari_jadi="";
		return $hari_jadi." ".$hari_split." ".$bulan_jadi." ".$tahun_split;
	}
	
	
	function tgl_bln_thn($tgl, $hari_tampil = true)
	{
		$bulan	= array("Jan"
						, "Feb"
						, "Mar"
						, "Apr"
						, "Mei"
						, "Jun"
						, "Jul"
						, "Agu"
						, "Sep"
						, "Okt"
						, "Nov"
						, "Des");
		$hari	= array("Senin"
						, "Selasa"
						, "Rabu"
						, "Kamis"
						, "Jum'at"
						, "Sabtu"
						, "Minggu");
		$tahun_split	= substr($tgl, 0, 4);
		$bulan_split	= substr($tgl, 5, 2);
		$hari_split		= substr($tgl, 8, 2);
		$tmpstamp		= mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
		$bulan_jadi 	= $bulan[date("n", $tmpstamp)-1];
		$hari_jadi 		= $hari[date("N", $tmpstamp)-1];
		if(!$hari_tampil)
		$hari_jadi="";
		return $hari_split." ".$bulan_jadi." ".$tahun_split;
	}
	
	
	
// modif nama

	function convert_name($title)
	{
		$title 	= str_replace(array(" ", "'", ",", "[", "]", "?", "!"), "-", $title);
		return strtolower($title);
	}
	
	


// tanggal versi indonesia
function modif_create($tgl, $hari_tampil = true)
	{
		$bulan	= array("Jan"
						, "Feb"
						, "Mar"
						, "Apr"
						, "Mei"
						, "Jun"
						, "Jul"
						, "Agu"
						, "Sep"
						, "Okt"
						, "Nov"
						, "Des");
		$hari	= array("Senin"
						, "Selasa"
						, "Rabu"
						, "Kamis"
						, "Jum'at"
						, "Sabtu"
						, "Minggu");
		$tahun_split	= substr($tgl, 0, 4);

		$y 				= strtotime($tgl);
		$year			= date("y", $y);
		$bulan_split	= substr($tgl, 5, 2);
		$hari_split		= substr($tgl, 8, 2);
		$tmpstamp		= mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
		$bulan_jadi 	= $bulan[date("n", $tmpstamp)-1];
		$hari_jadi 		= $hari[date("N", $tmpstamp)-1];
		if(!$hari_tampil)
		$hari_jadi="";
		return $hari_split." ".$bulan_jadi;
	}
	
	function dateExpired($date)
	{
  $mydate= date("Y-m-d H:i:s");
  $theDiff="";
  //echo $mydate;//2014-06-06 21:35:55
  $datetime1 = date_create($date);
  $datetime2 = date_create($mydate);
  $interval = date_diff($datetime1, $datetime2);
  //echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>";
  $min=$interval->format('%i');
  $sec=$interval->format('%s');
  $hour=$interval->format('%h');
  $mon=$interval->format('%m');
  $day=$interval->format('%d');
  $year=$interval->format('%y');
  if($interval->format('%i%h%d%m%y')=="00000")
  {
    //echo $interval->format('%i%h%d%m%y')."<br>";
    return $sec." Detik yang lalu";

  } 

else if($interval->format('%h%d%m%y')=="0000"){
   return $min." Menit yang lalu";
   }


else if($interval->format('%d%m%y')=="000"){
   return $hour." Jam yang lalu";
   }


else if($interval->format('%m%y')=="00"){
   return $day." Hari yang lalu";
   }

else if($interval->format('%y')=="0"){
   return $mon." Bulan";
   }



else{
   return $year." Tahun";
   }

}	


}
 
?>
