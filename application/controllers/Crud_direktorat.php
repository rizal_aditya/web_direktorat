<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_direktorat extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			
			redirect("direktorat/direktorat");
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			
			$url					= $this->config->item('base_url');	

			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);
			$dek					= $this->input->post('isi', TRUE);	
			$isi					=  htmlspecialchars($dek, ENT_QUOTES);

			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tipe					= 	$this->input->post('tipe', TRUE);
			$id_member					= 	$this->input->post('nama_member', TRUE);
			$id_kelas					= 	$this->input->post('nama_kelas', TRUE);
			$id_cabang					= 	$this->input->post('divisi', TRUE);
			
		
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'berita/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			$max 		= $_FILES['userfile']['size'];
			$extensi 	=  $this->mod_main->get_ekstensi($asli);
			
			
			
			

			if(!empty($asli)){ 
			
			
			
				if($max == 0 or $max > 1500000){
							
					echo"<script> alert('Pilih file maks 1,5 MB...');</script>";
					echo'<script type="text/javascript">location.href="'.$url.'/sekolah/direktorat";</script>';
					
				} else if(($extensi != 'jpg') && ($extensi != 'jpeg') && ($extensi != 'png')){ 
					
					echo"<script type='text/javascript'>alert('Extensi file harus jpeg atau jpg atau png...');</script>";
					echo'<script type="text/javascript">location.href="'.$url.'/sekolah/direktorat";</script>';
					
				} else {	
			
			
			
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/berita/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] 			= TRUE;
			$config['max_size'] 			= '0';
			$config['max_width'] 			= '0';
			$config['max_height'] 			= '0'; 						
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
			$thumbr = $thumb;

					$q = "insert into tbl_berita(judul,id_merchant,isi,status,photo,tipe,id_cabang,create_at) 
				 values('".$judul."','".$id_merchant."','".$isi."','".$status."','".$thumbr."','".$tipe."','".$id_cabang."',NOW())";
				$this->mod_main->create($q);
					
				}
				
				
				
			}else{ 
			
				$q = "insert into tbl_berita(judul,id_merchant,isi,status,tipe,id_cabang,create_at) 
				 values('".$judul."','".$id_merchant."','".$isi."','".$status."','".$tipe."','".$id_cabang."',NOW())";
				$this->mod_main->create($q);
			
			 }		
			 
			 
			 
			 
						$id_beritae = "";
						$data["news"] = $this->model_sekolah->get_IDBerita_sekolah($id_merchant,$id_beritae);				
						$id_berita 	= $data["news"][0]['id_berita'];
						$judul 		= $data["news"][0]['judul'];
						$isi   		= $data["news"][0]['isi'];
						$status 	= $data["news"][0]['status'];
						$photo 		= $data["news"][0]['photo'];
						$create_at 	= $data["news"][0]['tgl_create'];
						$merchant 	= $data["news"][0]['nama_merchant'];
						
					
						
							
						$message = array(
						"idnews" 				=> $id_berita,
						"judul" 				=> $judul,
						"isi" 					=> $isi,
						"status" 				=> $status,
						"photo" 				=> $photo,
						"create" 				=> $create_at,
						"merchant" 				=> $merchant,
						"param"					=> "berita",
						"sender"				=> $sender);

				
				$api_key = $api_merchant;	
				if($broadcast == "y" && $tipe="direktorat")
				{
					
						//send gcm all
						$all = $this->model_sekolah->get_gcm_member($id_merchant);
						foreach ($all as $row)
						{
							$reg_id 		= "".$row['gcm']."";
							$nama_member	= "".$row['nama_member']."";
							$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);	
						}
					
				}
					
				redirect("direktorat/direktorat");	
				
				
			
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$url					= $this->config->item('base_url');	
		
			
			$id_berita 				= $this->uri->segment(3);
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);
			$dek					= $this->input->post('isi', TRUE);
			$isi					= htmlspecialchars($dek, ENT_QUOTES);	
			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tipe					= 	$this->input->post('tipe', TRUE);
			$id_member					= 	$this->input->post('nama_member', TRUE);
			$id_kelas					= 	$this->input->post('nama_kelas', TRUE);
			$gbr 		= $this->input->post('gbr');
			$id_cabang					= 	$this->input->post('divisi', TRUE);
	
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'berita/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			$max 		= $_FILES['userfile']['size'];
			$extensi 	=  $this->mod_main->get_ekstensi($asli);
			
			
			
			
			
			if(!empty($asli)){
				
				
				
				if($max == 0 or $max > 1500000){
							
					echo"<script> alert('Pilih file maks 1,5 MB...');</script>";
					echo'<script type="text/javascript">location.href="'.$url.'/sekolah/direktorat";</script>';
					
			} else if(($extensi != 'jpg') && ($extensi != 'jpeg') && ($extensi != 'png')){ 
					
					echo"<script type='text/javascript'>alert('Extensi file harus jpeg atau jpg atau png...');</script>";
					echo'<script type="text/javascript">location.href="'.$url.'/sekolah/direktorat";</script>';
					
			 } else {	
			 
			 
			
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/berita/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] = TRUE;
			$config['max_size'] = '0';
			$config['max_width']  = '0';
			$config['max_height']  = '0'; 							
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
			
			$thumbr = $thumb;
			
			
			 	
			$q_update = "update tbl_berita set 
			judul = '".$judul."'
			, id_merchant = '".$id_merchant."'
			, isi = '".$isi."'
			, status = '".$status."'
			, id_cabang = '".$id_cabang."'	
			, tipe 		= '".$tipe."'
			, photo = '".$thumbr."'
			
			, update_at = NOW() 
			where id_berita  	='".$id_berita."'";
			$this->mod_main->put($q_update);
				
					if(!empty($gbr))
					{
						$file_gambar  = './assets/upload/berita/'.$gbr;
						unlink($file_gambar);
					}
					
			 }	
					
			
			}else{
				
			

			$q_update = "update tbl_berita set 
			judul = '".$judul."'
			, id_merchant = '".$id_merchant."'
			, isi = '".$isi."'
			, status = '".$status."'
			
			, tipe 		= '".$tipe."'
		
			, update_at = NOW() 
			where id_berita  	='".$id_berita."'";
			$this->mod_main->put($q_update);
				
			}		
			
			
				$data["news"] = $this->model_sekolah->get_IDBerita_sekolah($id_merchant,$id_berita);				
				$id_berita 	= $data["news"][0]['id_berita'];
				$judul 		= $data["news"][0]['judul'];
				$isi   		= $data["news"][0]['isi'];
				$status 	= $data["news"][0]['status'];
				$photo 		= $data["news"][0]['photo'];
				$create_at 	= $data["news"][0]['tgl_create'];
				$merchant 	= $data["news"][0]['nama_merchant'];
				
			
				
					
				$message = array(
				"idnews" 				=> $id_berita,
				"judul" 				=> $judul,
				"isi" 					=> $isi,
				"status" 				=> $status,
				"photo" 				=> $photo,
				"create" 				=> $create_at,
				"merchant" 				=> $merchant,
				"param"					=> "berita",
				"sender"				=> $sender);

				$api_key = $api_merchant;	
				if($broadcast == "y" && $tipe="direktorat")
				{
					
						//send gcm all
						$all = $this->model_sekolah->get_gcm_member($id_merchant);
						foreach ($all as $row)
						{
						
						$reg_id 		= "".$row['gcm']."";
						$nama_member	= "".$row['nama_member']."";
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);	
						
						}
						
					
				}	
									

			
			
				
					redirect("direktorat/direktorat");	
					
				

			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!=""){

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$id_merchant			= 	$data["id_merchant"];
		
		
			

			$info = $this->model_sekolah->get_direktorat($id_merchant,$kode);
			$photo = $info[0]['photo'];

			if($photo != "")
			{	
			$gambar  = './assets/upload/berita/'.$photo;
			unlink($gambar);
			}

			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_berita","tbl_berita");
			
			
			redirect("direktorat/direktorat");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	


	function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
