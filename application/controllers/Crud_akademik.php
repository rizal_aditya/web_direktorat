<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crud_akademik extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		
	}
	
	
	
	function index()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			
			redirect("direktorat/agenda");

		}else{

			$this->load->view('function/login_merchant');	
		}	
	}

	
	function insert()
	{

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
			
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);
			$warna					= $this->input->post('warna', TRUE);
			$dek					= $this->input->post('deskripsi', TRUE);		
			$deskripsi				= htmlspecialchars($dek, ENT_QUOTES);
			$dv 					= strip_tags($desk,"<p>");
			$type					= $this->input->post('type', TRUE);
			$id_cabang					= $this->input->post('id_cabang', TRUE);
			
			$tgl_mulai				= $this->input->post('tgl_mulai', TRUE);
			$tgl_selesai			= $this->input->post('tgl_selesai', TRUE);
			//$tgl_selesai			= $this->input->post('tgl_selesai', TRUE);
			$status					= $this->input->post('status', TRUE);
			

			$q = "insert into tbl_kalender(judul,warna,id_cabang,type,id_merchant,tgl_mulai,tgl_selesai,deskripsi,status,create_at) 
					values('".$judul."','".$warna."','".$id_cabang."','".$type."','".$id_merchant."','".$tgl_mulai."','".$tgl_selesai."','".$deskripsi."','".$status."',NOW())";
			$this->mod_main->create($q);
			 
			 
			redirect("direktorat/agenda");					
										
			}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}
	
	
	function update()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
			$id_kalender =  $this->uri->segment(3);
			
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);
			$warna					= $this->input->post('warna', TRUE);
			$dek					= $this->input->post('deskripsi', TRUE);	
			$deskripsi				= htmlspecialchars($dek, ENT_QUOTES);
			$tgl_mulai				= $this->input->post('tgl_mulai', TRUE);
			$tgl_selesai			= $this->input->post('tgl_selesai', TRUE);
			$status					= $this->input->post('status', TRUE);
			$type					= $this->input->post('type', TRUE);			
			$id_cabang					= $this->input->post('id_cabang', TRUE);		
			 
			 $q_update = "update tbl_kalender set 
			  judul				= '".$judul."'
			, id_merchant 		= '".$id_merchant."'
			, warna 			= '".$warna."' 
			, tgl_mulai			= '".$tgl_mulai."'
			, tgl_selesai		= '".$tgl_selesai."'
			, type				= '".$type."'
			, id_cabang			= '".$id_cabang."'
			, deskripsi			= '".$deskripsi."'
			, status			= '".$status."'
			, update_at 		= NOW()
			where id_kalender 	='".$id_kalender."'";
			$this->mod_main->put($q_update);	
				
	
			redirect("direktorat/agenda");
			
			

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!=""){

			
					

			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_kalender","tbl_kalender");
			
				redirect("direktorat/agenda");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	
	
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
