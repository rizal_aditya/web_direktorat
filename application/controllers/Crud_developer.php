<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_developer extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_developer");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_admin_user"]	=	$pecah[0];
			$data["user_name"]		=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$kode					= 	$data["id_admin_user"];
			$id_developer			= 	$data["id_admin_user"];	
			
			redirect("developer/sekolah");

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_admin_user"]	=	$pecah[0];
			$data["user_name"]		=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$kode					= 	$data["id_admin_user"];
			$id_developer			= 	$data["id_admin_user"];	
			

			$url					= 	$this->config->item('base_url');	
			
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'merchant/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;

			$default = "vcard";
			$nama_sekolah			= 	$this->input->post('nama_sekolah', TRUE);
			$email					= 	$this->input->post('email', TRUE);
			$telp					= 	$this->input->post('telp', TRUE);
			$pass 					= 	$this->pass_word->encryptPass($default);
			$alamat					= 	$this->input->post('alamat', TRUE);
			$api_key				= 	$this->input->post('api_key', TRUE);
			$status					= 	$this->input->post('status', TRUE);
			$jenis					= 	"sekolah";
			$id_paket				= 	$this->input->post('id_paket', TRUE);
			$max 		= $_FILES['userfile']['size'];
			$extensi 	=  $this->mod_main->get_ekstensi($asli);
			
			
		
			
			$pusat			=  "pusat";
			
			$kodeM = "";
			$limit = "Limit 1";
			
			
		
			if(!empty($asli)){ 
				
				if($max == 0 or $max > 1500000){
								
						echo"<script> alert('Pilih file maks 1,5 MB...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/developer/sekolah";</script>';
						
				} else if(($extensi != 'jpg') && ($extensi != 'jpeg') && ($extensi != 'png')){ 
						
						echo"<script type='text/javascript'>alert('Extensi file harus jpeg atau jpg atau png...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/developer/sekolah";</script>';
						
				 } else {	
				
				
		
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/merchant/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] 			= TRUE;
			$config['max_size'] 			= '0';
			$config['max_width'] 			= '0';
			$config['max_height'] 			= '0'; 						
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
			
			
			
					$thumbr = $thumb;

					$q = "insert into tbl_merchant(
					nama_merchant
					,telp
					,email
					,alamat
					,pass
					,api_key
					,jenis
					,id_paket
					,active
					,photo
					,create_at) 
					 values(
					 '".$nama_sekolah."'
					 ,'".$telp."'
					 ,'".$email."'
					 ,'".$alamat."'	
					 ,'".$pass."'
					 ,'".$api_key."'
					 ,'".$jenis."'
					 ,'".$id_paket."'
					 ,'".$status."'
					 ,'".$thumbr."'
					 ,NOW())";
					$this->mod_main->create($q);
					
				$q = $this->model_developer->get_merchant($kodeM,$limit);
				$id_merchant = $q[0]['id_merchant'];
				$nama_sek  = 	$q[0]['nama_merchant'];
					$q = "insert into tbl_cabang_merchant(nama_cabang,id_merchant,telp,alamat,status_cabang,status,create_at) 
					 values('".$nama_sek."','".$id_merchant."','".$telp."','".$alamat."','".$pusat."','".$status."',NOW())";
					$this->mod_main->create($q);
					
					
					
					
			}

		
		
			
				
			
				

			
		}else{
			
			
			$q = "insert into tbl_merchant(
					nama_merchant
					,telp
					,email
					,alamat
					,pass
					,api_key
					,jenis
					,id_paket
					,active
					
					,create_at) 
					 values(
					 '".$nama_sekolah."'
					 ,'".$telp."'
					 ,'".$email."'
					 ,'".$alamat."'	
					 ,'".$pass."'
					 ,'".$api_key."'
					 ,'".$jenis."'
					 ,'".$id_paket."'
					 ,'".$status."'
				
					 ,NOW())";
					$this->mod_main->create($q);
					
					$q = $this->model_developer->get_merchant($kodeM,$limit);
					$id_merchant = $q[0]['id_merchant'];
					$nama_sek  = 	$q[0]['nama_merchant'];
					$q = "insert into tbl_cabang_merchant(nama_cabang,id_merchant,telp,alamat,status_cabang,status,create_at) 
					 values('".$nama_sek."','".$id_merchant."','".$telp."','".$alamat."','".$pusat."','".$status."',NOW())";
					$this->mod_main->create($q);
					
		}
			
			
			
			redirect("developer/sekolah");	

		
							
				
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_admin_user"]	=	$pecah[0];
			$data["user_name"]		=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$kode					= 	$data["id_admin_user"];
			$id_developer			= 	$data["id_admin_user"];	
				
				$gbr 		= $this->input->post('gbr');
			$id_merchant = $this->uri->segment(3);		
			$url					= 	$this->config->item('base_url');	
			
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'merchant/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;

			$default = "vcard";
			$nama_sekolah			= 	$this->input->post('nama_sekolah', TRUE);
			
			$telp					= 	$this->input->post('telp', TRUE);
			$pass 					= 	$this->pass_word->encryptPass($default);
			$alamat					= 	$this->input->post('alamat', TRUE);
			$api_key				= 	$this->input->post('api_key', TRUE);
			$status					= 	$this->input->post('status', TRUE);
			$jenis					= 	"sekolah";
			$id_paket				= 	$this->input->post('id_paket', TRUE);
			$max 		= $_FILES['userfile']['size'];
			$extensi 	=  $this->mod_main->get_ekstensi($asli);
			
			
		
	
		
		
			if(!empty($asli)){ 
				
				if($max == 0 or $max > 1500000){
								
						echo"<script> alert('Pilih file maks 1,5 MB...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/developer/sekolah";</script>';
						
				} else if(($extensi != 'jpg') && ($extensi != 'jpeg') && ($extensi != 'png')){ 
						
						echo"<script type='text/javascript'>alert('Extensi file harus jpeg atau jpg atau png...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/developer/sekolah";</script>';
						
				 } else {	
				
				
		
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/merchant/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] 			= TRUE;
			$config['max_size'] 			= '0';
			$config['max_width'] 			= '0';
			$config['max_height'] 			= '0'; 						
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
			
			
			
					$thumbr = $thumb;

					 $q_update 		= "update tbl_merchant set 
					nama_merchant	= '".$nama_sekolah."'
					, telp 			= '".$telp."'
					
					, alamat 		= '".$alamat."'
					, api_key 		= '".$api_key."'
					, jenis 			= '".$jenis."'
					, id_paket 		= '".$id_paket."'
					, active		= '".$status."'
					, photo = '".$thumbr."'
					, update_at = NOW() 
					where id_merchant 	='".$id_merchant."'";
					$this->mod_main->put($q_update);
				
				 }
				
				 }else{
					 
					 
					 $q_update 		= "update tbl_merchant set 
					nama_merchant	= '".$nama_sekolah."'
					, telp 			= '".$telp."'
				
					, alamat 		= '".$alamat."'
					, api_key 		= '".$api_key."'
					, jenis 			= '".$jenis."'
					, id_paket 		= '".$id_paket."'
					, active 		= '".$status."'
					
					, update_at = NOW() 
					where id_merchant 	='".$id_merchant."'";
					$this->mod_main->put($q_update);
			
			
					if(!empty($gbr))
					{
				
					$file_gambar  = './assets/upload/merchant/'.$gbr;
					unlink($file_gambar);

					}
					 
					 
					 
					 
				 }

	
					redirect("developer/sekolah");		
					


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}
	
	
	function update_notifikasi()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_admin_user"]	=	$pecah[0];
			$data["user_name"]		=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$kode					= 	$data["id_admin_user"];
			$id_developer			= 	$data["id_admin_user"];	
				

			
			
			$id_merchant = $this->uri->segment(3);
			
			$notifikasi			= 	$this->input->post('notifikasi', TRUE);
			$deskripsi				= 	$this->input->post('deskripsi', TRUE);
		

			$q = "insert into tbl_notifikasi(
					id_merchant
					,notifikasi
					,deskripsi
					,status
					
					,create_at) 
					 values(
					 '".$id_merchant."'
					 ,'".$notifikasi."'
					 ,'".$deskripsi."'
					 ,'".$status."'			
					 ,NOW())";
					$this->mod_main->create($q);
					
	
				
	
	
					redirect("developer/sekolah/notifikasi");		
					


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!=""){

		
			

			$info = $this->model_developer->delete_merchant($kode);
			foreach ($info as $data) 
			{

				$photo = "".$data['photo']."";

			}	

			if($photo != "")
			{	
			$gambar  = './assets/upload/merchant/'.$photo;
			unlink($gambar);
			}

			$data["del"] = $this->model_developer->hapus_konten($kode,"id_merchant","tbl_merchant");
			$data["del_cabang"] = $this->model_developer->hapus_konten($kode,"id_merchant","tbl_cabang_merchant");
			redirect("developer/sekolah");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	
	function delete_notifikasi()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!=""){

		
			

		


			$data["del"] = $this->model_developer->hapus_konten($kode,"id_notifikasi","tbl_notifikasi");
			
			redirect("developer/sekolah/notifikasi");	

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
