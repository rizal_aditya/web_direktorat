<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_divisi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			
			redirect("direktorat/direktorat");
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			
			$url					= $this->config->item('base_url');	

			$jdl					= $this->input->post('nama_cabang', TRUE);		
			$nama_cabang    		= htmlspecialchars($jdl, ENT_QUOTES);
			$status					= "y";
			$status_cabang			= "cabang";
			
			

			$q = "insert into tbl_cabang_merchant(nama_cabang,id_merchant,status_cabang,status,create_at) 
			 values('".$nama_cabang."','".$id_merchant."','".$status."','".$status_cabang."',NOW())";
			$this->mod_main->create($q);
					
			
					
			redirect("direktorat/divisi");	
				
				
			
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$url					= $this->config->item('base_url');	
		
			
			$id_divisi 				= $this->uri->segment(3);
			$jdl					= $this->input->post('nama_cabang', TRUE);		
			$nama_cabang    		= htmlspecialchars($jdl, ENT_QUOTES);
			$status					= "y";
			$status_cabang			= "cabang";
			
	
			$q_update = "update tbl_cabang_merchant set 
			nama_cabang 			= '".$nama_cabang."'
			, id_merchant 	= '".$id_merchant."'
			, status 		= '".$status."'
			, status_cabang = '".$status_cabang."'
			, update_at = NOW() 
			where id_cabang  	='".$id_divisi."'";
			$this->mod_main->put($q_update);
				
			redirect("direktorat/divisi");	
					
				

			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!=""){

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$id_merchant			= 	$data["id_merchant"];
		
		
		

			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_cabang","tbl_cabang_merchant");
			
			
			redirect("direktorat/divisi");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	


	function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
