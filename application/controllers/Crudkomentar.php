<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CrudKomentar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			$id_member				= 	$this->uri->segment(4);	


			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			$id_member				= 	$this->uri->segment(4);	



			$data['page'] 				= 'merchant/komentar';
			$data['deskripsi'] 			= "Komentar Merchant";

			$data["komentar_diskon"]				= $this->model_merchant->get_komentar_diskon();
			$data["komentar_berita"]				= $this->model_merchant->get_komentar_berita();

			$data['header_keyword'] 	= "";
			$data['header_desc'] 		= "";
			$data['header_title'] 		= "Komentar Merchant";

			$this->load->view('header',$data);
			$this->load->view('main_header',$data);	
			$this->load->view('main_left',$data);
			$this->load->view('main_menu',$data);
			$this->load->view('footer',$data);	

		

		

		}else{

			echo "<meta http-equiv='refresh' content='0; url=".base_url()."merchant/login'>";
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{

			


		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!=""){

		

			$data["del"] = $this->model_merchant->hapus_konten($kode,"id_komentar","tbl_komentar");
			redirect("merchant/komentar");

		}else{

			redirect("merchant/");
		}	
		
	}


	function multidelete()
	{

		$action = $this->input->post('action');
						

		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!=""){


						if($action == "view"){

						echo "<meta http-equiv='refresh' content='0; url=".base_url()."merchant/member'>";

						} else if($action == "update"){
							$get = $this->input->post('msg');

						}else if ($action == "delete") {
							$delete = $this->input->post('msg');
							for ($i=0; $i < count($delete) ; $i++) { 


								echo $delete[$i];	

							//$this->db->where('id_member', $delete[$i]);
							//$this->db->delete('tbl_member');

							}

						}

				//echo "<meta http-equiv='refresh' content='0; url=".base_url()."merchant/member'>";

		}else{

			echo "<meta http-equiv='refresh' content='0; url=".base_url()."merchant'>";
		}		

	}	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
