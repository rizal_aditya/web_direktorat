<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crudgreetingdeveloper extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_developer");
		$this->load->model("mod_main");
		//session_start();
	}
	
	
	function index()
	{
		
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{
			
			redirect("developer/greetings");
	
			

		}else{

		
			$this->load->view('function/login_merchant');
			
		}	
	}

	
	function insert()
	{

		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$url					= $this->config->item('base_url');
			
			$judul					= $this->input->post('judul', TRUE);
			$dek					= $this->input->post('isi', TRUE);
			$isi					= htmlspecialchars($dek, ENT_QUOTES);
			$agama					= $this->input->post('agama', TRUE);
			$kategori				= $this->input->post('kategori', TRUE);			
			$tgl_greeting			= $this->input->post('tgl_greeting', TRUE);
			$status					= $this->input->post('status', TRUE);
			$negara 				= "indonesia";

			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			$alt		= 'assets/upload/';
			$dir 		= 'greeting/';
			
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			$max 		= $_FILES['userfile']['size'];
			$extensi 	=  $this->mod_main->get_ekstensi($asli);
			
			if(!empty($asli)){ 
			
			
					if($max == 0 or $max > 1500000){
								
						echo"<script> alert('Pilih file maks 1,5 MB...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/developer/greetings";</script>';
						
					} else if(($extensi != 'jpg') && ($extensi != 'jpeg') && ($extensi != 'png')){ 
						
						echo"<script type='text/javascript'>alert('Extensi file harus jpeg atau jpg atau png...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/developer/greetings";</script>';
						
					} else {	
			
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/greeting/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] 			= TRUE;
			$config['max_size'] 			= '0';
			$config['max_width'] 			= '0';
			$config['max_height'] 			= '0'; 							
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
			$thumbr = $thumb;

			$q = "insert into tbl_greeting(judul,id_admin,isi,agama,kategori,tgl_greeting,status,negara,photo,create_at) 
					values('".$judul."','".$id_merchant."','".$isi."','".$agama."','".$kategori."','".$tgl_greeting."','".$status."','".$negara."','".$thumbr."',NOW())";
			$this->mod_main->create($q);
			
					}
				
			}else{ 
			
			$q = "insert into tbl_greeting(judul,id_admin,isi,agama,kategori,tgl_greeting,status,negara,create_at) 
					values('".$judul."','".$id_merchant."','".$isi."','".$agama."','".$kategori."','".$tgl_greeting."','".$status."','".$negara."',NOW())";
			$this->mod_main->create($q);
			
			
		 
		 
		 
			}
			
				
			
						
			redirect("developer/greetings");					
										
			}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}
	
	
	function update()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$url					= $this->config->item('base_url');
			
			$id_greeting 			= $this->uri->segment(3);
			$judul					= $this->input->post('judul', TRUE);
			$dek					= $this->input->post('isi', TRUE);
			$isi					= htmlspecialchars($dek, ENT_QUOTES);
			$agama					= $this->input->post('agama', TRUE);
			$kategori				= $this->input->post('kategori', TRUE);			
			$tgl_greeting			= $this->input->post('tgl_greeting', TRUE);
			$status					= $this->input->post('status', TRUE);
			$negara					= "indonesia";
					
			$gbr 		= $this->input->post('gbr');
	
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			
			$alt		= 'assets/upload/';
			$dir 		= 'greeting/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			
			if(!empty($asli)){
				
					if($max == 0 or $max > 1500000){
								
						echo"<script> alert('Pilih file maks 1,5 MB...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/developer/greetings";</script>';
						
					} else if(($extensi != 'jpg') && ($extensi != 'jpeg') && ($extensi != 'png')){ 
						
						echo"<script type='text/javascript'>alert('Extensi file harus jpeg atau jpg atau png...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/developer/greetings";</script>';
						
					} else {	
		
		
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/greeting/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] 			= TRUE;
			$config['max_size'] 			= '0';
			$config['max_width']  			= '0';
			$config['max_height']  			= '0'; 						
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
			
			$thumbr = $thumb; 
			
			$q_update = "update tbl_greeting set 
			  judul				= '".$judul."'
			, id_admin			= '".$id_merchant."'  
			, isi				= '".$isi."'
			, agama 			= '".$agama."'
			, kategori			= '".$kategori."'
			, tgl_greeting		= '".$tgl_greeting."'
			, negara			= '".$negara."'
			, status			= '".$status."'
			, photo 			= '".$thumbr."'
			, update_at 		= NOW()
			where id_greeting 	='".$id_greeting."'";
			$this->mod_main->put($q_update);	
			
					if(!empty($gbr))
					{

					$file_gambar  = './assets/upload/greeting/'.$gbr;
					unlink($file_gambar);
			
					}
					
				}
			
			
			}else{
				
			$q_update = "update tbl_greeting set 
			  judul				= '".$judul."'
			, id_admin 			= '".$id_merchant."'  
			, isi				= '".$isi."'
			, agama 			= '".$agama."'
			, kategori			= '".$kategori."'
			, tgl_greeting		= '".$tgl_greeting."'
			, negara			= '".$negara."'
			, status			= '".$status."'
			, update_at 		= NOW()
			where id_greeting 	='".$id_greeting."'";
			$this->mod_main->put($q_update);	
		

			}		
		
			redirect("developer/greetings");

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!=""){

			if(!empty($photo))
			{	

					$info = $this->model_developer->delete_greeting_developer($kode);
					foreach ($info as $data) 
					{

						$photo = "".$data['photo']."";

					}	

			
					$gambar  = "./assets/upload/greeting/".$photo;
					unlink($gambar);
			}

			$data["del"] = $this->model_developer->hapus_konten($kode,"id_greeting","tbl_greeting");
		
			
			redirect("developer/greetings");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	


	function sendMessageThroughGCM($registatoin_ids, $message, $api_key)
	{
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
