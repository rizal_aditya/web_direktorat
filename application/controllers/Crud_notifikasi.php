<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_notifikasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			
			redirect("direktorat/direktorat");
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			
			$url					= $this->config->item('base_url');	

			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);
			$dek					= $this->input->post('isi', TRUE);	
			$isi					=  htmlspecialchars($dek, ENT_QUOTES);

			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tipe					= 	$this->input->post('tipe', TRUE);
			$tgl_mulai					= 	$this->input->post('tgl_mulai', TRUE);
			$set_waktu					= 	$this->input->post('set_waktu', TRUE);
			$type					= 	$this->input->post('type', TRUE);
			

			$q = "insert into tbl_notif_direktorat(judul,id_merchant,isi,tgl_mulai,set_waktu,status,type,broadcast,create_at) 
			 values('".$judul."','".$id_merchant."','".$isi."','".$tgl_mulai."','".$set_waktu."','".$status."','".$type."','".$broadcast."',NOW())";
			$this->mod_main->create($q);
					
					
				redirect("direktorat/notifikasi");	
				
				
			
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}
	
	
	



	function update()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$url					= $this->config->item('base_url');	
		
			
			$id_notif 				= $this->uri->segment(3);
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);
			$dek					= $this->input->post('isi', TRUE);
			$isi					= htmlspecialchars($dek, ENT_QUOTES);	
			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tipe					= 	$this->input->post('tipe', TRUE);
			$tgl_mulai				= 	$this->input->post('tgl_mulai', TRUE);
			$set_waktu				= 	$this->input->post('set_waktu', TRUE);
	
			
		
			
			
			 	
			$q_update = "update tbl_notif_direktorat set 
			judul 			= '".$judul."'
			, id_merchant 	= '".$id_merchant."'
			, isi 			= '".$isi."'
			, status 		= '".$status."'
			, broadcast 	= '".$broadcast."'
			, tgl_mulai 	= '".$tgl_mulai."'
			, set_waktu 	= '".$set_waktu."'

			, update_at = NOW() 
			where id_notif  	='".$id_notif."'";
			$this->mod_main->put($q_update);
				
					
		

			
			

						$id_notifikasi = "";
						$data["notif"] = $this->model_sekolah->get_IDnotifikasi_direktorat($id_merchant,$id_notifikasi);				
						$id_notif 	= $data["notif"][0]['id_notif'];
						$judul 		= $data["notif"][0]['judul'];
						$isi   		= $data["notif"][0]['isi'];
						$status 	= $data["notif"][0]['status'];
						$tgl_mulai 	= $data["notif"][0]['tgl_mulai'];
						$set_waktu 	= $data["notif"][0]['set_waktu'];
						$create_at 	= $data["notif"][0]['tgl_create'];
						$merchant 	= $data["notif"][0]['nama_merchant'];
						
					
						
							
						$message = array(
						"id_notif" 				=> $id_notif,
						"judul" 				=> $judul,
						"isi" 					=> $isi,
						"status" 				=> $status,
						"tgl_mulai" 			=> $tgl_mulai,
						"set_waktu" 			=> $set_waktu,
						"create" 				=> $create_at,
						"merchant" 				=> $merchant,
						"param"					=> "notifikasi",
						"sender"				=> $sender);

				
				$api_key = $api_merchant;	
				if($broadcast == "y")
				{
					
						//send gcm all
						$all = $this->model_sekolah->get_gcm_member($id_merchant);
						foreach ($all as $row)
						{
							$reg_id 		= "".$row['gcm']."";
							$nama_member	= "".$row['nama_member']."";
							$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
							//echo $reg_id;
						}
					
					
				}

			
			
				
					redirect("direktorat/notifikasi");	
					
				

			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!=""){

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$id_merchant			= 	$data["id_merchant"];
		
		
		

			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_notif","tbl_notif_direktorat");
			
			
			redirect("direktorat/notifikasi");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	


	function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
