<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudkelas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			
			redirect("sekolah/kelas");	
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			$id_kelas = "";	
				

			$kel						= 	$this->input->post('nama_kelas', TRUE);
			$nama_kelas					=  	htmlspecialchars($kel, ENT_QUOTES);
			$kategori					= 	$this->input->post('kategori', TRUE);
			$status						= 	$this->input->post('status', TRUE);
			
			$cekkelas = $this->model_sekolah->get_kelas($id_merchant,$id_kelas);
			$nama = $cekkelas[0]['nama_kelas'];
			
			if($nama == $kel)
			{
				
				
				echo"<script>alert('Nama kelas sudah ada!!')</script>";
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."sekolah/kelas'>";
				
			}else{
				
				
			$q = "insert into tbl_kelas(nama_kelas,id_merchant,tipe,status,create_at) 
			values('".$nama_kelas."','".$id_merchant."','".$kategori."','".$status."',NOW())";
			$this->mod_main->create($q);
			redirect("sekolah/kelas");	
			}
			


				
			 

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_sekolah"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
		
			$id_kelas				= $this->uri->segment(3);	
		
			$kel						= 	$this->input->post('nama_kelas', TRUE);
			$nama_kelas					=  	htmlspecialchars($kel, ENT_QUOTES);
			$kategori					= 	$this->input->post('kategori', TRUE);
			$status						= 	$this->input->post('status', TRUE);
			
			
			$q_update = "update tbl_kelas set 
			nama_kelas 			= '".$nama_kelas."'
			, id_merchant 	= '".$id_merchant."'
		
			, tipe 		= '".$kategori."'
			, status 		= '".$status."'
			, update_at = NOW() 
			where id_kelas  	='".$id_kelas."'";
			$this->mod_main->put($q_update);
			
			redirect("sekolah/kelas");	
					


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_kelas","tbl_kelas");
			
			redirect("sekolah/kelas");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	
	function delete_all()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_pegawai","tbl_pengajar");
			
			redirect("sekolah/pengajar");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
