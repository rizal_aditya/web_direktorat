<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_survey_sekolah extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			redirect("sekolah/survey");
		

		}else{

			$this->load->view('function/login_merchant');
			
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);	
			$status					= $this->input->post('status', TRUE);
			$id_survey = sprintf("%04d", mt_rand(1, 9999));
			
			
			$base_url = $this->config->item('base_url');
			$url = "sekolah/view_pertanyaan/$id_merchant/";
			$slas = "/";
			$link_url = $base_url.$slas.$url.$slas.$id_survey;
			
			
	

			$q = "insert into tbl_survey(id_survey,judul,id_merchant,status,link_url,create_at) 
				values('".$id_survey."','".$judul."','".$id_merchant."','".$status."','".$link_url."',NOW())";
			$this->mod_main->create($q);
				
				
			redirect("sekolah/survey");

	
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}


	function insert_merchant()
	{

		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);	
			$status					= $this->input->post('status', TRUE);
			
			
			
			$base_url = $this->config->item('base_url');
			$url = "merchant/view_pertanyaan/$id_merchant/";
			$link_url = $base_url.$url;
			
			
	

			$q = "insert into tbl_survey(judul,id_merchant,status,link_url,create_at) 
				values('".$judul."','".$id_merchant."','".$status."','".$link_url."',NOW())";
			$this->mod_main->create($q);
				
				
			
								
			redirect("merchant/survey");

	
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}


	function update_merchant()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);	
			$status					= $this->input->post('status', TRUE);

						
		
			
			
			$q_update 			= "update tbl_survey set 
			  judul				= '".$judul."'
			, id_merchant 		= '".$id_merchant."'  
			, status			= '".$status."'
			, update_at 		= NOW()
			where id_survey 	='".$id_survey."'";
			$this->mod_main->put($q_update);	
				

					
			redirect("merchant/survey");


			}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}
	
	
	
	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			
			
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);	
			$status					= $this->input->post('status', TRUE);

						
		
			
			
			$q_update 			= "update tbl_survey set 
			  judul				= '".$judul."'
			, id_merchant 		= '".$id_merchant."'  
			, status			= '".$status."'
			, update_at 		= NOW()
			where id_survey 	='".$id_survey."'";
			$this->mod_main->put($q_update);	
				

			
					
			redirect("sekolah/survey");


			}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

				
				
			
				
				
				
			$data["del"] 			= $this->model_merchant->hapus_konten($kode,"id_survey","tbl_survey");
			$data["del_pertanyaan"] = $this->model_merchant->hapus_konten($kode,"id_survey","tbl_polling");

			
			redirect("sekolah/survey");

		}else{

			redirect("sekolah/layanan");
		}	
		
	}


	





	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
