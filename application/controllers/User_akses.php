<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_akses extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	


	
	
	function update_profilesekolah()
	{
	
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		
			$nama_merchant			= $this->input->post("nama_merchant");
			$email					= $this->input->post("email");
			$dek					= $this->input->post('deskripsi', TRUE);	
			$deskripsi				= htmlspecialchars($dek, ENT_QUOTES);		
			$website				= $this->input->post("website");	
			$youtube				= $this->input->post("youtube");
			$pin_bb					= $this->input->post("pin_bb");	
			$wa						= $this->input->post("wa");
			$line					= $this->input->post("line");
		

	
			$q_update = "update tbl_merchant set 
			nama_merchant 	='".$nama_merchant."'
			,email 			='".$email."'
			,deskripsi 		='".$deskripsi."'
			
			,youtube		='".$youtube."'
			,website		='".$website."'		
			,pin_bb			='".$pin_bb."'
			,wa 			='".$wa."'
			,line 			='".$line."'
			
			,update_at 	= NOW() 
			where id_merchant ='".$id_merchant."'";
			$this->mod_main->put($q_update);

		

		redirect("direktorat/profile");	
		

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
		
	
	}
	
	
	
	
	
	
	
	
	function update_lokasisekolah()
	{
	
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$nama					= 	$data["nama_merchant"];
		$id	= "pusat";
		
		
		
		
		$id_cabang				= $this->input->post("id_cabang");
		echo $id_cabang;
		$nama_cabang			= $nama.' '.$id;
		$telp					= $this->input->post("telp");
		$dek					= $this->input->post('alamat', TRUE);	
		$alamat					= htmlspecialchars($dek, ENT_QUOTES);
		$latitude				= $this->input->post("inputLat");
		$longitude				= $this->input->post("inputLng");
		
		$status = "y";
		$pusat	= "pusat";
		
			$q = $this->model_sekolah->cek_data_cabang($id_merchant);
			if (count($q->result())>0){ 
				
				
					$q_update = "update tbl_cabang_merchant set 
					nama_cabang 	='".$nama_cabang."'
					,id_merchant 	='".$id_merchant."'
					,telp 			='".$telp."'
					,alamat			='".$alamat."'
					,latitude		='".$latitude."'
					,longitude		='".$longitude."'
					,status_cabang	='".$pusat."'
					,status			='".$status."'	
					,update_at 		= NOW() 
					where id_cabang='$id_cabang'";
					$this->mod_main->put($q_update);
		

				
			}else{

			//$q = "insert into tbl_cabang_merchant(id_cabang,nama_cabang,id_merchant,telp,alamat,latitude,longitude,status_cabang,status,create_at) 
				// values('".$id_cabang."','".$nama_cabang."','".$id_merchant."','".$telp."','".$alamat."','".$latitude."','".$longitude."','".$pusat."','".$status."',NOW())";
			//$this->mod_main->create($q);
					
					
					
			}		
		
		
		
		

		redirect("direktorat/profile");	
		

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
		
	
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
