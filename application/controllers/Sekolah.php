<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sekolah extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('captcha','date','text_helper'));
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	
		
	}
	
	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			
			$tgl = date("Y-m-d");
		$data["tgl"] = date("Y-m-d");
		$tanggal = date("m-d");
		
		
		
		$id_greeting = $this->uri->segment(3);
		
		
		
		$data["greeting_ultah"]			= $this->model_sekolah->get_dasboard_ultah($id_merchant,$tanggal);	
		$data["greeting_agama"]			= $this->model_sekolah->get_dasboard_agama($tgl,$id_greeting);	
		$data["greeting_nasional"]		= $this->model_sekolah->get_dasboard_nasional($tgl,$id_greeting);
		$data["greeting_budaya"]		= $this->model_sekolah->get_dasboard_budaya($tgl,$id_greeting);
			
	
		$data["jumlah_member"]		= $this->model_sekolah->get_jml_member($id_merchant);
		$data["jumlah_berita"]		= $this->model_sekolah->get_jml_berita($id_merchant);
		$data["jumlah_kalender"]	= $this->model_sekolah->get_jml_kalender($id_merchant);
		$data["jumlah_greeting"]	= $this->model_sekolah->get_jml_greeting($id_merchant);
		$data["jumlah_survey"]		= $this->model_sekolah->get_jml_survey($id_merchant);
		$data["jumlah_ekskul"]		= $this->model_sekolah->get_jml_ekskul($id_merchant);
		$data["jumlah_pegawai"]		= $this->model_sekolah->get_jml_kepegawaian($id_merchant);
		$data["jumlah_pres_sek"]	= $this->model_sekolah->get_jml_prestasi_sek($id_merchant);
		$data["jumlah_pres_alumni"]	= $this->model_sekolah->get_jml_prestasi_alumni($id_merchant);
		$data["jumlah_mapel"]		= $this->model_sekolah->get_jml_mapel($id_merchant);
		$data["jumlah_anak"]		= $this->model_sekolah->get_jml_anak($id_merchant);
		$data["jumlah_undangan"]	= $this->model_sekolah->get_jml_undangan($id_merchant);
		$data["jumlah_undangan"]	= $this->model_sekolah->get_jml_undangan($id_merchant);
		
		
		$profile					= $this->model_sekolah->get_profile_merchant($id_merchant);
		
		// Aktifitas  terbaru
		$data["member_terbaru"]		= $this->model_sekolah->get_member_terbaru($id_merchant);	
	
		
		
		
		foreach ($profile as $row)
		{
			$data["notiv"]= "".$row['create_at']."";
			
		}

		$data['page'] 				= 'sekolah/dasboard';
		$data['deskripsi'] 			= "Dasboard sekolah";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Dasboard sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		

		}else{

			$this->load->view('function/login_merchant');
		}
	}


	
	function anak_didik()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			
			 $kelas      = $this->input->post('kelas', TRUE);
			
			if(isset($_POST['cari'])){
				redirect("sekolah/anak_didik/$kelas");
				
			}
			
			$kode = "";
			
			$id_anak = $this->uri->segment(4);
			$data["anak_didik"]		=   $this->model_sekolah->get_anak_didik($id_merchant,$id_anak);
			$data["kelas"]			=	$this->model_sekolah->get_kelas_sekolah($id_merchant,$kode);	
			$data["wali_murid"]		=	$this->model_sekolah->get_member_merchant($id_merchant,$kode);
			
			
			
			
			
			
		$data['page'] 				= 'sekolah/anak_didik';
		$data['deskripsi'] 			= "Anak didik";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Anak didik";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{

				$this->load->view('function/login_merchant');
		}
	}	
	
	
	
	


	function staff()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			
			$id_pegawai = $this->uri->segment(4);
			$data["staff"]			=   $this->model_sekolah->get_guru_staff($id_merchant,$id_pegawai);


		$data['page'] 				= 'sekolah/guru_staff';
		$data['deskripsi'] 			= "Guru dan Staff";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Guru dan Staff";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{

				$this->load->view('function/login_merchant');
		}
	}	


	function prestasi_sekolah()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			$id_kelas ="";
			
			$id_prestasi = $this->uri->segment(4);
			$data["prestasi"]		=   $this->model_sekolah->get_prestasi_sekolah($id_merchant,$id_prestasi);
			

		$data['page'] 				= 'sekolah/prestasi_sekolah';
		$data['deskripsi'] 			= "Prestasi Sekolah";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Prestasi Sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{

				$this->load->view('function/login_merchant');
		}
	}	
	
	
	
		function prestasi_alumni()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			$id_kelas ="";
			
			$id_prestasi = $this->uri->segment(4);
			$data["prestasi"]		=   $this->model_sekolah->get_prestasi_alumni($id_merchant,$id_prestasi);
			

		$data['page'] 				= 'sekolah/prestasi_alumni';
		$data['deskripsi'] 			= "Prestasi Alumni";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Prestasi Alumni";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{

				$this->load->view('function/login_merchant');
		}
	}	
	
	
	function pengajar()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			
			$id_pengajar = $this->uri->segment(4);
			//$data["pengajar"]			=   $this->model_sekolah->get_pengajar($id_merchant,$id_pengajar);
			$data["nama_guru"]			=   $this->model_sekolah->get_nama_pengajar($id_merchant);
			
			$data["pilih_guru"]			=	$this->model_sekolah->get_pilih_guru($id_merchant);
			$data["kelas"]				= $this->model_sekolah->get_kelas($id_merchant);	
			$data["mapel"]				= $this->model_sekolah->get_mapel($id_merchant);
			


		$data['page'] 				= 'sekolah/pengajar';
		$data['deskripsi'] 			= "Pengajar Sekolah";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Pengajar Sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{

				$this->load->view('function/login_merchant');
		}
	}

	function hotnews()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_berita 					= $this->uri->segment(4);
		$data["hotnews"]				= $this->model_sekolah->get_info_sekolah($id_merchant,$id_berita);	
		$id_member = "";
		$data["nama_wali"]			= $this->model_sekolah->get_orangtua($id_merchant,$id_member);	
		$data["kelas"]				= $this->model_sekolah->get_kelas($id_merchant);	

		$data['page'] 				= 'sekolah/hotnews';
		$data['deskripsi'] 			= "Pengumuman sekolah";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Pengumuman sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	function direktorat()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_direktorat 					= $this->uri->segment(4);
		
		$data["direktorat"]				= $this->model_sekolah->get_direktorat($id_merchant,$id_direktorat);	
		$id_member = "";
		

		$data['page'] 				= 'sekolah/direktorat';
		$data['deskripsi'] 			= "Direktorat";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Direktorat";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	function undangan()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_undangan					= $this->uri->segment(4);
		$data["undangan"]				= $this->model_sekolah->get_undangan($id_merchant,$id_undangan);	
		
		$id_member = "";
		$data["nama_wali"]			= $this->model_sekolah->get_orangtua($id_merchant,$id_member);	
		$data["kelas"]				= $this->model_sekolah->get_kelas($id_merchant);	
		$data["kehadiran"]				= $this->model_sekolah->get_kehadiran_wali($id_merchant,$id_undangan);
		$data['page'] 				= 'sekolah/undangan';
		$data['deskripsi'] 			= "Undangan sekolah";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Undangan sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	
	function kelas()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
		$session = "sekolah";
		$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
		$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_kelas					= $this->uri->segment(4);
		
		$data["kelas"]				= $this->model_sekolah->get_kelas($id_merchant,$id_kelas);	
		
	
	
		$data['page'] 				= 'sekolah/kelas';
		$data['deskripsi'] 			= "Kelas Sekolah";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Kelas Sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	function walikelas()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_kelas					= $this->uri->segment(4);
		
		
		$data["kelas"]				= $this->model_sekolah->get_kelas($id_merchant,$id_kelas);
	
		$data["walikelas"]				= $this->model_sekolah->get_kelas_sekolah($id_merchant,$id_kelas);
		$data["pilih_guru"]			=	$this->model_sekolah->get_pilih_guru($id_merchant);
	
		$data['page'] 				= 'sekolah/walikelas';
		$data['deskripsi'] 			= "Walikelas Sekolah";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Kelas Sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}

	
	function mapel()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			
			
		$id_mapel					= $this->uri->segment(4);
		$data["mapel"]				= $this->model_sekolah->get_mapel_sekolah($id_merchant,$id_mapel);	
		
		
	
		$data['page'] 				= 'sekolah/mapel';
		$data['deskripsi'] 			= "Mapel Sekolah";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Mapel Sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}

	function kehadiran()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$id_agenda 				=   $this->uri->segment(4);
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			
			
			$id_kehadiran					= $this->uri->segment(4);
			$data["kehadiran"]			= $this->model_sekolah->get_kehadiran($id_merchant,$id_kehadiran);		
			

		$data['page'] 				= 'sekolah/kehadiran';
		$data['deskripsi'] 			= "Kehadiran Walimurid";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Kehadiran Walimurid";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}


	function ekskul()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
				

			$id_ekskul 				= $this->uri->segment(4);
			$data["ekskul"]			= $this->model_sekolah->get_ekskul_sekolah($id_merchant,$id_ekskul);		
				

		$data['page'] 				= 'sekolah/ekskul';
		$data['deskripsi'] 			= "Ekskul sekolah";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Ekstrakulikuler sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	



	}else{

			$this->load->view('function/login_merchant');
		}
	}	


	function akademik()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
				

			$id_kalender 				= $this->uri->segment(4);
			$data["akademik"]			= $this->model_sekolah->get_kalender_sekolah($id_merchant,$id_kalender);		
				

		$data['page'] 				= 'sekolah/kalender';
		$data['deskripsi'] 			= "Akademik sekolah";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Kalender sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	



	}else{

			$this->load->view('function/login_merchant');
		}
	}	

	function wali()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			$id_member				= 	$this->uri->segment(4);	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			//$link_url = "";	

			$data['page'] 				= 'sekolah/member';
			$data['deskripsi'] 			= "Member sekolah";

			$data["member"]				= $this->model_sekolah->get_member_merchant($id_merchant,$id_member);

			$data['header_keyword'] 	= "";
			$data['header_desc'] 		= "";
			$data['header_title'] 		= "Member sekolah";

			$this->load->view('header',$data);
			$this->load->view('main_header',$data);	
			$this->load->view('main_left',$data);
			$this->load->view('main_menu',$data);
			$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}

	
	function layanan()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$id_katalog				= 	$this->uri->segment(4);	
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);


		$data['page'] 				= 'sekolah/layanan';
		$data['deskripsi'] 			= "Layanan sekolah";

		$data["layanan"]			= $this->model_sekolah->get_katalog_merchant($id_merchant,$id_katalog);
		

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Layanan Merchant";
		

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}

	function greeting()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_category = "1";
		
		$id_greeting					= $this->uri->segment(4);
		$data["greeting"]				= $this->model_sekolah->get_greeting_sekolah($id_greeting);	
			


		$data['page'] 				= 'sekolah/greeting';
		$data['deskripsi'] 			= "Greeting sekolah";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Greeting sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	


	function Komentar()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$id_member				= 	$this->uri->segment(4);	

			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);

		$data['page'] 				= 'sekolah/komentar';
		$data['deskripsi'] 			= "Komentar sekolah";

		$data["komentar_diskon"]	= $this->model_sekolah->get_komentar_diskon($id_merchant);
		$data["komentar_berita"]	= $this->model_sekolah->get_komentar_berita($id_merchant);

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Komentar sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}


	function report()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$data['page'] 				= 'merchant/report';
		$data['deskripsi'] 			= "Report Merchant";

		//$data["profile"]			= $this->model_sekolah->get_profile_merchant($id_merchant);
		

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Report Merchant";


		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	function survey()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		//$id_jawaban 			= $this->uri->segment(5);
		
		$id_survey 				= $this->uri->segment(4);
		
		$data["survey"]			= $this->model_sekolah->get_survey($id_merchant,$id_survey);
		
		
		//$data["jawaban_det"]	= $this->model_sekolah->get_jawaban_det($id_merchant,$id_jawaban);
		
		
		$data['page'] 				= 'sekolah/survey';
		$data['deskripsi'] 			= "Survey sekolah";
		
		
		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Survey sekolah";


		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
		
	}	
	
	
	function pertanyaan()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
			$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_polling 			= $this->uri->segment(4);
		$id_survey 				= $this->uri->segment(3);
		$data["pertanyaan"]		= $this->model_sekolah->get_pertanyaan($id_merchant,$id_survey,$id_polling);
		$data["survey"]			= $this->model_sekolah->get_survey($id_merchant,$id_survey);
		
		
		$data['page'] 				= 'sekolah/daftar_pertanyaan_sekolah';
		$data['deskripsi'] 			= "Pertanyaan Survey sekolah";
		
		
		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Pertanyaan Survey sekolah";


		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	
		
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}	
		
	}
	
	function view_pertanyaan()
	{
		
		
		$id_merchant 			= $this->uri->segment(3);
		$id_survey 				= $this->uri->segment(4);
		$id_member 				= $this->uri->segment(5);
		
		$data["polling"]			= $this->model_sekolah->get_pertanyaan_polling($id_merchant,$id_survey,$id_member);
		
		
		$this->load->view('sekolah/view_pertanyaan',$data);


		

		
	}	
	
	
	function download()
	{
		
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		
		$id_survey 				= $this->uri->segment(3);
		$data["survey"]			= $this->model_sekolah->get_survey($id_merchant,$id_survey);
		$data["hasil"]			= $this->model_sekolah->get_hasil_survey($id_merchant,$id_survey);
		$nama					= $data["survey"][0]["judul"];
		$data["nama"]			= $this->model_sekolah->convert_name($nama);
		
				
		
		$this->load->view('sekolah/surveyexel',$data);


		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
		
	}	
	
	function terima_kasih()
	{
		
		$this->load->view('sekolah/terima_kasih');
	}	
	
	function Profile()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);


		$data['page'] 				= 'sekolah/profile';
		$data['deskripsi'] 			= "Profile sekolah";

		$data["profile"]			= $this->model_sekolah->get_profile_merchant($id_merchant);
		

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Profile sekolah";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}


	

	

	function ubah_password()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);


		$data['page'] 				= 'sekolah/ubah_password';
		$data['deskripsi'] 			= "Ubah Password sekolah";

		//$data["profile"]			= $this->model_sekolah->get_profile_merchant($id_merchant);
		

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Ubah Password sekolah";
		

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	function location()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$pusat = "pusat";
		$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$data["profile"]			= $this->model_sekolah->get_profile_merchant($id_merchant);
		$data["cabang"]				= $this->model_sekolah->get_cabang_pusat($id_merchant,$pusat);

		$data['page'] 				= 'sekolah/lokasi';
		$data['deskripsi'] 			= "Update Lokasi sekolah";


		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Update Lokasi sekolah";
		

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	

	function login()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){
			redirect("sekolah");	
		}
		else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}



	function logout()
	{
		session_destroy();
		redirect("sekolah");		
	}
	
	
	// update password
	
	
	function put_password()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "sekolah";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);

		$pwd_old	= $this->input->post('password', TRUE);
		$pwd_baru	= $this->pass_word->encryptPass($this->input->post('passwordbaru', TRUE));
		$pwdb		= $this->input->post('passwordbaru', TRUE);
		$pwdu		= $this->input->post('passwordulang', TRUE);

						$sql	= "SELECT pass FROM tbl_merchant
									WHERE
									id_merchant	= '$id_merchant'
									";
									//die($sql);
						$rec	= $this->db->query($sql);
						$count	= $rec->num_rows();
						$res	= $rec->result_array();
						if($count>0){
							if($this->pass_word->decryptPass($res[0]['pass'])==$pwd_old){

									if($pwdb == $pwdu){

							 	
										$q_update = "update tbl_merchant set pass ='".$pwd_baru."' , update_at = NOW() where id_merchant ='".$id_merchant."'";
										$this->mod_main->put($q_update);
										
										echo "<script>alert('Update password berhasil..');</script>";
										echo'<script type="text/javascript">location.href="./profile";</script>';

									}else{

									
										
										echo"<script>alert('Password Ditolak....');</script>";
										echo'<script type="text/javascript">location.href="./ubah_password";</script>';

									}			
							
							}else{
							
							echo"<script>alert('Update password gagal....');</script>";
							echo'<script type="text/javascript">location.href="./ubah_password";</script>';
							
							

							}
						}

		}else{
			
			$this->load->view('function/login_merchant');
			
		}				


	}
	
	
	
	
	
	
	


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
