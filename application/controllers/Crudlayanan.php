<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudlayanan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!="")
		{
		
			redirect("instansi/layanan");
		

		}else{

			$this->load->view('function/login_merchant');
			
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			$produk					= $this->input->post('produk', TRUE);
			$harga					= $this->input->post('harga', TRUE);	
			$deskripsi				= $this->input->post('deskripsi', TRUE);
			$status					= $this->input->post('status', TRUE);
	
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'katalog/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			
			if(!empty($asli)){

			$config["file_name"]			= $photo;
			$config['upload_path'] 			= './assets/upload/katalog/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] = TRUE;
			$config['max_size'] = '0';
			$config['max_width']  = '0';
			$config['max_height']  = '0'; 					
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
				
	// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
							
				
				

				$thumbr = $thumb; 
				$q = "insert into tbl_katalog(produk,id_merchant,deskripsi,status,photo,create_at) 
					values('".$produk."','".$id_merchant."','".$deskripsi."','".$status."','".$thumbr."',NOW())";
				$this->mod_main->create($q);
				
				
				}else{ 
				
				$q = "insert into tbl_katalog(produk,id_merchant,deskripsi,status,create_at) 
					values('".$produk."','".$id_merchant."','".$deskripsi."','".$status."',NOW())";
				$this->mod_main->create($q);
				
				
				
				
				}	

				
			
								
			redirect("instansi/layanan");

	
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			$id_katalog 			= $this->uri->segment(3);
			$produk					= $this->input->post('produk', TRUE);
			$harga					= $this->input->post('harga', TRUE);
			$deskripsi				= $this->input->post('deskripsi', TRUE);
			$status					= $this->input->post('status', TRUE);
			
			$gbr 		= $this->input->post('gbr');
	
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'katalog/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			
			if(!empty($asli)){

			$config["file_name"]			= $photo;
			$config['upload_path'] 			= './assets/upload/katalog/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] = TRUE;
			$config['max_size'] = '0';
			$config['max_width']  = '0';
			$config['max_height']  = '0'; 					
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
		
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
						
			
			$thumbr = $thumb;
			
			
			$q_update = "update tbl_katalog set 
			  produk			= '".$produk."'
			, id_merchant 		= '".$id_merchant."'  
			, deskripsi			= '".$deskripsi."'
			, status			= '".$status."'
			, photo 			= '".$thumbr."'
			, update_at 		= NOW()
			where id_katalog 	='".$id_katalog."'";
			$this->mod_main->put($q_update);	
			
					if(!empty($gbr))
					{

					$file_gambar  = './assets/upload/katalog/'.$gbr;
					unlink($file_gambar);
			
					}
			

			 }else{

			$q_update = "update tbl_katalog set 
			  produk			= '".$produk."'
			, id_merchant 		= '".$id_merchant."'  
			
			, deskripsi			= '".$deskripsi."'
			, status			= '".$status."'
			
			, update_at 		= NOW()
			where id_katalog 	='".$id_katalog."'";
			$this->mod_main->put($q_update);	
			 
			 
			 }	

			

			
					
			redirect("instansi/layanan");


			}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!=""){

				
				
				$info = $this->model_merchant->delete_katalog_merchant($kode);
				foreach ($info as $data) 
				{

					$photo = "".$data['photo']."";

				}	
				
				if($photo != "")
				{	
				$gambar  = './assets/upload/katalog/'.$photo;
				unlink($gambar);
				}
				
				
				
			$data["del"] = $this->model_merchant->hapus_konten($kode,"id_katalog","tbl_katalog");
			redirect("instansi/layanan");

		}else{

			redirect("instansi/layanan");
		}	
		
	}


	





	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
