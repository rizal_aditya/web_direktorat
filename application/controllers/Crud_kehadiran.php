<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_kehadiran extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			
			redirect("sekolah/hotnews");
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			
				

			$judul					= $this->input->post('judul', TRUE);					
		
			$dek					= $this->input->post('isi', TRUE);
			$isi					= htmlspecialchars($dek, ENT_QUOTES);
		
			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tipe					= 	$this->input->post('tipe', TRUE);
			$id_member				= 	$this->input->post('nama_member', TRUE);
			$id_kelas				= 	$this->input->post('nama_kelas', TRUE);
			
			
		
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'berita/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;

			if(!empty($asli)){ 
			
			
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/berita/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] 			= TRUE;
			$config['max_size'] 			= '0';
			$config['max_width'] 			= '0';
			$config['max_height'] 			= '0'; 						
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
			$thumbr = $thumb;

					$q = "insert into tbl_berita(judul,id_merchant,isi,status,photo,broadcast,tipe,create_at) 
					 values('".$judul."','".$id_merchant."','".$isi."','".$status."','".$thumbr."','".$broadcast."','".$tipe."',NOW())";
					$this->mod_main->create($q);
					
					
					


			}else{ 
			
					$q = "insert into tbl_berita(judul,id_merchant,isi,status,broadcast,tipe,create_at) 
					 values('".$judul."','".$id_merchant."','".$isi."','".$status."','".$broadcast."','".$tipe."',NOW())";
					$this->mod_main->create($q);
			
			 }		

		
					
				
					
					
					
					$data_id = $this->model_sekolah->get_IDBerita_sekolah($id_merchant);				
					foreach ($data_id as $ray) 
					{
						$id_berita 	= "".$ray['id_berita']."";
						$judul 		= "".$ray['judul']."";
						$isi   		= "".$ray['isi']."";
						$status 	= "".$ray['status']."";
						$photo 		= "".$ray['photo']."";
						$create_at 	= "".$ray['tgl_create']."";
						$merchant 	= "".$ray['nama_merchant']."";
					}		

						
							
					$message = array(
					"idnews" 				=> $id_berita,
					"judul" 				=> $judul,
					"isi" 					=> $isi,
					"status" 				=> $status,
					"photo" 				=> $photo,
					"create" 				=> $create_at,
					"merchant" 				=> $merchant,
					"param"					=> "berita",
					"sender"				=> $sender);
	
					

					
				
				$api_key = $api_merchant;	
				if($broadcast == "y" && $tipe == "pribadi")
				{
					
					// send gcm private	
					$member = $this->model_sekolah->get_orangtua($id_merchant,$id_member);	
					foreach ($member as $data)
					{
						$reg_id = "".$data['gcm']."";
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
						
						
					}	
					
				}
				
				else if($broadcast == "y" && $tipe == "kelas")
				{
					
					// send gcm sekolah
					$sekolah = $this->model_sekolah->get_kelas_anak($id_merchant,$id_kelas);
					foreach ($sekolah as $data)
					{
						$reg_id = "".$data['gcm']."";
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
						
						
					}	

				}else{
					//send gcm all
					$member = $this->model_sekolah->get_gcm_member();
					foreach ($member as $data)
					{
						$reg_id = "".$data['gcm']."";
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
					}					
				}	
				
	
					
				redirect("sekolah/hotnews");									
				
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
		
			
			$id_berita 				= 	$this->uri->segment(3);
			$judul					= 	$this->input->post('judul', TRUE);
			$dek					= $this->input->post('isi', TRUE);
			$isi					= htmlspecialchars($dek, ENT_QUOTES);
			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tipe					= 	$this->input->post('tipe', TRUE);
			$id_member					= 	$this->input->post('nama_member', TRUE);
			$id_kelas					= 	$this->input->post('nama_kelas', TRUE);
			$gbr 		= $this->input->post('gbr');
	
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'berita/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			
			if(!empty($asli)){
			
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/berita/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] = TRUE;
			$config['max_size'] = '0';
			$config['max_width']  = '0';
			$config['max_height']  = '0'; 							
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
			
			$thumbr = $thumb;
			
			
			 	
			$q_update = "update tbl_berita set 
			judul = '".$judul."'
			, id_merchant = '".$id_merchant."'
			, isi = '".$isi."'
			, status = '".$status."'
			, broadcast = '".$broadcast."'
			, tipe 		= '".$tipe."'
			, photo = '".$thumbr."'
			, update_at = NOW() 
			where id_berita  	='".$id_berita."'";
			$this->mod_main->put($q_update);
				
					if(!empty($gbr))
					{
						$file_gambar  = './assets/upload/berita/'.$gbr;
						unlink($file_gambar);
					}
			
			}else{
				
			

			$q_update = "update tbl_berita set 
			judul = '".$judul."'
			, id_merchant = '".$id_merchant."'
			, isi = '".$isi."'
			, status = '".$status."'
			, broadcast = '".$broadcast."' 
			, tipe 		= '".$tipe."'
			, update_at = NOW() 
			where id_berita  	='".$id_berita."'";
			$this->mod_main->put($q_update);
				
			}		
			
			
									

			$data_id = $this->model_sekolah->get_IDBerita_sekolah($id_merchant);				
					foreach ($data_id as $ray) 
					{
						$id_berita 	= "".$ray['id_berita']."";
						$judul 		= "".$ray['judul']."";
						$isi   		= "".$ray['isi']."";
						$status 	= "".$ray['status']."";
						$photo 		= "".$ray['photo']."";
						$create_at 	= "".$ray['tgl_create']."";
						$merchant 	= "".$ray['nama_merchant']."";
					}		

						
							
					$message = array(
					"idnews" 				=> $id_berita,
					"judul" 				=> $judul,
					"isi" 					=> $isi,
					"status" 				=> $status,
					"photo" 				=> $photo,
					"create" 				=> $create_at,
					"merchant" 				=> $merchant,
					"param"					=> "berita",
					"sender"				=> $sender);
	
					

					
				
				$api_key = $api_merchant;	
				if($broadcast == "y" && $tipe == "pribadi")
				{
					
					// send gcm private	
					$member = $this->model_sekolah->get_orangtua($id_merchant,$id_member);	
					foreach ($member as $data)
					{
						$reg_id = "".$data['gcm']."";
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
						
						
					}	
					
				}
				
				else if($broadcast == "y" && $tipe == "kelas")
				{
					
					// send gcm sekolah
					$sekolah = $this->model_sekolah->get_kelas_anak($id_merchant,$id_kelas);
					foreach ($sekolah as $data)
					{
						$reg_id = "".$data['gcm']."";
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
						
						
					}	

				}else{
					//send gcm all
					$member = $this->model_sekolah->get_gcm_member();
					foreach ($member as $data)
					{
						$reg_id = "".$data['gcm']."";
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
					}					
				}		
			
				
					redirect("sekolah/hotnews");	
					


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			

			

			

			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_kehadiran","tbl_konf_kehadiran");
			
			
			redirect("sekolah/kehadiran");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	


	function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
