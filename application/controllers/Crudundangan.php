<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudundangan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			
			redirect("sekolah/undangan");
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			
				

			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);
			$dek					= $this->input->post('deskripsi', TRUE);	
			$deskripsi				= htmlspecialchars($dek, ENT_QUOTES);
			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tipe					= 	$this->input->post('tipe', TRUE);
			$id_member				= 	$this->input->post('nama_member', TRUE);
			$id_kelas				= 	$this->input->post('nama_kelas', TRUE);
			
			
		
		
			
			$q = "insert into tbl_undangan(judul,id_merchant,deskripsi,status,broadcast,tipe,create_at) 
			 values('".$judul."','".$id_merchant."','".$deskripsi."','".$status."','".$broadcast."','".$tipe."',NOW())";
			$this->mod_main->create($q);
			
		

	
					
					
					
					


						$id_undangan = "";
						$data["undangan"] = $this->model_sekolah->get_IDundangan_sekolah($id_merchant,$id_undangan);				
						$id_undangan 	= $data["undangan"][0]['id_berita'];
						$judul 			= $data["undangan"][0]['judul'];
						$deskripsi  	= $data["undangan"][0]['deskripsi'];
						$status 		= $data["undangan"][0]['status'];
						$create_at 		= $data["undangan"][0]['tgl_create'];
						$merchant 		= $data["undangan"][0]['nama_merchant'];	

						
							
						$message = array(
						"idundangan" 			=> $id_undangan,
						"judul" 				=> $judul,
						"deskripsi" 			=> $deskripsi,
						"status" 				=> $status,
						"create" 				=> $create_at,
						"merchant" 				=> $merchant,
						"param"					=> "undangan",
						"sender"				=> $sender);
		
					

					
				
				$api_key = $api_merchant;	
				if($broadcast == "y" && $tipe == "pribadi")
				{
					
					
					// send gcm private	
						$data["member"] = $this->model_sekolah->get_orangtua($id_merchant,$id_member);	
						$reg_id  		= $data["member"][0]['gcm'];
						
						$nama_member 	= $data["member"][0]['nama_member'];
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
					
				}
				
				else if($broadcast == "y" && $tipe == "kelas")
				{
					
					// send gcm sekolah
					$data["sekolahe"] = $this->model_sekolah->get_kelas_anak($id_merchant,$id_kelas);
					$id_member  	= $data["sekolahe"][0]["id_member"];
					$reg_id 				= $data["sekolahe"][0]["gcm"];
					$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);

				}else{
						//send gcm all
						$all = $this->model_sekolah->get_gcm_member($id_merchant);
						foreach ($all as $row)
						{
							$reg_id 		= "".$row['gcm']."";
							$nama_member	= "".$row['nama_member']."";
							$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);	
						}			
				}	
				
	
					
				redirect("sekolah/undangan");									
				
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
		
			
			$id_undangan			= 	$this->uri->segment(3);
			$jdl					= $this->input->post('judul', TRUE);		
			$judul    				= htmlspecialchars($jdl, ENT_QUOTES);
			$dek					= $this->input->post('deskripsi', TRUE);	
			$deskripsi				= htmlspecialchars($dek, ENT_QUOTES);
			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tipe					= 	$this->input->post('tipe', TRUE);
			$id_member				= 	$this->input->post('nama_member', TRUE);
			$id_kelas				= 	$this->input->post('nama_kelas', TRUE);
			
	
		
			

			$q_update = "update tbl_undangan set 
			judul = '".$judul."'
			, id_merchant = '".$id_merchant."'
			, deskripsi = '".$deskripsi."'
			, status = '".$status."'
			, broadcast = '".$broadcast."' 
			, tipe 		= '".$tipe."'
			, update_at = NOW() 
			where id_undangan  	='".$id_undangan."'";
			$this->mod_main->put($q_update);
				
			
			
			
									


					
						$data["undangan"] = $this->model_sekolah->get_IDundangan_sekolah($id_merchant,$id_berita);				
						$id_berita 	= $data["undangan"][0]['id_berita'];
						$judul 		= $data["undangan"][0]['judul'];
						$deskripsi	= $data["undangan"][0]['deskripsi'];
						$status 	= $data["undangan"][0]['status'];
						$create_at 	= $data["undangan"][0]['tgl_create'];
						$merchant 	= $data["undangan"][0]['nama_merchant'];
						

						
							
					$message = array(
					"idundangan" 			=> $id_undangan,
					"judul" 				=> $judul,
					"deskripsi" 			=> $deskripsi,
					"status" 				=> $status,
					"create" 				=> $create_at,
					"merchant" 				=> $merchant,
					"param"					=> "undangan",
					"sender"				=> $sender);
	
					

					
				
				$api_key = $api_merchant;	
				if($broadcast == "y" && $tipe == "pribadi")
				{
					
					
					// send gcm private	
						$data["member"] = $this->model_sekolah->get_orangtua($id_merchant,$id_member);	
						$reg_id  		= $data["member"][0]['gcm'];
						
						$nama_member 	= $data["member"][0]['nama_member'];
						$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
					
				}
				
				else if($broadcast == "y" && $tipe == "kelas")
				{
					
					// send gcm sekolah
					$data["sekolahe"] = $this->model_sekolah->get_kelas_anak($id_merchant,$id_kelas);
					$id_member  	= $data["sekolahe"][0]["id_member"];
					$reg_id 				= $data["sekolahe"][0]["gcm"];
					$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);

				}else{
						//send gcm all
						$all = $this->model_sekolah->get_gcm_member($id_merchant);
						foreach ($all as $row)
						{
							$reg_id 		= "".$row['gcm']."";
							$nama_member	= "".$row['nama_member']."";
							$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);	
						}			
				}			
			
				
					redirect("sekolah/undangan");	
					


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
		

			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_undangan","tbl_undangan");
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_undangan","tbl_konf_kehadiran");
			
			redirect("sekolah/undangan");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	function delete_kehadiran()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
		

			
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_undangan","tbl_konf_kehadiran");
			
			redirect("sekolah/undangan");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
