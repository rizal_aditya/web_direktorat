<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudjawaban extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		
	}

	function index()
	{
		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!="")
		{
				redirect("instansi/survey");
				
		}else{

				$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"]	;
			$id_merchant			= 	$data["id_merchant"];

			$id_survey				= 	$this->input->post('id_survey', TRUE);
			$id_pertanyaan			= 	$this->input->post('id_pertanyaan', TRUE);
			$jawaban				= 	$this->input->post('jawaban', TRUE);
			

			
			
		
			 
			 $q = "insert into tbl_survey_jawaban(id_pertanyaan,jawaban,id_survey,create_at) 
		  values('".$id_pertanyaan."','".$jawaban."','".$id_survey."',NOW())";
			$this->mod_main->create($q);

			
				redirect("instansi/survey/pertanyaan/$id_survey");
				
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"]	;
			$id_merchant			= 	$data["id_merchant"];
			
			$id_jawaban					= 	$this->uri->segment(3);
			$id_pertanyaan			= 	$this->input->post('id_pertanyaan', TRUE);
			$jawaban				= 	$this->input->post('jawaban', TRUE);
			$id_survey				= 	$this->input->post('id_survey', TRUE);
			
			
			
		
			$q_update 			= "update tbl_survey_jawaban set 
			id_pertanyaan 			= '".$id_pertanyaan."'
			, jawaban			= '".$jawaban."'
			, id_survey			= '".$id_survey."'
			, update_at 		= NOW() 
			where id_jawaban  ='".$id_jawaban."'";
			$this->mod_main->put($q_update);	
			
							
			redirect("instansi/survey/pertanyaan/$id_survey");	


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	


	


	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
