<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudmember extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			
			redirect("direktorat/pegawai");	
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			$url					= 	$this->config->item('base_url');	
			
			
			

			$les						= 	$this->input->post('nama_member', TRUE);
			$nama_member 				=  htmlspecialchars($les, ENT_QUOTES);
			$status						= 	$this->input->post('status', TRUE);
			$dek					= $this->input->post('alamat', TRUE);	
			$alamat					= htmlspecialchars($dek, ENT_QUOTES);
			$telp						= 	$this->input->post('telp', TRUE);
			$email						= 	$this->input->post('email', TRUE);
			$tgl_lahir					= 	$this->input->post('tgl_lahir', TRUE);
			$type						= 	$this->input->post('type', TRUE);
			$nip						= 	$this->input->post('nip', TRUE);
			$jabatan					= 	$this->input->post('jabatan', TRUE);
			$id_cabang					= 	$this->input->post('id_cabang', TRUE);
			$negara 					= "indonesia";
			$photo 						= "assets/member/no-image.jpg";
			$default = "vcard";
			$password 					= 	$this->pass_word->encryptPass($default);
			$date = date_create($tgl_lahir);
			$tanggal = date_format($date,"m-d");
			
			
			
				
			
			
			
		$q = $this->model_sekolah->cek_email_guru($email,$id_merchant);
		if (count($q->result())>0){
			
			echo"<script>alert('Data email staff  sudah digunakan !!!');</script>";
			
			echo'<script type="text/javascript">location.href="'.$url.'/direktorat/pegawai";</script>';
		}else{	
				
			
					$q = "insert into tbl_member(
					nama_member
					,id_merchant
					,telp
					,email
					,tgl_lahir
					,type
					,status
					,negara
					,tanggal
					,alamat
					,nip
					,jabatan
					,id_cabang
					,photo
					,pass
					,create_at) 
					 values(
					 '".$nama_member."'
					 ,'".$id_merchant."'
					 ,'".$telp."'
					 ,'".$email."'
					 ,'".$tgl_lahir."'
					 ,'".$type."'
					 ,'".$status."'
					 ,'".$negara."'
					 ,'".$tanggal."'
					 ,'".$alamat."'
					 ,'".$nip."'
					 ,'".$jabatan."'
					 ,'".$id_cabang."'
					 ,'".$photo."'
					 ,'".$password."'
					 ,NOW())";
					$this->mod_main->create($q);
			
			
				redirect("direktorat/pegawai");	
			 

			

		
			
				 }									
				
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_sekolah"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
			$id_member					= 	$this->uri->segment(3);
			$les						= 	$this->input->post('nama_member', TRUE);
			$nama_member 				=  htmlspecialchars($les, ENT_QUOTES);
			$status						= 	$this->input->post('status', TRUE);
			$dek					= $this->input->post('alamat', TRUE);	
			$alamat					= htmlspecialchars($dek, ENT_QUOTES);
			$telp						= 	$this->input->post('telp', TRUE);
			$email						= 	$this->input->post('email', TRUE);
			$tgl_lahir					= 	$this->input->post('tgl_lahir', TRUE);
			$type						= 	$this->input->post('type', TRUE);
			$nip						= 	$this->input->post('nip', TRUE);
			$jabatan					= 	$this->input->post('jabatan', TRUE);
			$id_cabang					= 	$this->input->post('id_cabang', TRUE);
			$negara 					= "indonesia";
			$date = date_create($tgl_lahir);
			$tanggal = date_format($date,"m-d");

			

			$q_update = "update tbl_member set 
			nama_member 		= '".$nama_member."'
			, id_merchant 	= '".$id_merchant."'
			, telp 			= '".$telp."'
			, email			= '".$email."'
			, tgl_lahir 	= '".$tgl_lahir."'
			, type 			= '".$type."'
			, status 		= '".$status."'
			, alamat 		= '".$alamat."'
			, negara		= '".$negara."'
			, tanggal 		= '".$tanggal."'
			, nip			= '".$nip."'
			, jabatan		= '".$jabatan."'
			, id_cabang		= '".$id_cabang."'
			, update_at = NOW() 
			where id_member 	='".$id_member."'";
			$this->mod_main->put($q_update);
						
									
			redirect("direktorat/pegawai");
					
				


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			

			$info = $this->model_sekolah->delete_guru_sekolah($kode);
			foreach ($info as $data) 
			{

				$photo = "".$data['photo']."";

			}	

			if($photo != "")
			{	
			$gambar  = './assets/upload/pegawai/'.$photo;
			unlink($gambar);
			}

			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_pegawai","tbl_pegawai");
			
			redirect("sekolah/staff");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	


	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
