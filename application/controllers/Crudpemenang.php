<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crudpemenang extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		
	}
	
	
	function index()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
			$id_kuis 				=   $this->input->post('id_kuis', TRUE);
			$id_member					= $this->input->post('id_member', TRUE);
			$id_jawaban					= $this->input->post('id_jawaban', TRUE);
			$ucapan						= $this->input->post('isi', TRUE);
			$id_merchant				= $this->input->post('id_merchant', TRUE);		
			//$tgl_berlaku				= $this->input->post('tgl_berlaku', TRUE);
			
			
			
			
		$cek = $this->db->where('id_jawaban',$id_jawaban)->get("tbl_pemenang");
        if($cek->num_rows()==0)
		{
            
			
			$q = "insert into tbl_pemenang(id_member,id_kuis,id_jawaban,isi,id_merchant,create_at) 
			 values('".$id_member."','".$id_kuis."','".$id_jawaban."','".$ucapan."','".$id_merchant."',NOW())";
			$this->mod_main->create($q);
			
			
			$data_id = $this->model_merchant->get_pemenang_send($id_merchant,$id_jawaban);				
					foreach ($data_id as $ray) 
					{
						$id_pemenang 	= "".$ray['id_pemenang']."";
						$judul_kuis  	= "".$ray['judul_kuis']."";
						$isi 			= "".$ray['isi']."";
						$create_at		= "".$ray['create_at']."";	
						$merchant 		= "".$ray['id_pemenang']."";
					}	
					
						$message = array(
						"judul_kuis" 			=> $judul_kuis,
						"isi" 					=> $isi,
						"tanggal" 				=> $create_at,
						"merchant" 				=> $merchant,
						"param"					=> "pemenang",
						"sender"				=> $sender
						);	
										
									$api_key = $api_merchant;
									$member = $this->model_merchant->get_pemenang_member($id_merchant,$id_member);
									foreach ($member as $data)
									{
										$reg_id = "".$data['gcm']."";
										//echo $reg_id;
										
										$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
										
										
									}	
			
		}
		else
		{
			
			
			
					
									
									
						redirect("merchant/kuis/hasil/$id_kuis");			
			
			
		}
						
			
			
			
			
			redirect("merchant/kuis/hasil/$id_kuis");	
			
			
		}else{

			echo "<meta http-equiv='refresh' content='0; url=".base_url()."merchant/login'>";
		}	
	}

	
	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!=""){

			$pemenang = $this->model_merchant->delete_pemenang_kuis($kode);
			foreach ($pemenang as $data) 
			{

				$id_kuis = "".$data['id_kuis']."";

			}	


			$data["del"] = $this->model_merchant->hapus_konten($kode,"id_pemenang","tbl_pemenang");
			
			
			redirect("merchant/kuis/hasil/$id_kuis");

		}else{

			redirect("merchant/");
		}	
		
	}
				
	
	


	function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
