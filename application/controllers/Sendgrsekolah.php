<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sendgrsekolah extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}
	
	function greeting_brithday()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
			$kategori 		= $this->uri->segment(3);	
			$id_greeting 	= $this->uri->segment(4);	
			
			
			//tgl sekarang
			$tanggal = date("m-d");
			$agama_gret ="";
			$tgl_lahir = "";
			$tgl = date("Y-m-d");
			// API Key
			$api_key = $api_merchant;
									
			
		
			$date_database = $this->model_sekolah->get_greeting_member($id_merchant,$agama_gret,$tanggal);			
			foreach ($date_database as $row)
			{
					// tgl brithday
					$thn_skrg = date('Y');
					$tgl_lahir = "".$row['tgl_lahir']."";
					$tgl_conversi = date_format(date_create($tgl_lahir), 'm-d');
					$thn_lahir = date_format(date_create($tgl_lahir), 'Y');
					$member = "".$row['nama_member']."";
					
					$usia = $thn_skrg - $thn_lahir;
					$reg_id = "".$row['gcm']."";
					
					
					//echo $member;
					
					
				//kondisi jika sama dengan tanggal lahir	
				if($tanggal == $tgl_conversi)
				{
			
					
						
					
					
						$date_database = $this->model_sekolah->get_greeting($kategori,$id_greeting);
						foreach ($date_database as $row)
						{
							// get brithday
							$aku = "yang ke";
							
							$datet = ''.'yang ke'.' '.$usia.'';
							
							$judul 		= "".$row['judul']."";
							$isi 		= "".$row['isi']."";
							$status 	= "".$row['status']."";
							$photo 		= "".$row['photo']."";
							$create_at 	= $datet;
							
							$ucapan = $isi.' '.$member.' '.'yang ke'.' '.$usia.' '.'Semoga panjang umur, sukses selalu';
						
							
							// masukan kedalam array
							$message = array(
							"id_greeting" 			=> $id_greeting,
							"judul" 				=> $judul,
							"isi" 					=> $ucapan,
							"status" 				=> $status,
							"photo" 				=> $photo,
							"create" 				=> $create_at,
							"param"					=> "greeting",
							"sender"				=> $sender);
						
						}
						
							// update sender
							$q_update = "update tbl_greeting set 
							kirim =  NOW() 
							where id_greeting  	='".$id_greeting."'";
							$this->mod_main->put($q_update);

							$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
									
				}
			
		
			}
	
			
			
				redirect("sekolah");
				
			
		
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	// agama
	function greeting_agama()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
			
			$kategori 		= $this->uri->segment(3);
			$agama 			= $this->uri->segment(4);
			$id_greeting 	= $this->uri->segment(5);
			
			
			
			
			
			//tgl sekarang
			$tanggal = date("Y-m-d");
			
			// API Key
			$api_key = $api_merchant;
			
			$agama_gret ="";
			$tgl_lahir = "";
			
			
			
			// cari greeting  berdasarkan agama dan tanggal sekarang
			

			
			
			
					$date_database = $this->model_sekolah->get_greeting_member($id_merchant,$agama,$tgl_lahir);			
					foreach ($date_database as $row)
					{
							
							
						
							$member = "".$row['nama_member']."";
							$agama_member = "".$row['agama']."";
							$reg_id = "".$row['gcm']."";
							
							
							//echo $member;
							
						$date_database = $this->model_sekolah->get_greeting($kategori,$id_greeting);
						foreach ($date_database as $row)
						{
							// get ucapan agama
							
							
							
							
							$judul 		= "".$row['judul']."";
							$isi 		= "".$row['isi']."";
							$status 	= "".$row['status']."";
							$photo 		= "".$row['photo']."";
							$create_at 	= tgl_indo($row['tgl_greeting']);
							
							$ucapan = $isi;
						
							
							// masukan kedalam array
							$message = array(
							"id_greeting" 			=> $id_greeting,
							"judul" 				=> $judul,
							"isi" 					=> $ucapan,
							"status" 				=> $status,
							"photo" 				=> $photo,
							"create" 				=> $create_at,
							"param"					=> "greeting",
							"sender"				=> $sender);
							
							
						
					
						}
						
							// update sender
							$q_update = "update tbl_greeting set 
							kirim =  NOW() 							where id_greeting  	='".$id_greeting."'";
							$this->mod_main->put($q_update);
	
							//kondisi jika sama dengan agama	
							if($agama == $agama_member)
							{
								$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
							}
					}
				
				redirect("sekolah");
		
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	// nasionalisme
	function greeting_nasinonalis()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
			
			$kategori 		= $this->uri->segment(3);
			$id_greeting 	= $this->uri->segment(4);
			$agama = "";
			
			
			
			
			//tgl sekarang
			$tanggal = date("Y-m-d");
			
			// API Key
			$api_key = $api_merchant;
			
			$agama_gret ="";
			$tgl_lahir = "";
			
			
			
			// cari greeting  berdasarkan agama dan tanggal sekarang
			

			
			
			
					$date_database = $this->model_sekolah->get_greeting_member($id_merchant,$agama,$tgl_lahir);			
					foreach ($date_database as $row)
					{
							
							
						
							$member = "".$row['nama_member']."";
							$agama_member = "".$row['agama']."";
							$reg_id = "".$row['gcm']."";
							
							
							//echo $member;
							
						$date_database = $this->model_sekolah->get_greeting($kategori,$id_greeting);
						foreach ($date_database as $row)
						{
							// get ucapan nasionalisme
							
							
							
							
							$judul 		= "".$row['judul']."";
							$isi 		= "".$row['isi']."";
							$status 	= "".$row['status']."";
							$photo 		= "".$row['photo']."";
							$create_at 	= tgl_indo($row['tgl_greeting']);
							
							$ucapan = $isi;
						
							
							// masukan kedalam array
							$message = array(
							"id_greeting" 			=> $id_greeting,
							"judul" 				=> $judul,
							"isi" 					=> $ucapan,
							"status" 				=> $status,
							"photo" 				=> $photo,
							"create" 				=> $create_at,
							"param"					=> "greeting",
							"sender"				=> $sender);
							
							
						
					
						}
						
							// update sender
							$q_update = "update tbl_greeting set 
							kirim =  NOW() 							where id_greeting  	='".$id_greeting."'";
							$this->mod_main->put($q_update);
	
							//kondisi jika sama dengan agama	
							//if($agama == $agama_member)
							//{
								$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
							//}
					}
				
				redirect("sekolah");
		
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	// nasionalisme
	function greeting_budaya()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
			
			$kategori 		= $this->uri->segment(3);
			$id_greeting 	= $this->uri->segment(4);
			$agama = "";
			
			
			
			
			//tgl sekarang
			$tanggal = date("Y-m-d");
			
			// API Key
			$api_key = $api_merchant;
			
			$agama_gret ="";
			$tgl_lahir = "";
			
			
			
			// cari greeting  berdasarkan agama dan tanggal sekarang
			

			
			
			
					$date_database = $this->model_sekolah->get_greeting_member($id_merchant,$agama,$tgl_lahir);			
					foreach ($date_database as $row)
					{
							
							
						
							$member = "".$row['nama_member']."";
							$agama_member = "".$row['agama']."";
							$reg_id = "".$row['gcm']."";
							
							
							//echo $member;
							
						$date_database = $this->model_sekolah->get_greeting($kategori,$id_greeting);
						foreach ($date_database as $row)
						{
							// get ucapan naslinalisme
							
							
							
							
							$judul 		= "".$row['judul']."";
							$isi 		= "".$row['isi']."";
							$status 	= "".$row['status']."";
							$photo 		= "".$row['photo']."";
							$create_at 	= tgl_indo($row['tgl_greeting']);
							
							$ucapan = $isi;
						
							
							// masukan kedalam array
							$message = array(
							"id_greeting" 			=> $id_greeting,
							"judul" 				=> $judul,
							"isi" 					=> $ucapan,
							"status" 				=> $status,
							"photo" 				=> $photo,
							"create" 				=> $create_at,
							"param"					=> "greeting",
							"sender"				=> $sender);
							
							
						
					
						}
						
							// update sender
							$q_update = "update tbl_greeting set 
							kirim =  NOW() 							where id_greeting  	='".$id_greeting."'";
							$this->mod_main->put($q_update);
	
							//kondisi jika sama dengan agama	
							//if($agama == $agama_member)
							//{
								$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
							//}
					}
				
				redirect("sekolah");
		
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
