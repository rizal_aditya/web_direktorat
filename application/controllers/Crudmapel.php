<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudmapel extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			
			redirect("sekolah/kelas");	
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			
				

									
			$pel						= 	$this->input->post('nama_mapel', TRUE);
			$nama_mapel					=  	htmlspecialchars($pel, ENT_QUOTES);
			$status						= 	$this->input->post('status', TRUE);
			

			$q = "insert into tbl_mapel(nama_mapel,id_merchant,status,create_at) 
			values('".$nama_mapel."','".$id_merchant."','".$status."',NOW())";
			$this->mod_main->create($q);

			redirect("sekolah/mapel");		
			 

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_sekolah"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
		
			$id_mapel				= $this->uri->segment(3);	
		
			$pel						= 	$this->input->post('nama_mapel', TRUE);
			$nama_mapel					=  	htmlspecialchars($pel, ENT_QUOTES);
			$status						= 	$this->input->post('status', TRUE);

			$q_update = "update tbl_mapel set 
			nama_mapel 			= '".$nama_mapel."'
			, id_merchant 	= '".$id_merchant."'
			
			, status 		= '".$status."'
			, update_at = NOW() 
			where id_mapel  	='".$id_mapel."'";
			$this->mod_main->put($q_update);
			
			redirect("sekolah/mapel");	
					


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_mapel","tbl_mapel");
			
			redirect("sekolah/mapel");	

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	
	
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
