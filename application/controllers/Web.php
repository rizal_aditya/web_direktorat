<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
	}

	function index()
	{
			
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!=""){
			
			echo "<meta http-equiv='refresh' content='0; url=".base_url()."merchant'>";
			
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
