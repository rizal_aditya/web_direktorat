<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Developer extends CI_Controller {


	var $engineReturn 	= array();
	var $specialValue 	= array();
	var $data			= array();
	var $loginIdUser		= "";
	var $loginAksesUser		= "";
	var $menuUserOtority	= "developer";
	


	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('captcha','date','text_helper'));
		$this->load->library('email');
		$this->load->model('mod_main');
		$this->load->model('model_developer');
		$this->load->helper('tgl_indo');
		//session_start();
		
	}
	
	function index()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_admin_user"]	=	$pecah[0];
			$data["user_name"]		=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$kode					= 	$data["id_admin_user"];
			$id_developer			= 	$data["id_admin_user"];
			$session = "developer";
			$data["menu"] 				= 	$this->model_developer->get_menu($session);
		
		$data['page'] 				= 'developer/dasboard';
		$data['deskripsi'] 			= "Dasboard Developer";

		
		


		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Dasboard Developer";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		

		}
		else{
			
			$this->load->view('function/login_developer');

		}
	}



	
	public function sekolah()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_admin_user"]	=	$pecah[0];
			$data["user_name"]		=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$kode					= 	$data["id_admin_user"];
			$id_developer			= 	$data["id_admin_user"];	
			$session = "developer";
			$data["menu"] 				= 	$this->model_developer->get_menu($session);
		
		$id_merchant = $this->uri->segment(4);
		$id = $this->uri->segment(5);
		$limit ="";
		$data['merchant']  	= $this->model_developer->get_merchant($id_merchant,$limit);
		
		$data['notifikasi']  	= $this->model_developer->get_notifikasi($id_merchant);
		if(!empty($data['notifikasi'][0]['id_merchant']))
		{
			$data['kode_merchant']  = $data['notifikasi'][0]['id_merchant'];
		
		}else{
			
			$data['kode_merchant'] = $id_merchant;
		}
		$data['page'] 				= 'developer/merchant';
		$data['deskripsi'] 			= "Developer Sekolah";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Merchant developer";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	
		
		
		}else{
			
			$this->load->view('function/login_developer');
	
		}
		
	}

	
	public function greetings()
	{
		
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{
			
				$pecah					=	explode("|",$session);
				$data["id_admin_user"]	=	$pecah[0];
				$data["user_name"]		=	$pecah[1];
				$data["photo"]			=	$pecah[2];
				$kode					= 	$data["id_admin_user"];
				$id_developer			= 	$data["id_admin_user"];	
				$session = "developer";
				$data["menu"] 				= 	$this->model_developer->get_menu($session);
			
			$id_merchant = $this->uri->segment(4);
			$id = $this->uri->segment(5);
			$data['greeting']  	= $this->model_developer->get_greeting_developer($id_developer);
			

			$data['page'] 				= 'developer/greeting';
			$data['deskripsi'] 			= "Developer Greetings";



			$data['header_keyword'] 	= "";
			$data['header_desc'] 		= "";
			$data['header_title'] 		= "Developer Greetings";

			$this->load->view('header',$data);
			$this->load->view('main_header',$data);	
			$this->load->view('main_left',$data);
			$this->load->view('main_menu',$data);
			$this->load->view('footer',$data);	
			
			
		}else{
				
				$this->load->view('function/login_developer');
		
		}
		
		
	}
	
	function login()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!=""){
			
			redirect("developer");


		}
		else{
			
		
			$this->load->view('function/login_developer');
			
		}
	}


	
	
	
	function logout()
	{
		session_destroy();
		redirect("developer");
	}
	


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
