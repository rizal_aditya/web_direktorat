<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudguru extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			
			redirect("sekolah/pengajar");
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			
				

			$id_pegawai					= 	$this->input->post('id_pegawai', TRUE);
			$id_mapel					= 	$this->input->post('id_mapel', TRUE);
			$id_kelas					= 	$this->input->post('id_kelas', TRUE);
			$status						= 	$this->input->post('status', TRUE);
			
			
			$q = $this->model_sekolah->cek_nama_guru($id_pegawai,$id_merchant);
			if (count($q->result())>0){
				
					$q_update = "update tbl_pengajar set 
					id_pegawai 			= '".$id_pegawai."'
					, id_merchant 	= '".$id_merchant."'
					, status 		= '".$status."'
					, update_at = NOW() 
					where id_pegawai  	='".$id_pegawai."'";
					$this->mod_main->put($q_update);
			
				
				
			}else{
				

			$q = "insert into tbl_pengajar(id_pegawai,id_merchant,status,create_at) 
			values('".$id_pegawai."','".$id_merchant."','".$status."',NOW())";
			$this->mod_main->create($q);
			
			
			}	
			
			
			$q = "insert into tbl_pengajar_det(id_pegawai,id_merchant,id_kelas,id_mapel) 
			values('".$id_pegawai."','".$id_merchant."','".$id_kelas."','".$id_mapel."')";
			$this->mod_main->create($q);
			

			redirect("sekolah/pengajar");	
			 

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_sekolah"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
		
			$id_pengajar				= $this->uri->segment(3);	
		
			$id_pegawai					= 	$this->input->post('id_pegawai', TRUE);
			$id_mapel					= 	$this->input->post('id_mapel', TRUE);
			$id_kelas					= 	$this->input->post('id_kelas', TRUE);
			$status						= 	$this->input->post('status', TRUE);

			$q_update = "update tbl_pengajar set 
			id_pegawai 			= '".$id_pegawai."'
			, id_merchant 	= '".$id_merchant."'
			, id_mapel 		= '".$id_mapel."'
			, id_kelas 		= '".$id_kelas."'
			, status 		= '".$status."'
			, update_at = NOW() 
			where id_pengajar  	='".$id_pengajar."'";
			$this->mod_main->put($q_update);
			
			
		
			redirect("sekolah/pengajar");	
					


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_pengajar_det","tbl_pengajar_det");
			
			redirect("sekolah/pengajar");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	
	function delete_all()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_pegawai","tbl_pengajar");
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_pegawai","tbl_pengajar_det");
			redirect("sekolah/pengajar");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
