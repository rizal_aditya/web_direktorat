<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_pertanyaan_sekolah extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			redirect("sekolah/survey");
		

		}else{

			$this->load->view('function/login_merchant');
			
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			
			$dek					= $this->input->post('pertanyaan', TRUE);	
			$pertanyaan				= htmlspecialchars($dek, ENT_QUOTES);
			$id_survey				= $this->input->post('id_survey', TRUE);
			$a						= $this->input->post('que_a', TRUE);
			$que_a					= htmlspecialchars($a, ENT_QUOTES);
			$b 						= $this->input->post('que_b', TRUE);	
			$que_b					= htmlspecialchars($b, ENT_QUOTES);
			$c 						= $this->input->post('que_c', TRUE);
			$que_c					= htmlspecialchars($c, ENT_QUOTES);
			$d						= $this->input->post('que_d', TRUE);
			$que_d					= htmlspecialchars($d, ENT_QUOTES);
			$e 						= $this->input->post('que_e', TRUE);
			$que_e					= htmlspecialchars($e, ENT_QUOTES);
			
			$q = "insert into tbl_polling(pertanyaan,id_merchant,id_survey,que_a,que_b,que_c,que_d,que_e,create_at) 
				values('".$pertanyaan."','".$id_merchant."','".$id_survey."','".$que_a."','".$que_b."','".$que_c."','".$que_d."','".$que_e."',NOW())";
			$this->mod_main->create($q);
				
								
			redirect("sekolah/pertanyaan/$id_survey");

	
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{


			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			$id_polling 			= $this->uri->segment(3);
			$dek					= $this->input->post('pertanyaan', TRUE);	
			$pertanyaan				= htmlspecialchars($dek, ENT_QUOTES);
			$id_survey				= $this->input->post('id_survey', TRUE);
			$a						= $this->input->post('que_a', TRUE);
			$que_a					= htmlspecialchars($a, ENT_QUOTES);
			$b 						= $this->input->post('que_b', TRUE);	
			$que_b					= htmlspecialchars($b, ENT_QUOTES);
			$c 						= $this->input->post('que_c', TRUE);
			$que_c					= htmlspecialchars($c, ENT_QUOTES);
			$d						= $this->input->post('que_d', TRUE);
			$que_d					= htmlspecialchars($d, ENT_QUOTES);
			$e 						= $this->input->post('que_e', TRUE);
			$que_e					= htmlspecialchars($e, ENT_QUOTES);
			
			
			
			$q_update = "update tbl_polling set 
		 	  pertanyaan			= '".$pertanyaan."'
		 	, id_merchant 		= '".$id_merchant."'  
		 	, id_survey			= '".$id_survey."'
			
			, que_a				= '".$que_a."'
			, que_b				= '".$que_b."'
			, que_c				= '".$que_c."'
			, que_d				= '".$que_d."'
			, que_e				= '".$que_e."'
		 	, update_at 		= NOW()
		 	where id_polling  	='".$id_polling."'";
			$this->mod_main->put($q_update);
			
		
					
			redirect("sekolah/pertanyaan/$id_survey");
			
		


			}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(4) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(4);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

				$id_survey = $this->uri->segment(3);
			
				
				
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_polling","tbl_polling");
			
			
			redirect("sekolah/pertanyaan/$id_survey");
			
			
			

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	





	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
