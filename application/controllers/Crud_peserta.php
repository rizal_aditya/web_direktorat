<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_peserta extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		$this->load->library('email');
		$this->email->set_newline("\r\n");
	}

	function index()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			

				
		}else{

				$this->load->view('function/login_merchant');
		}	
	}


	



	function nonaktif()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];

			
			$id_member			= $this->uri->segment(3);
			$status				= 'n';
			
			$q_update = "update tbl_member set 
			status 				= '".$status."'
			, update_at 		= NOW() 
			where id_merchant ='".$id_merchant."' and id_member ='".$id_member."'";
			$this->mod_main->put($q_update);
			

			redirect("direktorat/pegawai");

			
		

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}
	
	function aktif()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];

			
			
			$id_member				= $this->uri->segment(3);
			
			$status				= 'y';
			
			$q_update = "update tbl_member set 
			status 				= '".$status."'
			, update_at 		= NOW() 
			where id_member ='".$id_member."'";
			$this->mod_main->put($q_update);
			
			
			
			
			//redirect("http://apisekolah.kartuvirtual.com/sekolah/send_aktivasi/$id_merchant/$id_member");
			
			redirect("direktorat/pegawai");	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!=""){
			
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			
			$member	= $this->model_sekolah->get_member_merchant($id_merchant,$kode);
			$photo =  $member[0]['photo'];
			
			if($photo != "")
			{	
			$gambar  = 'http://apisekolah.kartuvirtual.com/assets/member'.$photo;
			unlink($gambar);
			}
			
		
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_member","tbl_member");
			
			redirect("direktorat/pegawai");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
