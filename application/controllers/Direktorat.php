<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Direktorat extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('captcha','date','text_helper'));
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	
		
	}
	
	function index()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			
			$tgl = date("Y-m-d");
		$data["tgl"] = date("Y-m-d");
		$tanggal = date("m-d");
		
		
		
		$id_greeting = $this->uri->segment(3);
		
		
		
		$data["greeting_ultah"]			= $this->model_sekolah->get_dasboard_ultah($id_merchant,$tanggal);	
		$data["greeting_agama"]			= $this->model_sekolah->get_dasboard_agama($tgl,$id_greeting);	
		$data["greeting_nasional"]		= $this->model_sekolah->get_dasboard_nasional($tgl,$id_greeting);
		$data["greeting_budaya"]		= $this->model_sekolah->get_dasboard_budaya($tgl,$id_greeting);
			
	
		$data["jumlah_member"]		= $this->model_sekolah->get_jml_member($id_merchant);
		$data["jumlah_berita"]		= $this->model_sekolah->get_jml_berita($id_merchant);
		$data["jumlah_kalender"]	= $this->model_sekolah->get_jml_kalender($id_merchant);
		$data["jumlah_notif"]		= $this->model_sekolah->get_jml_notifikasi($id_merchant);
		$data["jumlah_direktorat"]		= $this->model_sekolah->get_jml_direktorat($id_merchant);
		
		$data["jumlah_divisi"]		= $this->model_sekolah->get_jml_divisi($id_merchant);
		$data["jumlah_pres_sek"]	= $this->model_sekolah->get_jml_prestasi_sek($id_merchant);
		$data["jumlah_pres_alumni"]	= $this->model_sekolah->get_jml_prestasi_alumni($id_merchant);
		$data["jumlah_mapel"]		= $this->model_sekolah->get_jml_mapel($id_merchant);
		$data["jumlah_anak"]		= $this->model_sekolah->get_jml_anak($id_merchant);
		$data["jumlah_undangan"]	= $this->model_sekolah->get_jml_undangan($id_merchant);
		$data["jumlah_undangan"]	= $this->model_sekolah->get_jml_undangan($id_merchant);
		
		
		$profile					= $this->model_sekolah->get_profile_merchant($id_merchant);
		
		// Aktifitas  terbaru
		$data["member_terbaru"]		= $this->model_sekolah->get_member_terbaru($id_merchant);	
	
		
		
		
		foreach ($profile as $row)
		{
			$data["notiv"]= "".$row['create_at']."";
			
		}

		$data['page'] 				= 'sekolah/dasboard';
		$data['deskripsi'] 			= "Dasboard Direktorat";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Dasboard Direktorat";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		

		}else{

			$this->load->view('function/login_merchant');
		}
	}

	function tes()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			$id_member				= 	$this->uri->segment(4);	
			$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			//$link_url = "";	

			$data['page'] 				= 'sekolah/tes';
			$data['deskripsi'] 			= "Peserta Direktorat";

			

			$data['header_keyword'] 	= "";
			$data['header_desc'] 		= "";
			$data['header_title'] 		= "Peserta Direktorat";

			$this->load->view('header',$data);
			$this->load->view('main_header',$data);	
			$this->load->view('main_left',$data);
			$this->load->view('main_menu',$data);
			$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	
	function pegawai()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			$id_member				= 	$this->uri->segment(4);	
			$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			//$link_url = "";	

			$data['page'] 				= 'sekolah/member';
			$data['deskripsi'] 			= "Peserta Direktorat";

			$data["pegawai"]			= $this->model_sekolah->get_pegawai_direktorat($id_merchant,$id_member);
			$data["unit"]				= $this->model_sekolah->get_unit_direktorat($id_merchant);

			$data['header_keyword'] 	= "";
			$data['header_desc'] 		= "";
			$data['header_title'] 		= "Peserta Direktorat";

			$this->load->view('header',$data);
			$this->load->view('main_header',$data);	
			$this->load->view('main_left',$data);
			$this->load->view('main_menu',$data);
			$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	


	function divisi()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];
			$id_divisi				= 	$this->uri->segment(4);	
			$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
			//$link_url = "";	

			$data['page'] 				= 'sekolah/divisi';
			$data['deskripsi'] 			= "Satuan Kerja Direktorat";

			$data["divisi"]				= $this->model_sekolah->get_divisi($id_merchant,$id_divisi);

			$data['header_keyword'] 	= "";
			$data['header_desc'] 		= "";
			$data['header_title'] 		= "Peserta Direktorat";

			$this->load->view('header',$data);
			$this->load->view('main_header',$data);	
			$this->load->view('main_left',$data);
			$this->load->view('main_menu',$data);
			$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	

	function headline_news()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
			$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_berita 					= $this->uri->segment(4);
		$data["hotnews"]				= $this->model_sekolah->get_info_sekolah($id_merchant,$id_berita);	
		$id_member = "";
	
		

		$data['page'] 				= 'sekolah/hotnews';
		$data['deskripsi'] 			= "Newsletter";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "News Letter";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	function direktorat()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
			$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$id_direktorat 					= $this->uri->segment(4);
		
		$data["direktorat"]				= $this->model_sekolah->get_direktorat($id_merchant,$id_direktorat);	
		$id_member = "";
		$data["unit"]				= $this->model_sekolah->get_devisi_unit($id_merchant);	

		$data['page'] 				= 'sekolah/direktorat';
		$data['deskripsi'] 			= "Direktorat";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Direktorat";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	function notifikasi()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$data["api_key"]		=	$pecah[3];
		$data["sender"]			=	$pecah[4];
		$data["akses"]			=	$pecah[5];
		$data["jenis"]			=	$pecah[6];
		$api_merchant			=   $data["api_key"];
		$sender					= 	$data["sender"];	
		$id_merchant			= 	$data["id_merchant"];
		$akses 					= 	$data["akses"];
		$jenis					= 	$data["jenis"];	
			$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		
			$limit = "";
			$id_notifikasi				= $this->uri->segment(4);
			$data["notifikasi"]			= $this->model_sekolah->get_notifikasi_direktorat($id_merchant,$id_notifikasi,$limit);
		
		
		$data['page'] 				= 'sekolah/notifikasi';
		$data['deskripsi'] 			= "Notifikasi Direktorat";

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Notifikasi Direktorat";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	
	
	

	

	function agenda()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "direktorat";
			
			$id_divisi = "";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
				

			$id_kalender 				= $this->uri->segment(4);
			$data["akademik"]			= $this->model_sekolah->get_kalender_sekolah($id_merchant,$id_kalender);		
			$data["divisi"]				= $this->model_sekolah->get_divisi($id_merchant,$id_divisi);	

		$data['page'] 				= 'sekolah/kalender';
		$data['deskripsi'] 			= "Agenda Direktorat";



		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Agenda Direktorat";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	



	}else{

			$this->load->view('function/login_merchant');
		}
	}	
	
	
	function tabel_agenda()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$data["akses"]			=	$pecah[5];
			$data["jenis"]			=	$pecah[6];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			$akses 					= 	$data["akses"];
			$jenis					= 	$data["jenis"];	
			$session = "direktorat";
			
			$id_divisi = "";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
				

			$id_kalender 				= $this->uri->segment(4);
			$data["akademik"]			= $this->model_sekolah->get_kalender_sekolah($id_merchant,$id_kalender);		
			$data["divisi"]				= $this->model_sekolah->get_divisi($id_merchant,$id_divisi);	
			$unit = $this->input->post('id_unit', TRUE);

			$data['id_unit'] = 	$unit;
			$data["agenda"]					= $this->model_sekolah->get_dcalendary($id_merchant,$unit);
			$data['page'] 				= 'sekolah/kalender';
			$data['deskripsi'] 			= "Agenda Direktorat";

			$data['header_keyword'] 	= "";
			$data['header_desc'] 		= "";
			$data['header_title'] 		= "Agenda Direktorat";

			$this->load->view('tabel_agenda',$data);
		
			


	}else{

			$this->load->view('function/login_merchant');
		}
	}	

	
	
	function Profile()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);


		$data['page'] 				= 'sekolah/profile';
		$data['deskripsi'] 			= "Profile Direktorat";

		$data["profile"]			= $this->model_sekolah->get_profile_merchant($id_merchant);
		

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Profile Direktorat";

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}


	

	

	function ubah_password()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);


		$data['page'] 				= 'sekolah/ubah_password';
		$data['deskripsi'] 			= "Ubah Password Direktorat";

		//$data["profile"]			= $this->model_sekolah->get_profile_merchant($id_merchant);
		

		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Ubah Password Direktorat";
		

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	function location()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$pusat = "pusat";
		$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);
		$data["profile"]			= $this->model_sekolah->get_profile_merchant($id_merchant);
		$data["cabang"]				= $this->model_sekolah->get_cabang_pusat($id_merchant,$pusat);

		$data['page'] 				= 'sekolah/lokasi';
		$data['deskripsi'] 			= "Update Lokasi Direktorat";


		$data['header_keyword'] 	= "";
		$data['header_desc'] 		= "";
		$data['header_title'] 		= "Update Lokasi Direktorat";
		

		$this->load->view('header',$data);
		$this->load->view('main_header',$data);	
		$this->load->view('main_left',$data);
		$this->load->view('main_menu',$data);
		$this->load->view('footer',$data);	





		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}
	
	
	

	function login()
	{
		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!=""){
			redirect("direktorat");	
		}
		else{
			
		
			$this->load->view('function/login_merchant');
			
		}
	}



	function logout()
	{
		session_destroy();
		redirect("direktorat");		
	}
	
	
	// update password
	
	
	function put_password()
	{

		$session=isset($_SESSION['direktorat']) ? $_SESSION['direktorat']:'';
		if($session!="")
		{

		$pecah					=	explode("|",$session);
		$data["id_merchant"]	=	$pecah[0];
		$data["nama_merchant"]	=	$pecah[1];
		$data["photo"]			=	$pecah[2];
		$id_merchant			= 	$data["id_merchant"];
		$session = "direktorat";
			$data["menu"] 				= 	$this->model_sekolah->get_menu($session);
			$data["menu_costume"] 		= 	$this->model_sekolah->get_menu_costume($session,$id_merchant);

		$pwd_old	= $this->input->post('password', TRUE);
		$pwd_baru	= $this->pass_word->encryptPass($this->input->post('passwordbaru', TRUE));
		$pwdb		= $this->input->post('passwordbaru', TRUE);
		$pwdu		= $this->input->post('passwordulang', TRUE);

						$sql	= "SELECT pass FROM tbl_merchant
									WHERE
									id_merchant	= '$id_merchant'
									";
									//die($sql);
						$rec	= $this->db->query($sql);
						$count	= $rec->num_rows();
						$res	= $rec->result_array();
						if($count>0){
							if($this->pass_word->decryptPass($res[0]['pass'])==$pwd_old){

									if($pwdb == $pwdu){

							 	
										$q_update = "update tbl_merchant set pass ='".$pwd_baru."' , update_at = NOW() where id_merchant ='".$id_merchant."'";
										$this->mod_main->put($q_update);
										
										echo "<script>alert('Update password berhasil..');</script>";
										echo'<script type="text/javascript">location.href="./profile";</script>';

									}else{

									
										
										echo"<script>alert('Password Ditolak....');</script>";
										echo'<script type="text/javascript">location.href="./ubah_password";</script>';

									}			
							
							}else{
							
							echo"<script>alert('Update password gagal....');</script>";
							echo'<script type="text/javascript">location.href="./ubah_password";</script>';
							
							

							}
						}

		}else{
			
			$this->load->view('function/login_merchant');
			
		}				


	}
	
	
	
	
	
	
	


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
