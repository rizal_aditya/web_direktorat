<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudpegawai extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			
			redirect("sekolah/staff");	
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			$url					= 	$this->config->item('base_url');	
			
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'pegawai/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			

			$les						= 	$this->input->post('nama_pegawai', TRUE);
			$nama_pegawai 				=  htmlspecialchars($les, ENT_QUOTES);
			$jabatan					= 	$this->input->post('jabatan', TRUE);
			$status						= 	$this->input->post('status', TRUE);
			
			$dek					= $this->input->post('alamat', TRUE);	
			$alamat					= htmlspecialchars($dek, ENT_QUOTES);
			
			$telp						= 	$this->input->post('telp', TRUE);
			$email						= 	$this->input->post('email', TRUE);
			$pendidikan					= 	$this->input->post('pendidikan', TRUE);
			$id_kelas					= 	$this->input->post('id_kelas', TRUE);
			$id_mapel					= 	$this->input->post('id_mapel', TRUE);
			$tipe 						= 	$this->input->post('tipe', TRUE);
			
			$max 		= $_FILES['userfile']['size'];
			$extensi 	=  $this->mod_main->get_ekstensi($asli);
			
			
				
			
			
			
		$q = $this->model_sekolah->cek_email_guru($email,$id_merchant);
		if (count($q->result())>0){
			
			echo"<script>alert('Data email staff & guru sudah digunakan !!!');</script>";
			
			echo'<script type="text/javascript">location.href="'.$url.'/sekolah/staff";</script>';
		}else{	
				
				
			
			
			
			
			
			
			
			

			if(!empty($asli)){ 
			
			
			
				if($max == 0 or $max > 1500000){
								
						echo"<script> alert('Pilih file maks 1,5 MB...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/sekolah/staff";</script>';
						
				} else if(($extensi != 'jpg') && ($extensi != 'jpeg') && ($extensi != 'png')){ 
						
						echo"<script type='text/javascript'>alert('Extensi file harus jpeg atau jpg atau png...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/sekolah/staff";</script>';
						
				 } else {	
			
			
			
			
			
			
			
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/pegawai/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] 			= TRUE;
			$config['max_size'] 			= '0';
			$config['max_width'] 			= '0';
			$config['max_height'] 			= '0'; 						
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
			
			
			
			$thumbr = $thumb;

					$q = "insert into tbl_pegawai(
					nama_pegawai
					,id_merchant
					,jabatan
					,telp
					,email
					,pendidikan
					,tipe
					,status
					,photo
					,alamat
					,create_at) 
					 values(
					 '".$nama_pegawai."'
					 ,'".$id_merchant."'
					 ,'".$jabatan."'
					 ,'".$telp."'
					 ,'".$email."'
					 ,'".$pendidikan."'
					 ,'".$tipe."'
					 ,'".$status."'
					 ,'".$thumbr."'
					 ,'".$alamat."'
					 ,NOW())";
					$this->mod_main->create($q);
				 }

			}else{ 
			
					$q = "insert into tbl_pegawai(
					nama_pegawai
					,id_merchant
					,jabatan
					,telp
					,email
					,pendidikan
					,tipe
					,status
					,alamat
					,create_at) 
					 values(
					 '".$nama_pegawai."'
					 ,'".$id_merchant."'
					 ,'".$jabatan."'
					 ,'".$telp."'
					 ,'".$email."'
					 ,'".$pendidikan."'
					 ,'".$tipe."'
					 ,'".$status."'
					 ,'".$alamat."'
					 ,NOW())";
					$this->mod_main->create($q);
			
			 }
				redirect("sekolah/staff");	
			 

			

		
			
				 }									
				
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_sekolah"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
		
			$id_pegawai					= 	$this->uri->segment(3);
			$nama_pegawai				= 	$this->input->post('nama_pegawai', TRUE);
			$jabatan					= 	$this->input->post('jabatan', TRUE);
			$status						= 	$this->input->post('status', TRUE);
			$dek						= $this->input->post('alamat', TRUE);	
			$alamat						= htmlspecialchars($dek, ENT_QUOTES);
			$telp						= 	$this->input->post('telp', TRUE);
			$email						= 	$this->input->post('email', TRUE);
			$pendidikan					= 	$this->input->post('pendidikan', TRUE);
			
			$tipe 						= 	$this->input->post('tipe', TRUE);
			
			$gbr 		= $this->input->post('gbr');
	
			$url					= 	$this->config->item('base_url');	
			
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'pegawai/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			$max 		= $_FILES['userfile']['size'];
			$extensi 	= $this->mod_main->get_ekstensi($asli);
			
			
				
			
			
			
			if(!empty($asli)){
				
				
				
				if($max == 0 or $max > 1500000){
								
						echo"<script> alert('Pilih file maks 1,5 MB...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/sekolah/staff";</script>';
						
				} else if(($extensi != 'jpg') && ($extensi != 'jpeg') && ($extensi != 'png')){ 
						
						echo"<script type='text/javascript'>alert('Extensi file harus jpeg atau jpg atau png...');</script>";
						echo'<script type="text/javascript">location.href="'.$url.'/sekolah/staff";</script>';
						
				 } else {	
				
				
				
				
			
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/pegawai/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] = TRUE;
			$config['max_size'] = '0';
			$config['max_width']  = '0';
			$config['max_height']  = '0'; 							
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
			
			$thumbr = $thumb;
			
			
			 	
			$q_update = "update tbl_pegawai set 
			nama_pegawai		= '".$nama_pegawai."'
			, id_merchant 	= '".$id_merchant."'
			, jabatan 		= '".$jabatan."'
			, pendidikan 	= '".$pendidikan."'
			, status 		= '".$status."'
			, telp 			= '".$telp."'
			, email 		= '".$email."'
			, tipe 			= '".$tipe."'
			
			, alamat 		= '".$alamat."' 
			, photo = '".$thumbr."'
			, update_at = NOW() 
			where id_pegawai 	='".$id_pegawai."'";
			$this->mod_main->put($q_update);
			
			
					if(!empty($gbr))
					{
				
					$file_gambar  = './assets/upload/pegawai/'.$gbr;
					unlink($file_gambar);

					}				
				 }	
			
			}else{
				
			

			$q_update = "update tbl_pegawai set 
			nama_pegawai 		= '".$nama_pegawai."'
			, id_merchant 	= '".$id_merchant."'
			, jabatan 		= '".$jabatan."'
			, pendidikan 	= '".$pendidikan."'
			, status 		= '".$status."'
			, telp 			= '".$telp."'
			, email 		= '".$email."'
			, tipe 			= '".$tipe."'
			, alamat 		= '".$alamat."' 
		
			, update_at = NOW() 
			where id_pegawai 	='".$id_pegawai."'";
			$this->mod_main->put($q_update);
				
			}		
			
					
									
					redirect("sekolah/staff");
					
				


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			

			$info = $this->model_sekolah->delete_guru_sekolah($kode);
			foreach ($info as $data) 
			{

				$photo = "".$data['photo']."";

			}	

			if($photo != "")
			{	
			$gambar  = './assets/upload/pegawai/'.$photo;
			unlink($gambar);
			}

			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_pegawai","tbl_pegawai");
			
			redirect("sekolah/staff");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	


	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
