<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session == "merchant")
		{
			
			redirect("merchant/member");
			
		}else if($session == "instansi"){

			redirect("instansi/member");
		
		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	



	function nonaktif()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		
		if($session == "merchant")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];

			
			$id_member			= $this->uri->segment(3);
			$status				= 'n';
			
			$q_update = "update tbl_member set 
			status 				= '".$status."'
			, update_at 		= NOW() 
			where id_member ='".$id_member."'";
			$this->mod_main->put($q_update);
			

			redirect("merchant/member");

			
		}else if($session == "instansi"){


			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];

			
			$id_member			= $this->uri->segment(3);
			$status				= 'n';
			
			$q_update = "update tbl_member set 
			status 				= '".$status."'
			, update_at 		= NOW() 
			where id_member ='".$id_member."'";
			$this->mod_main->put($q_update);
			

			redirect("instansi/member");
			
			

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}
	
	function aktif()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		
		if($session == "merchant")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];

			
			$id_member			= $this->uri->segment(3);
			$status				= 'y';
			
			$q_update = "update tbl_member set 
			status 				= '".$status."'
			, update_at 		= NOW() 
			where id_member ='".$id_member."'";
			$this->mod_main->put($q_update);
			

			redirect("merchant/member");
			
			
		}else if($session == "instansi"){
			
			
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$id_merchant			= 	$data["id_merchant"];

			
			$id_member			= $this->uri->segment(3);
			$status				= 'y';
			
			$q_update = "update tbl_member set 
			status 				= '".$status."'
			, update_at 		= NOW() 
			where id_member ='".$id_member."'";
			$this->mod_main->put($q_update);
			

			redirect("merchant/member");
			
			

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				


	}




	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
