<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudasset extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{
			
				redirect("developer/merchant/asset");

		}else{

				$this->load->view('function/login_developer');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_admin_user"]	=	$pecah[0];
			$data["user_name"]		=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$kode					= 	$data["id_admin_user"];
			$id_developer			= 	$data["id_admin_user"];	
				
			
				
			$id_merchant			= 	$this->input->post('id_merchant', TRUE);
			$nama_assets			= 	$this->input->post('nama_assets', TRUE);
			$deskripsi				= 	$this->input->post('deskripsi', TRUE);
			$status					= 	$this->input->post('status', TRUE);
			

			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$nama 		= $code.'-'.'file'.'-'.$_FILES['userfile']['name'];
			
			$file 		= $nama;

		
			
			
			
			
			
			
			if(!empty($asli)){
				
						$config["file_name"]	= $file;
						$config['upload_path'] = './assets/upload/download/';
						$config['allowed_types'] = 'pdf|xls|zip|jpg|jpeg|png|txt|doc|docx|xlsx';
						$config['max_size'] = '50000';						
						$this->load->library('upload', $config);
					
						$this->upload->do_upload();
			

					$thumb = $file; 
					$q = "insert into tbl_asset(nama_asset,id_merchant,deskripsi,status,photo,create_at) 
					 values('".$nama_asset."','".$id_merchant."','".$deskripsi."','".$status."','".$thumb."','".$broadcast."',NOW())";
					$this->mod_main->create($q);


			}else{ 


					$q = "insert into tbl_asset(nama_asset,id_merchant,deskripsi,status,create_at) 
					 values('".$nama_asset."','".$id_merchant."','".$deskripsi."','".$status."','".$broadcast."',NOW())";
					$this->mod_main->create($q);



			}	

		
					
						


			
				redirect("developer/merchant/asset");									
				
		}else{
			
		
			$this->load->view('function/login_developer');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_admin_user"]	=	$pecah[0];
			$data["user_name"]		=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$kode					= 	$data["id_admin_user"];
			$id_developer			= 	$data["id_admin_user"];	
				
			
			
			$id_merchant			= 	$this->input->post('id_merchant', TRUE);
			$nama_assets			= 	$this->input->post('nama_assets', TRUE);
			$deskripsi				= 	$this->input->post('deskripsi', TRUE);
			$status					= 	$this->input->post('status', TRUE);
			
			
			
			
			$gbr 		= $this->input->post('gbr');
	
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$nama 		= $code.'-'.'file'.'-'.$_FILES['userfile']['name'];
			
			$file 		= $nama;

			
			
			if(!empty($asli)){ 
			
			
				$config["file_name"]	= $file;
				$config['upload_path'] = './assets/upload/download/';
				$config['allowed_types'] = 'pdf|xls|zip|jpg|jpeg|png|txt|doc|docx|xlsx';
				$config['max_size'] = '50000';						
				$this->load->library('upload', $config);
			
				$this->upload->do_upload();
			
			
			

				$thumb = $photo; 
				$q_update = "update tbl_asset set nama_asset = '".$nama_asset."', id_merchant = '".$id_merchant."', deskripsi = '".$deskripsi."', status = '".$status."' , photo = '".$thumb."', update_at = NOW() where id_asset  	='".$id_asset."'";
				$this->mod_main->put($q_update);

					if($gbr != "")
					{	
					$gambar  = './assets/upload/asset/'.$gbr;
					unlink($gambar);
					}


			}else{ 

				 
				$q_update = "update tbl_asset set nama_asset = '".$nama_asset."', id_merchant = '".$id_merchant."', deskripsi = '".$deskripsi."', status = '".$status."' , update_at = NOW() where id_asset  	='".$id_asset."'";
				$this->mod_main->put($q_update);



			}	
					
			

					redirect("developer/merchant/asset");	
									



			}else{
			
		
					$this->load->view('function/login_developer');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['developer']) ? $_SESSION['developer']:'';
		if($session!=""){

		
			

			$asset = $this->model_developer->delete_asset_merchant($kode);
			foreach ($asset as $data) 
			{

				$photo = "".$data['photo']."";

			}	

			if($photo != "")
			{	
			$gambar  = './assets/upload/asset/'.$photo;
			unlink($gambar);
			}

			$data["del"] = $this->model_merchant->hapus_konten($kode,"id_asset","tbl_asset");
			
			$this->load->view('function/login_developer');

		}else{

			
		}	
		
	}


	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
