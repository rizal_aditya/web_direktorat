<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudhasilsurvey extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!="")
		{
		
			redirect("instansi/survey");
		

		}else{

			$this->load->view('function/login_merchant');
			
		}	
	}


	function insert()
	{

			$id_merchant				= $this->input->post('id_merchant', TRUE);
			$id_member					= $this->input->post('id_member', TRUE);
			$id_survey					= $this->input->post('id_survey', TRUE);
			$id_jawaban					= $this->input->post('id_jawaban', TRUE);
			$id_pertanyaan				= $this->input->post('id_pertanyaan', TRUE);
			
			
					$jml_pertanyaan = count($id_pertanyaan);
					if($jml_pertanyaan > 0){
						for($i=0; $i<$jml_pertanyaan; $i++){
							

								$kode_pertanyaan = $id_pertanyaan[$i];
							

						}
					}
			
			
					$jml_jawaban = count($id_jawaban);
					if($jml_jawaban > 0){
						for($i=0; $i<$jml_jawaban; $i++){
							
	
										
								
								$kode_jawaban = $id_jawaban[$i];
							

						}
					}
			
			
			
					
			
			
			
			
			$q = "insert into tbl_survey_hasil(id_merchant,id_pertanyaan,id_jawaban,id_member,id_survey,create_at) 
				values('".$id_merchant."','".$id_pertanyaan."','".$kode_jawaban."','".$id_member."','".$id_survey."',NOW())";
			$this->mod_main->create($q);
				
				
		
								
		redirect("instansi/terima_kasih");

	
				
		
	}




	function update()
	{
		
		 
		$id_merchant				= $this->input->post('id_merchant', TRUE);
		$id_member					= $this->input->post('id_member', TRUE);
		$id_survey					= $this->input->post('id_survey', TRUE);
		
		
		$a = $this->model_merchant->cek_survey($id_merchant,$id_survey);
		
		if (count($a->result())>0){
			
			
	
		
			$opt	= $this->input->post('opt');
				if(count($opt) > 0)
				{
				
					foreach ($opt as $id_polling => $value) 
					{
				
					$q_update = "update tbl_polling set ".$value." = ".$value." + 1 
					where id_polling 	='".$id_polling."'";
					$this->mod_main->put($q_update);
				
						
					}

				}
				
			redirect("instansi/view_pertanyaan/$id_merchant/$id_survey/$id_member");
			
			
			
			
			
		}else{
			
			$q = "insert into tbl_survey_hasil(id_merchant,id_polling,id_member,id_survey,create_at) 
				values('".$id_merchant."','".$id_polling."','".$id_member."','".$id_survey."',NOW())";
			$this->mod_main->create($q); 
			
					
			redirect("instansi/terima_kasih");
			
			
		}

	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['instansi']) ? $_SESSION['instansi']:'';
		if($session!=""){

				
				
				$info = $this->model_merchant->delete_katalog_merchant($kode);
				foreach ($info as $data) 
				{

					$photo = "".$data['photo']."";

				}	
				
				if($photo != "")
				{	
				$gambar  = './assets/upload/katalog/'.$photo;
				unlink($gambar);
				}
				
				
				
			$data["del"] = $this->model_merchant->hapus_konten($kode,"id_katalog","tbl_katalog");
			redirect("instansi/layanan");

		}else{

			redirect("instansi/layanan");
		}	
		
	}


	





	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
