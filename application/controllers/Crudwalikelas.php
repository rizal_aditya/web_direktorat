<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudwalikelas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_sekolah");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
			
			redirect("sekolah/kelas");	
		

		}else{

			$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];
			$id_merchant			= 	$data["id_merchant"];
			
				
			$id_kelas					= 	$this->input->post('id_kelas', TRUE);
			$id_pegawai					= 	$this->input->post('id_pegawai', TRUE);

			$q = "insert into tbl_wali_kelas(id_kelas,id_pegawai,create_at) 
			values('".$id_kelas."','".$id_pegawai."',NOW())";
			$this->mod_main->create($q);

			redirect("sekolah/walikelas");		
			 

		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_sekolah"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"];	
			$id_merchant			= 	$data["id_merchant"];
			
		
			$id_wali				= $this->uri->segment(3);	
		
			$id_kelas					= 	$this->input->post('id_kelas', TRUE);
			$id_pegawai					= 	$this->input->post('id_pegawai', TRUE);
			
			
			$q_update = "update tbl_wali_kelas set 
			id_pegawai			= '".$id_pegawai."'
			, id_kelas 			= '".$id_kelas."'		
			, update_at 		= NOW() 
			where id_wali_kelas  	='".$id_wali."'";
			$this->mod_main->put($q_update);
			
			redirect("sekolah/walikelas");	
					


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_wali_kelas","tbl_wali_kelas");
			
			redirect("sekolah/walikelas");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	
	function delete_all()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['sekolah']) ? $_SESSION['sekolah']:'';
		if($session!=""){

		
			$data["del"] = $this->model_sekolah->hapus_konten($kode,"id_kelas","tbl_wali_kelas");
			
			redirect("sekolah/walikelas");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
