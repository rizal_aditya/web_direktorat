<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudkuis extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('tgl_indo');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->model("model_merchant");
		$this->load->model("mod_main");
		//session_start();
	}

	function index()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{
				redirect("merchant/kuis");
				
		}else{

				$this->load->view('function/login_merchant');
		}	
	}


	function insert()
	{

		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{
		
			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"]	;
			$id_merchant			= 	$data["id_merchant"];


			$judul_kuis				= 	$this->input->post('judul_kuis', TRUE);
			$pertanyaan				= 	$this->input->post('pertanyaan', TRUE);
			$a						= 	$this->input->post('a', TRUE);
			$b						= 	$this->input->post('b', TRUE);
			$c						= 	$this->input->post('c', TRUE);
			$d						= 	$this->input->post('d', TRUE);
			$jawaban				= 	$this->input->post('jawaban', TRUE);
			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tgl_expired			= 	$this->input->post('tgl_expired', TRUE);
			

			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'kuis/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;
			
			
			if(!empty($asli)){

			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/kuis/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite'] = TRUE;
			$config['max_size'] = '0';
			$config['max_width']  = '0';
			$config['max_height']  = '0'; 						
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
			 $thumbr = $thumb; 
			 
			 $q = "insert into tbl_kuis(judul_kuis,id_merchant,pertanyaan,a,b,c,d,jawab,status,broadcast,tgl_expired,photo,create_at) 
		  values('".$judul_kuis."','".$id_merchant."','".$pertanyaan."','".$a."','".$b."','".$c."','".$d."','".$jawaban."','".$status."','".$broadcast."','".$tgl_expired."','".$thumbr."',NOW())";
			$this->mod_main->create($q);
			 
			 
			 }else{ 
			 
			$q = "insert into tbl_kuis(judul_kuis,id_merchant,pertanyaan,a,b,c,d,jawab,status,broadcast,tgl_expired,create_at) 
		  values('".$judul_kuis."','".$id_merchant."','".$pertanyaan."','".$a."','".$b."','".$c."','".$d."','".$jawaban."','".$status."','".$broadcast."','".$tgl_expired."',NOW())";
			$this->mod_main->create($q); 
		




			 }		
			
		

								 	
			

	// gcm kuis									
			if($broadcast == "y")
				{
						
					$data_id = $this->model_merchant->get_IDKuis($id_merchant);				
					foreach ($data_id as $ray) 
					{
						$id_kuis 			= "".$ray['id_kuis']."";
						$judul 				= "".$ray['judul_kuis']."";
						$pertanyaan   		= "".$ray['pertanyaan']."";
						$a 					= "".$ray['a']."";
						$b 					= "".$ray['b']."";
						$c					= "".$ray['c']."";
						$d 					= "".$ray['d']."";
						$photo 				= "".$ray['photo']."";
						$status				= "".$ray['status']."";
						$tgl_expired		= "".$ray['tgl_expired']."";
						
						$create_at 	= "".$ray['create_at']."";
						$merchant 	= "".$ray['nama_merchant']."";
					}			
							
									$message = array(
										"id" 					=> $id_kuis,
										"a" 					=> $a,
										"b" 					=> $b,
										"c" 					=> $c,
										"d" 					=> $d,
										"pertanyaan" 			=> $pertanyaan,
										"judul" 				=> $judul,
										"photo" 				=> $photo,
										"nama_merchant" 		=> $merchant,
										"param"					=> "kuis",
										"sender"				=> $sender);	
								

					
								
									$api_key = $api_merchant;
									$member = $this->model_merchant->get_gcm_member();
									foreach ($member as $data)
									{
										$reg_id = "".$data['gcm']."";
										$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
									
									}					

				}
					
				redirect("merchant/kuis");
				
		}else{
			
		
			$this->load->view('function/login_merchant');
			
		}				
		
	}




	function update()
	{
		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!="")
		{

			$pecah					=	explode("|",$session);
			$data["id_merchant"]	=	$pecah[0];
			$data["nama_merchant"]	=	$pecah[1];
			$data["photo"]			=	$pecah[2];
			$data["api_key"]		=	$pecah[3];
			$data["sender"]			=	$pecah[4];
			$api_merchant			=   $data["api_key"];
			$sender					= 	$data["sender"]	;
			$id_merchant			= 	$data["id_merchant"];
			
			$id_kuis 				= 	$this->uri->segment(3);
			$judul_kuis				= 	$this->input->post('judul_kuis', TRUE);
			$pertanyaan				= 	$this->input->post('pertanyaan', TRUE);
			$a						= 	$this->input->post('a', TRUE);
			$b						= 	$this->input->post('b', TRUE);
			$c						= 	$this->input->post('c', TRUE);
			$d						= 	$this->input->post('d', TRUE);
			$jawaban				= 	$this->input->post('jawaban', TRUE);
			$status					= 	$this->input->post('status', TRUE);
			$broadcast				= 	$this->input->post('broadcast', TRUE);
			$tgl_expired			= 	$this->input->post('tgl_expired', TRUE);
			
			
			

			$gbr 		= $this->input->post('gbr');
	
			$code		=	rand(00000000000,999999);
			$asli 		= $_FILES['userfile']['name'];
			$covert 	= $this->mod_main->convert_name($asli);
			$nama 		= $code.'-'.'photo'.'-'.$covert;
			
			$alt		= 'assets/upload/';
			$dir 		= 'kuis/';
			$photo 		= $code.'-'.'photo'.'-'.$covert;
			$thumb 		= $code.'-'.'thumb'.'-'.$covert;		
			
			if(!empty($asli)){ 
			
			$config["file_name"]			= $thumb;
			$config['upload_path'] 			= './assets/upload/kuis/';
			$config['allowed_types'] 		= 'jpg|jpeg|png';
			$config['overwrite']			= TRUE;
			$config['max_size'] 			= '0';
			$config['max_width']  			= '0';
			$config['max_height']  			= '0'; 						
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			
			// resize

			$upload_data = $this->upload->data();
			$image_config["image_library"] = "gd2";
			$image_config["source_image"] = $upload_data["full_path"];
			$image_config['create_thumb'] = FALSE;
			$image_config['maintain_ratio'] = TRUE;
			$image_config['new_image'] = $upload_data["file_path"].$thumb;
			$image_config['quality'] = "100%";
			$image_config['width'] = 640;
			$image_config['height'] = 400;
			$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
			$image_config['master_dim'] = ($dim > 0)? "height" : "width";
			 
			$this->load->library('image_lib');
			$this->image_lib->initialize($image_config);		 
			$this->image_lib->resize();
				
			
			$thumbr = $thumb;
			$q_update 		= "update tbl_kuis set 
			judul_kuis 		= '".$judul_kuis."'
			, id_merchant 	= '".$id_merchant."'
			, pertanyaan 	= '".$pertanyaan."'
			, a 			= '".$a."'
			, b 			= '".$b."'
			, c 			= '".$c."'
			, d 			= '".$d."'
			, jawab 		= '".$jawaban."'
			, tgl_expired 	= '".$tgl_expired."'
			, status 		= '".$status."'
			, broadcast 	= '".$broadcast."' 
			, photo 		= '".$thumbr."'
			, update_at 	= NOW() 
			where id_kuis  	='".$id_kuis."'";
			$this->mod_main->put($q_update);	
			
			
					if(!empty($gbr))
					{
				
					$file_gambar  = './assets/upload/kuis/'.$gbr;
					unlink($file_gambar);
			
					}
			

			}else{ 
			
				$q_update 		= "update tbl_kuis set 
			judul_kuis 		= '".$judul_kuis."'
			, id_merchant 	= '".$id_merchant."'
			, pertanyaan 	= '".$pertanyaan."'
			, a 			= '".$a."'
			, b 			= '".$b."'
			, c 			= '".$c."'
			, d 			= '".$d."'
			, jawab 		= '".$jawaban."'
			, tgl_expired 	= '".$tgl_expired."'
			, status 		= '".$status."'
			, broadcast 	= '".$broadcast."' 
			
			, update_at 	= NOW() 
			where id_kuis  	='".$id_kuis."'";
			$this->mod_main->put($q_update);	
			
			
			
			}
						
					
				
			

				
			// gcm kuis									
			if($broadcast == "y")
				{
						
					$data_id = $this->model_merchant->get_IDKuis($id_merchant);				
					foreach ($data_id as $ray) 
					{
						$id_kuis 			= "".$ray['id_kuis']."";
						$judul 				= "".$ray['judul_kuis']."";
						$pertanyaan   		= "".$ray['pertanyaan']."";
						$a 					= "".$ray['a']."";
						$b 					= "".$ray['b']."";
						$c					= "".$ray['c']."";
						$d 					= "".$ray['d']."";
						$photo 				= "".$ray['photo']."";
						$status				= "".$ray['status']."";
						$tgl_expired		= "".$ray['tgl_expired']."";
						
						$create_at 	= "".$ray['create_at']."";
						$merchant 	= "".$ray['nama_merchant']."";
					}			
							
									$message = array(
										"id" 					=> $id_kuis,
										"a" 					=> $a,
										"b" 					=> $b,
										"c" 					=> $c,
										"d" 					=> $d,
										"pertanyaan" 			=> $pertanyaan,
										"judul" 				=> $judul,
										"photo" 				=> $photo,
										"nama_merchant" 		=> $merchant,
										"param"					=> "kuis",
										"sender"				=> $sender);	
								

					
								
									$api_key = $api_merchant;
									$member = $this->model_merchant->get_gcm_member();
									foreach ($member as $data)
									{
										$reg_id = "".$data['gcm']."";
										$gcm_id = $this->sendMessageThroughGCM($reg_id,$message,$api_key);
									
									}					

				}
							
				redirect("merchant/kuis");	


			}else{
			
		
			$this->load->view('function/login_merchant');
			
			}				


	}


	function delete()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!=""){

		
			

			$kuis = $this->model_merchant->delete_kuis_merchant($kode);
			foreach ($kuis as $data) 
			{

				$photo = "".$data['photo']."";

			}	

			if($photo != "")
			{	
			$gambar  = './assets/upload/kuis/'.$photo;
			unlink($gambar);
			}

			$data["del"] = $this->model_merchant->hapus_konten($kode,"id_kuis","tbl_kuis");

			redirect("merchant/kuis");

		}else{

			$this->load->view('function/login_merchant');
		}	
		
	}


	function delete_jawaban()
	{

		$kode='';		
		if ($this->uri->segment(3) === FALSE){
    		$kode='';
		}else{
    		$kode = $this->uri->segment(3);
		}

		$session=isset($_SESSION['merchant']) ? $_SESSION['merchant']:'';
		if($session!=""){

		
			$jawaban = $this->model_merchant->delete_jawaban_kuis($kode);
			foreach ($jawaban as $data) 
			{

				$id_kuis = "".$data['id_kuis']."";

			}	

			$data["del"] = $this->model_merchant->hapus_konten($kode,"id_jawaban","tbl_jawaban");
			$data["delP"] = $this->model_merchant->hapus_konten($kode,"id_jawaban","tbl_pemenang");
			
			redirect("merchant/kuis/hasil/$id_kuis");
			

		}else{

			$this->load->view('function/login_merchant');
		}		

	}	


	function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
		//Google cloud messaging GCM-API url
		$code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
		// Update your Google Cloud Messaging API Key
		//define("GOOGLE_API_KEY", $api_key); 		
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);	
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);				
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
