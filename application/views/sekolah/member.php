<div class="page-header">
	<h1>Direktorat Jendral
		<small><i class="ace-icon fa fa-angle-double-right"></i> <?=$deskripsi;?></small>
	</h1>
	
	<?php if(!empty($this->uri->segment(3))){?>
		<a class="tambah"  href="<?php echo base_url();?>direktorat/pegawai/">Lihat Pegawai<i class="menu-icon fa fa-eye"></i> </a>
	<?php }else{ ?>
	
			
		<a class="FormData tambah">Tambah <i class="menu-icon fa fa-plus"></i></a>
		<a class="FormData tambah" style="display:none">Tutup <i class="menu-icon fa fa-minus"></i></a>
			
	<?php } ?>	

</div>
<br>
<div id="sukses"></div>
<div class="alert_data"></div>

<script>
$().ready(function(){
        $(".FormData").click(function(){
            $(".FormData").toggle();
			$(".FormData_ul").toggle('fast');
        });
    });
</script>

<div class="FormData_ul" style="display:none">
<?php $this->load->view('sekolah/form_member_tambah'); ?>
</div>

<?php if($this->uri->segment(3) == "edit"){?>
<?php $this->load->view('sekolah/form_member_edit'); ?>
<?php } ?>
</br>

<?php if(empty($this->uri->segment(4))){?>

<div class="table-header"><?=$deskripsi;?></div>
	<div class="table-responsive">
	<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Foto</th>
				<th>Nama</th>
				<th>Email</th>
				<th>Telp</th>
				<th>Alamat</th>
				<th>Status</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; if(!empty($pegawai)){foreach($pegawai as $row){?>
			<tr>
				<td>
					<?=$i;?>
				</td>
				<td>
					<?php if($row['photo'] == "")
					{ 
						echo"<img class='image_conten' src='http://apisekolah.kartuvirtual.com/assets/member/no-image.jpg'>";
					}else{ 
						echo"<img class='image_conten' src='http://apisekolah.kartuvirtual.com/".$row['photo']."'>";
					}?>
      			</td>
				<td><?php echo $row['nama_member'];?></td>
				<td><?php echo $row['email'];?></td>
				<td><?php echo $row['telp'];?></td>
				<td><?php echo $row['alamat'];?></td>
				
				<td><?php if($row['status'] =="n"){echo "Tidak Aktif";}else{echo"Aktif";}?></td>

				<td>
					<div class="action-buttons">
								
<div class="dropdown">
							
  <a class="btne  dropdown-toggle"  data-toggle="dropdown" ><span class="caret"></span></a>
  <ul class="dropdown-menu" >
    <li><a href="<?php echo base_url();?>direktorat/pegawai/detail/<?php echo $row['id_member'];?>"><i class="menu-icon fa fa-eye"></i> Detail </a></li>
	  <li><a href="<?php echo base_url();?>direktorat/pegawai/edit/<?php echo $row['id_member'];?>"><i class="menu-icon fa fa-pencil"></i> Edit </a></li>
    <li role="separator" class="divider"></li>
	<?php if($row['status'] =="y"){?>
    <li><a onclick="return confirm('Apakah anda yakin ingin menonaktifkan data ini ?')" href="<?php echo base_url();?>Crud_peserta/nonaktif/<?php echo $row['id_member'];?>"><i class="menu-icon fa fa-minus-circle"></i> Non Aktif</a></li>
	<?php }else{ ?>	
	<li><a onclick="return confirm('Apakah anda yakin ingin aktifkan data ini ?')" href="<?php echo base_url();?>Crud_peserta/aktif/<?php echo $row['id_member'];?>"><i class="menu-icon fa fa-refresh"></i> Aktif</a></li>
	<?php } ?>
	<li><a onclick="return confirm('Apakah anda yakin ingin aktifkan data ini ?')" href="<?php echo base_url();?>Crud_peserta/delete/<?php echo $row['id_member'];?>"><i class="menu-icon fa fa-times"></i> Delete</a></li>
  </ul>
</div>
			
			<!-- /btn-group -->
						
					</div>
				</td>
			</tr>

			<?php $i++; }} ?>
		</tbody>
	</table>
</div>
</br>

<?php }else if($this->uri->segment(3) == "detail"){?>

<?php  $this->load->view('sekolah/detail_member'); ?>

<?php }?>







