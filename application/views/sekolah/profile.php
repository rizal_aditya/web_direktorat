

<div class="page-header">
	<h1>Direktorat Jendral
		<small><i class="ace-icon fa fa-angle-double-right"></i> <?=$deskripsi;?></small>
	</h1>
  
</div>

<br>


<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Data Profile Instansi</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >

<div id="data">					
	<div class="row">
		<div id="postio" class="">
		<?php  foreach($profile as $row){?>
			
						<table  id="textField" class="table"  width="100%">
						<tr>
						<td rowspan="12">

							<?php if($row['photo'] ==""){?>

						         <img style="border:1px solid #ddd;"  src="<?php echo base_url();?>assets/images/no-image.jpg" />
						    		<?php }else{?>
						        <img style="border:1px solid #ddd;"   width="180" height="180" src="<?php echo base_url();?>assets/upload/merchant/<?php echo $row['photo'];?>" />

						    <?php } ?></td>	
						<td><b>Name</td>
						<td><b>:</b></td>
						<td><?php echo $row['nama_merchant'];?></td>
						</tr>
						<tr>
						<td><b>Profile Perusahaan</td>
						<td><b>:</b></td>
						<td><div style="width:500px;text-align:justify;"><?php echo $row['deskripsi'];?></div></td>
						</tr>		
						<tr>
						<td><b>Email</td>
						<td><b>:</b></td>
						<td><?php echo $row['email'];?></td>
						</tr>	
						
						
						<tr>
						<td><b>Website</b></td>
						<td><b>:</td>
						<td><?php echo $row['website'];?></td>
						</tr> 
						<tr>
						<td><b>PIN BB</b></td>
						<td><b>:</td>
						<td><?php echo $row['pin_bb'];?></td>
						</tr> 
						<tr>
						<td><b>WhatsApp</b></td>
						<td><b>:</b></td>
						<td><?php echo $row['wa'];?></td>
						</tr> 
						<tr>
						<td><b>Line</b></td>
						<td><b>:</b></td>
						<td><?php echo $row['line'];?></td>
						</tr> 
						<tr>
						<td><b>Status</b></td>
						<td><b>:</b></td>
						<td>

						<?php if($row['active'] == "y"){?>
								Aktif
							<?php }else{?>
								Tidak Aktif
							<?php } ?>

						</td>
						</tr>	
						</table>
				 <?php } ?>					
			</div>	 


	</div>
					<div class="form-actions left">
						<a id="EditForm" href="#" class="btn btn-sm btn-primary">Edit Data</a>
					</div>
</div>


<!-- List Data form -->
<div id="FormData" style="display:none;">
	<form action="<?php echo base_url();?>user_akses/update_profilesekolah"  method="post" enctype="multipart/form-data">
				<input type="hidden" value="<?php echo $row['photo'];?>" name="gbr" >
			<div class="row">
				<div id="postio" class="">

				<?php foreach($profile as $row){?>
				<table id="textField" class="table"  width="100%">
				<tr>
				<td><b>Nama Instansi</b></td>
				<td><b>:</b></td>
				<td><input type="text" class="fieldStyle" name="nama_merchant" value="<?php echo $row['nama_merchant'];?>"></td>
				</tr>

				<tr>
				<td><b>Profile Perusahaan</b></td>
				<td><b>:</b></td>
				<td><textarea  style="width:407px;height:200px;resize:none;"  name="deskripsi"><?php echo $row['deskripsi'];?></textarea></td>
				</tr>

				

				<tr>
				<td><b>Email</b></td>
				<td><b>:</b></td>
				<td><input class="fieldStyle" type="text" name="email" value="<?php echo $row['email'];?>"></td>
				</tr>

				
				<tr>
				<td><b>Website</b></td>
				<td><b>:</b></td>
				<td><input class="fieldStyle" type="text" name="website" value="<?php echo $row['website'];?>"></td>
				</tr>
				<tr>
				<td><b>Youtube</b></td>
				<td><b>:</b></td>
				<td><input class="fieldStyle" type="text" name="youtube" value="<?php echo $row['youtube'];?>"></td>
				</tr>
				<tr>
				<td><b>Pin bb</b></td>
				<td><b>:</b></td>
				<td><input class="fieldStyle" type="text" name="pin_bb" value="<?php echo $row['pin_bb'];?>"></td>
				</tr>
				<tr>
				<td><b>Whatapps</b></td>
				<td><b>:</b></td>
				<td><input type="text" name="wa" value="<?php echo $row['wa'];?>"></td>
				</tr>
				<tr>
				<td><b>Line</b></td>
				<td><b>:</b></td>
				<td><input class="fieldStyle" type="text" name="line" value="<?php echo $row['line'];?>"></td>
				</tr>
				
				
				
				</tr>
				</table>
						

				<?php } ?>

				</div>
			</div>

		<div class="form-actions left">
			<button id="balik" type="button" class="btn btn-sm btn-warning">Kembali</button>
			<button type="submit" class="btn btn-sm btn-success">Simpan</button>		
		</div>
		
		
	</form>
</div>


				</div>
			</div>
		</div>
	</div>
</div>


<script>

//lihat form	
 $("#EditForm").click(function(){
    $("#FormData").slideUp().toggle("slow") ;
	$("#data").hide();
});
 //balik profile
 $("#balik").click(function(){
    $("#data").slideUp().toggle("slow") ;
	$("#FormData").hide();
});




</script>

























