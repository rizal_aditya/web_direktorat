<?php if(!empty($this->uri->segment(4))) { foreach($akademik as $row){?>



<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Edit Kalender Akademik</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crud_akademik/update/<?php echo $this->uri->segment(4)?>"  onsubmit="return FormKalender(judul,tgl_mulai,warna,status);">

					<div class="row">
						<div id="kiri" class="col-sm-70">

<table id="textField" class="table"  width="100%">

<tr>
<td><b>Nama Agenda</b></td>
<td><b>:</b></td>
<td>
<input type="text" class="fieldStyle" name="judul" value="<?php echo $row['judul'];?>">
</td>
</tr>
<tr>
<td><b>Tanggal Agenda Mulai</b></td>
<td><b>:</b></td>
<td>
 <input type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" name="tgl_mulai" value="<?php echo $row['tgl_mulai'];?>">   
 </td>
</tr>
<tr>
<td><b>Tanggal Agenda Selesai</b></td>
<td><b>:</b></td>
<td>
 <input type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" name="tgl_selesai" value="<?php echo $row['tgl_selesai'];?>">   
 </td>
</tr>
<tr>
<td><b>Type</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="type">
	<?php if($row['type'] == "" ){ ?>
	<option value="">Pilih Type</option>
	<option value="umum">Umum</option>
	<option value="staff">Staff</option>
	<option value="pimpinan">Pimpinan</option>
	<?php }else if($row['type'] == "umum" ){ ?>
	<option value="">Pilih Type</option>
	<option value="umum" selected>Umum</option>
	<option value="staff">Staff</option>
	<option value="pimpinan">Pimpinan</option>
	<?php }else if($row['type'] == "staff" ){ ?>
	<option value="">Pilih Type</option>
	<option value="umum">Umum</option>
	<option value="staff" selected>Staff</option>
	<option value="pimpinan">Pimpinan</option>
	<?php }else if($row['type'] == "pimpinan" ){ ?>
	<option value="">Pilih Type</option>
	<option value="umum">Umum</option>
	<option value="staff">Staff</option>
	<option value="pimpinan" selected>Pimpinan</option>
	<?php } ?>
</select>  
 </td>
</tr>
<tr>
<td><b>Satuan Kerja</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="id_cabang">
<option value="">Pilih Satuan Kerja</option>
	<?php foreach($divisi as $rowe){?>
		<?php if($rowe['id_cabang'] == $row['id_cabang']){ ?>
				<option value="<?php echo $rowe['id_cabang'];?>" selected><?php echo $rowe['nama_cabang'];?></option>
		<?php }else{ ?>
				<option value="<?php echo $rowe['id_cabang'];?>"><?php echo $rowe['nama_cabang'];?></option>
		<?php } ?>
	<?php } ?>
</select>  
 </td>
</tr>
<tr>
<td><b>Warna</b></td>
<td><b>:</b></td>
<td>
 <div id="seld">
	 <input class="rdo" value="merah" name="warna" type="radio" <?php if($row['warna'] == 'merah') echo "checked";?> /> <div class="stse">Merah </div>
	  <input class="rdo" value="kuning" name="warna" type="radio" <?php if($row['warna'] == 'kuning') echo "checked";?> /> <div class="stse">Kuning </div>
	  <input class="rdo" value="hijau" name="warna" type="radio" <?php if($row['warna'] == 'hijau') echo "checked";?> /> <div class="stse">Hijau </div>
	  <input class="rdo" value="biru" name="warna" type="radio" <?php if($row['warna'] == 'biru') echo "checked";?> /> <div class="stse">Biru </div>
  </div>
 </td>
</tr>


<tr>
<td><b>Deskripsi</b></td>
<td><b>:</b></td>
<td><textarea id="deskripsie"  style="width:407px;height:200px;resize:none;" name="deskripsi"><?php echo $row['deskripsi'];?></textarea></td>
</tr>

<tr>
    <td><b>Status</b></td>
    <td ><b>:</b></td>
    <td>
	<div id="seld">
 <input class="rdo" value="n" name="status" type="radio" <?php if($row['status'] == 'n') echo "checked";?> /> <div class="stse">Tidak Aktif</div>
<input class="rdo" value="y" name="status" type="radio" <?php if($row['status'] == 'y') echo "checked";?> /> <div class="stse">Aktif</div>
</div>
    </td>
</tr>

</table>


							
							
							
							
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- edit berita -->
<?php }}  ?>
