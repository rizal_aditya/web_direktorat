
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Input Agenda</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crud_akademik/insert"  onsubmit="return FormKalender(judul,tgl_mulai,warna,status);">

					<div class="row">
						<div id="kiri" class="col-sm-70">

<table id="textField" class="table"  width="100%">
<tr>
<td><b>Agenda</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="judul">
</td>
</tr>


<tr>
<td><b>Tanggal Agenda Mulai</b></td>
<td><b>:</b></td>
<td>
 <input type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd"  name="tgl_mulai">   
 </td>
</tr>
<tr>
<tr>
<td><b>Tanggal Agenda Selesai</b></td>
<td><b>:</b></td>
<td>
 <input type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd"  name="tgl_selesai">   
 </td>
</tr>
<tr>

<tr>
<td><b>Type</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="type">
	<option value="">Pilih Type</option>
	<option value="umum">Umum</option>
	<option value="staff">Staff</option>
	<option value="pimpinan">Pimpinan</option>
</select>  
 </td>
</tr>
<tr>


<tr>
<td><b>Satuan Kerja</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="id_cabang">
	<option value="">Pilih Satuan Kerja</option>
	<?php foreach($divisi as $rowe){?>
		<option value="<?php echo $rowe['id_cabang'];?>"><?php echo $rowe['nama_cabang'];?></option>
	<?php } ?>
</select>  
 </td>
</tr>
<tr>

	

<td><b>Warna</b></td>
<td><b>:</b></td>
<td>
	<div id="seld">
	 <input class="rdo" value="merah" name="warna" type="radio" /> <div class="stse">Merah </div>
	  <input class="rdo" value="kuning" name="warna" type="radio"/> <div class="stse">Kuning </div>
	  <input class="rdo" value="hijau" name="warna" type="radio" /> <div class="stse">Hijau </div>
	  <input class="rdo" value="biru" name="warna" type="radio" /> <div class="stse">Biru </div>
  </div>
 </td>
</tr>
<tr>
<td><b>Deskripsi</b></td>
<td><b>:</b></td>
<td>
<textarea id="deskripsie" style="width:407px;height:200px;resize:none;" name="deskripsi"></textarea>
</td>
</tr>
<tr>
    <td><b>Status</b></td>
    <td><b>:</b></td>
    <td>
	<div id="seld">
 <input id="status1" class="rdo" value="n" name="status" type="radio" /> <div class="stse">Tidak Aktif</div>
  <input  id="status2" class="rdo" value="y" name="status" type="radio" /> <div class="stse">Aktif</div>
  </div>
    </td>
</tr>
</table>

						
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




