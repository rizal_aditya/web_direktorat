<div class="page-header">
	<h1>Direktorat Jendral
		<small><i class="ace-icon fa fa-angle-double-right"></i> <?=$deskripsi;?></small>
	</h1>

	<?php if(!empty($this->uri->segment(3))){?>
		<a class="tambah"  href="<?php echo base_url();?>direktorat/agenda/">Lihat Agenda <i class="menu-icon fa fa-eye"></i> </a>
	<?php }else{ ?>
	
			
		<a class="FormData tambah">Tambah <i class="menu-icon fa fa-plus"></i></a>
	
		<a class="FormData tambah" style="display:none">Tutup <i class="menu-icon fa fa-minus"></i></a>
			
	<?php } ?>	
</div>

<br>
<div id="sukses"></div>
<div class="alert_data"></div>


<script>
$().ready(function(){
        $(".FormData").click(function(){
            $(".FormData").toggle();
			$(".FormData_ul").toggle('fast');
        });
    });
</script>

<div class="FormData_ul" style="display:none">
<?php $this->load->view('sekolah/form_kalender_tambah'); ?>
</div>

<?php if($this->uri->segment(3) == "edit"){?>
<?php $this->load->view('sekolah/form_kalender_edit'); ?>
<?php } ?>
</br>

<?php if(empty($this->uri->segment(4))){?>
	
<div class="table-header"><?=$deskripsi;?></div>
	<div class="table-responsive">
	<table id="dynamic-table" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Agenda</th>
				<th>Deskripsi</th>
				<th>Warna</th>	
				<th>Tanggal</th>
				<th>Type</th>
				<th>Status</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; if(!empty($akademik)){foreach($akademik as $row){?>
			<tr>
				<td><?=$i;?></td>
				<td><?php echo $row['judul'];?></td>
				<td><?php echo $row['description'];?></td>
				<td>
				<?php if(!empty($row['warna'])){ ?>
				
				<?php echo $row['warna'];?>
					
				<?php } ?>
				
				</td>
				
				<td>
					<?php echo tgl_indo($row['tgl_mulai']);?>
      			</td>
				<td>
					<?php echo $row['type'];?>
      			</td>
				<td><?php if($row['status'] =="n"){echo "Tidak Aktif";}else{echo"Aktif";}?></td>
				<td>
					<div class="action-buttons">
						
					
					<div class="dropdown">				
						  <a class="btne  dropdown-toggle"  data-toggle="dropdown" ><span class="caret"></span></a>
						  <ul class="dropdown-menu" >
						
							
							<li><a  href="<?php echo base_url();?>direktorat/agenda/edit/<?php echo $row['id_kalender'];?>"><i class="ace-icon fa fa-pencil"></i> Edit Data</a></li>
							
							<li><a onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?php echo base_url();?>crud_akademik/delete/<?php echo $row['id_kalender'];?>"><i class="menu-icon fa fa-times"></i> Hapus Data</a></li>
							
						  </ul>
					</div>
					
					
					
					
					
					
					
					</div>
				</td>
			</tr>
			<?php $i++; }} ?>


		</tbody>
	</table>

</div>

<?php } ?>

	
</br>
	
