
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Edit Pegawai</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crudmember/update/<?php echo $this->uri->segment(4);?>"  onsubmit="return FormPegawai(nama_member,email,agama,tgl_lahir,nip,id_cabang,jabatan,type,status);">

					<div class="row">
						<div id="kiri" class="col-sm-70">
<?php foreach($pegawai as $row){?>
<table id="textField" class="table"  width="100%">
<tr>
<td><b>Nama Pegawai</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="nama_member" value="<?php echo $row['nama_member'];?>">
</td>
</tr>

<tr>
<td><b>No Telp</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="telp" value="<?php echo $row['telp'];?>">
<input  type="hidden" class="fieldStyle" name="email" value="<?php echo $row['email'];?>">
</td>
</tr>
<tr>
<td><b>Agama</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="agama">
<?php if($row['agama'] ==""){ ?>
<option value="">Pilih Agama</option>
<option value="islam">Islam</option>
<option value="katolik">Katolik</option>
<option value="nasrani">Nasrani</option>
<option value="buddha">Buddha</option>
<option value="hindu">Hindhu</option>
<option value="konghucu">Konghucu</option>
<?php }else if($row['agama'] =="islam"){ ?>
<option value="">Pilih Agama</option>
<option value="islam" selected>Islam</option>
<option value="katolik">Katolik</option>
<option value="nasrani">Nasrani</option>
<option value="buddha">Buddha</option>
<option value="hindu">Hindhu</option>
<option value="konghucu">Konghucu</option>
<?php }else if($row['agama'] =="katolik"){ ?>
<option value="">Pilih Agama</option>
<option value="islam">Islam</option>
<option value="katolik" selected>Katolik</option>
<option value="nasrani">Nasrani</option>
<option value="buddha">Buddha</option>
<option value="hindu">Hindhu</option>
<option value="konghucu">Konghucu</option>
<?php }else if($row['agama'] =="nasrani"){ ?>
<option value="">Pilih Agama</option>
<option value="islam">Islam</option>
<option value="katolik">Katolik</option>
<option value="nasrani" selected>Nasrani</option>
<option value="buddha">Buddha</option>
<option value="hindu">Hindhu</option>
<option value="konghucu">Konghucu</option>
<?php }else if($row['agama'] =="buddha"){ ?>
<option value="">Pilih Agama</option>
<option value="islam">Islam</option>
<option value="katolik">Katolik</option>
<option value="nasrani">Nasrani</option>
<option value="buddha" selected>Buddha</option>
<option value="hindu">Hindhu</option>
<option value="konghucu">Konghucu</option>
<?php }else if($row['agama'] =="hindu"){ ?>
<option value="">Pilih Agama</option>
<option value="islam">Islam</option>
<option value="katolik">Katolik</option>
<option value="nasrani">Nasrani</option>
<option value="buddha">Buddha</option>
<option value="hindu" selected>Hindhu</option>
<option value="konghucu">Konghucu</option>
<?php }else if($row['agama'] =="konghucu"){ ?>
<option value="">Pilih Agama</option>
<option value="islam">Islam</option>
<option value="katolik">Katolik</option>
<option value="nasrani">Nasrani</option>
<option value="buddha">Buddha</option>
<option value="hindu">Hindhu</option>
<option value="konghucu" selected>Konghucu</option>
<?php } ?>

</select>


</td>
</tr>

<tr>
<td><b>Tanggal Lahir</b></td>
<td><b>:</b></td>
<td>
 <input type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd"  name="tgl_lahir" value="<?php echo $row['tgl_lahir'];?>">   
 </td>
</tr>
<tr>

<tr>
<td><b>NIP</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="nip" value="<?php echo $row['nip'];?>">
</td>
</tr>

<tr>
<td><b>Unit</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="id_cabang">
	<option value="">Pilih Unit</option>
	<?php foreach($unit as $unt){ ?>
		<?php if($unt['id_cabang'] == $row['id_cabang']){?>
			<option value="<?php echo $unt['id_cabang'];?>" selected><?php echo $unt['nama_cabang'];?></option>
		<?php }else{ ?>
			<option value="<?php echo $unt['id_cabang'];?>" ><?php echo $unt['nama_cabang'];?></option>

		<?php } ?>	
	<?php } ?>
</select>  
 </td>
</tr>

<tr>
<td><b>Jabatan</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="jabatan">
<?php if($row['jabatan'] == "") { ?>
<option value="">Pilih Jabatan</option>
<option value="pimpinan">Pimpinan</option>
<option value="eselon1">Eselon 1</option>
<option value="eselon2">Eselon 2</option>
<option value="eselon3">Eselon 3</option>
<option value="eselon4">Eselon 4</option>
<option value="staff">Staff</option>
<?php }else if($row['jabatan'] == "pimpinan"){ ?>
<option value="">Pilih Jabatan</option>
<option value="pimpinan" selected>Pimpinan</option>
<option value="eselon1" >Eselon 1</option>
<option value="eselon2">Eselon 2</option>
<option value="eselon3">Eselon 3</option>
<option value="eselon4">Eselon 4</option>
<option value="staff">Staff</option>
<?php }else if($row['jabatan'] == "eselon1"){ ?>
<option value="">Pilih Jabatan</option>
<option value="pimpinan">Pimpinan</option>
<option value="eselon1" selected>Eselon 1</option>
<option value="eselon2">Eselon 2</option>
<option value="eselon3">Eselon 3</option>
<option value="eselon4">Eselon 4</option>
<option value="staff">Staff</option>
<?php }else if($row['jabatan'] == "eselon2"){ ?>
<option value="">Pilih Jabatan</option>
<option value="pimpinan">Pimpinan</option>
<option value="eselon1">Eselon 1</option>
<option value="eselon2" selected>Eselon 2</option>
<option value="eselon3">Eselon 3</option>
<option value="eselon4">Eselon 4</option>
<option value="staff">Staff</option>
<?php }else if($row['jabatan'] == "eselon3"){ ?>
<option value="">Pilih Jabatan</option>
<option value="pimpinan">Pimpinan</option>
<option value="eselon1">Eselon 1</option>
<option value="eselon2">Eselon 2</option>
<option value="eselon3" selected>Eselon 3</option>
<option value="eselon4">Eselon 4</option>
<option value="staff">Staff</option>
<?php }else if($row['jabatan'] == "eselon4"){ ?>
<option value="">Pilih Jabatan</option>
<option value="pimpinan">Pimpinan</option>
<option value="eselon1">Eselon 1</option>
<option value="eselon2">Eselon 2</option>
<option value="eselon3">Eselon 3</option>
<option value="eselon4" selected>Eselon 4</option>
<option value="staff">Staff</option>
<?php }else if($row['jabatan'] == "staff"){ ?>
<option value="">Pilih Jabatan</option>
<option value="pimpinan">Pimpinan</option>
<option value="eselon1">Eselon 1</option>
<option value="eselon2">Eselon 2</option>
<option value="eselon3">Eselon 3</option>
<option value="eselon4">Eselon 4</option>
<option value="staff" selected>Staff</option>
<?php } ?>
</select>
</td>
</tr>

<tr>
<td><b>Type</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="type">
	<?php if($row['type'] == ""){ ?>
	<option value="">Pilih Type</option>
	<option value="staff">Staff</option>
	<option value="pimpinan">Pimpinan</option>
	<?php }else if($row['type'] == "staff"){?>
	<option value="">Pilih Type</option>
	<option value="staff" selected>Staff</option>
	<option value="pimpinan">Pimpinan</option>
	<?php }else if($row['type'] == "pimpinan"){?>
	<option value="">Pilih Type</option>
	<option value="staff">Staff</option>
	<option value="pimpinan" selected>Pimpinan</option>
	
	<?php } ?>
</select>  

 </td>
</tr>

<tr>
<td><b>Alamat</b></td>
<td><b>:</b></td>
<td>
<textarea  style="width:407px;height:200px;resize:none;" name="alamat"><?php echo $row['alamat'];?></textarea>
</td>
</tr>
<tr>
    <td><b>Status</b></td>
    <td><b>:</b></td>
    <td>
	<div id="seld">
 <input class="rdo" value="n" name="status" type="radio" <?php if($row['status'] == 'n') echo "checked";?> /> <div class="stse">Tidak Aktif</div>
<input class="rdo" value="y" name="status" type="radio" <?php if($row['status'] == 'y') echo "checked";?> /> <div class="stse">Aktif</div>
  </div>
    </td>
</tr>
</table>
<?php } ?>
						
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




