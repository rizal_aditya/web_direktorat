

<script>
$(document).ready(function(){
	$("#satu").click(function(){
		$("#pilihan").fadeIn();
			
	});
	
	$("#dua").click(function(){
		$("#pilihan").fadeIn();	
		$("#cari_member").fadeOut();
		$("#cari_kelas").fadeOut();
	});

	$("#pribadi").click(function(){
		$("#cari_member").fadeIn();
		$("#cari_kelas").fadeOut();
	});
	
	$("#kelas").click(function(){
		$("#cari_member").fadeOut();
		$("#cari_kelas").fadeIn();
	});
	
	

	$("#all").click(function(){
		$("#cari_member").fadeOut();
		$("#cari_kelas").fadeOut();
			
	});
	
	
	
});
</script>
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Input Direktorat</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crud_direktorat/insert" onsubmit="return FormInfo(judul,status);">

					<div class="row">
						<div id="kiri" class="col-sm-70">

<table id="textField" class="table"  width="100%">
<tr>
  
<td><b>Judul</b></td>
<td><b>:</b></td>
<td>
   
<input id="judul" type="text" class="fieldStyle" name="judul">
<input  type="hidden" class="fieldStyle" name="tipe" value="direktorat">
</td>
</tr>
<tr>
<td><b>Divisi</b></td>
<td><b>:</b></td>
<td>
<select name="divisi" class="selectpicker">
<option value="">Pilih Divisi</option>
<?php foreach($unit as $row){ ?>
<option value="<?php echo $row['id_cabang'];?>"><?php echo $row['nama_cabang'];?></option>
<?php }?>
</select>
</td>
</tr>


<tr>
<td><b>Isi</b></td>
<td><b>:</b></td>
<td>
<textarea  style="width:407px;height:200px;resize:none;" name="isi"></textarea>
</td>
</tr>
<tr>
<td><b>Foto</b></td>
<td><b>:</b></td>
<td>
<input id="file" type="file" name='userfile' >
</td>
</tr>
<tr>
    <td><b>Status</b></td>
    <td ><b>:</b></td>
    <td><div id="seld">
 <input id="status1" class="rdo" value="n" name="status" type="radio" /> <div class="stse">Tidak Aktif</div>
  <input  id="status2" class="rdo" value="y" name="status" type="radio" /> <div class="stse">Aktif</div>
  </div>
    </td>
</tr>


</table>

						
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>



