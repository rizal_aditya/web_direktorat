

<script>
$(document).ready(function(){
	$("#satu").click(function(){
		$("#pilihan").fadeIn();
			
	});
	
	$("#dua").click(function(){
		$("#pilihan").fadeIn();	
		$("#cari_member").fadeOut();
		$("#cari_kelas").fadeOut();
	});

	$("#pribadi").click(function(){
		$("#cari_member").fadeIn();
		$("#cari_kelas").fadeOut();
	});
	
	$("#kelas").click(function(){
		$("#cari_member").fadeOut();
		$("#cari_kelas").fadeIn();
	});
	
	

	$("#all").click(function(){
		$("#cari_member").fadeOut();
		$("#cari_kelas").fadeOut();
			
	});
	
	
	
});
</script>
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Input Notifikasi</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crud_notifikasi/insert" onsubmit="return FormNotifikasi(judul,tgl_mulai,set_waktu,status,broadcast);">

					<div class="row">
						<div id="kiri" class="col-sm-70">

<table id="textField" class="table"  width="100%">
<tr>
  
<td><b>Judul</b></td>
<td><b>:</b></td>
<td>
   
<input id="judul" type="text" class="fieldStyle" name="judul">
<input  type="hidden" class="fieldStyle" name="tipe" value="notifikasi">
</td>
</tr>
<tr>
<td><b>Isi</b></td>
<td><b>:</b></td>
<td>
<textarea  style="width:407px;height:200px;resize:none;" name="isi"></textarea>
</td>
</tr>

<tr>
<td><b>Tanggal</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd"  name="tgl_mulai">   
</td>
</tr>

<tr>
<td><b>Set Waktu</b></td>
<td><b>:</b></td>
<td>
	
		
<input id="timepicker1" type="text" name="set_waktu" class="" />
			

</td>
</tr>

<tr>
    <td><b>Status</b></td>
    <td ><b>:</b></td>
    <td><div id="seld">
 <input  class="rdo" value="n" name="status" type="radio" /> <div class="stse">Tidak Aktif</div>
  <input class="rdo" value="y" name="status" type="radio" /> <div class="stse">Aktif</div>
  </div>
    </td>
</tr>
<tr>
<td><b>Broadcast</b></td>
<td ><b>:</b></td>
<td>
<div id="seld">
 <input  class="rdo" value="n" name="broadcast" type="radio" /> <div class="stse">Off</div>
  <input  class="rdo" value="y" name="broadcast" type="radio" /> <div class="stse">On</div>
  </div>
</td>
</tr>

</table>

						
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>





