
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Input Pegawai</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crudmember/insert"  onsubmit="return FormPegawai(nama_member,email,agama,tgl_lahir,nip,id_cabang,jabatan,type,status);">

					<div class="row">
						<div id="kiri" class="col-sm-70">

<table id="textField" class="table"  width="100%">
<tr>
<td><b>Nama Pegawai</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="nama_member">
</td>
</tr>
<tr>
<td><b>Email</b></td>
<td><b>:</b></td>
<td>
<input  type="email" class="fieldStyle" name="email">
</td>
</tr>
<tr>
<td><b>No Telp</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="telp">
</td>
</tr>
<tr>
<td><b>Agama</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="agama">
<option value="">Pilih Agama</option>
<option value="islam">Islam</option>
<option value="katolik">Katolik</option>
<option value="nasrani">Nasrani</option>
<option value="buddha">Buddha</option>
<option value="hindu">Hindhu</option>
<option value="konghucu">Konghucu</option>
</select>
</td>
</tr>

<tr>
<td><b>Tanggal Lahir</b></td>
<td><b>:</b></td>
<td>
 <input type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd"  name="tgl_lahir">   
 </td>
</tr>
<tr>

<tr>
<td><b>NIP</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="nip">
</td>
</tr>

<tr>
<td><b>Unit</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="id_cabang">
	<option value="">Pilih Unit</option>
	<?php foreach($unit as $unt){ ?>
	<option value="<?php echo $unt['id_cabang'];?>"><?php echo $unt['nama_cabang'];?></option>
	<?php } ?>
</select>  
 </td>
</tr>

<tr>
<td><b>Jabatan</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="jabatan">
<option value="">Pilih Jabatan</option>
<option value="pimpinan">Pimpinan</option>
<option value="eselon1">Eselon 1</option>
<option value="eselon2">Eselon 2</option>
<option value="eselon3">Eselon 3</option>
<option value="eselon4">Eselon 4</option>
<option value="staff">Staff</option>

</select>
</td>
</tr>


<tr>
<td><b>Type</b></td>
<td><b>:</b></td>
<td>
<select class="selectpicker" name="type">
	<option value="">Pilih Type</option>
	<option value="staff">Staff</option>
	<option value="pimpinan">Pimpinan</option>
</select>  
 </td>
</tr>

<tr>
<td><b>Alamat</b></td>
<td><b>:</b></td>
<td>
<textarea id="deskripsie" style="width:407px;height:200px;resize:none;" name="alamat"></textarea>
</td>
</tr>
<tr>
    <td><b>Status</b></td>
    <td><b>:</b></td>
    <td>
	<div id="seld">
 <input id="status1" class="rdo" value="n" name="status" type="radio" /> <div class="stse">Tidak Aktif</div>
  <input  id="status2" class="rdo" value="y" name="status" type="radio" /> <div class="stse">Aktif</div>
  </div>
    </td>
</tr>
</table>

						
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




