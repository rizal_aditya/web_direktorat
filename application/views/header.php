<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<meta name="keywords" content="<?php echo $header_keyword?>" />
		<meta name="description" content="<?php echo $header_desc?>" />
		<title>Kementerian Pendidikan & Kebudayaan | <?php echo $header_title; ?></title>
		<link href="<?php echo base_url();?>/assets/images/favicone.png" rel="shortcut icon">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/font-awesome/4.2.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/fonts/fonts.googleapis.com.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/ace.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/chosen.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/datepicker.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-select.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/editor.css" type="text/css" />

		<script src="<?=base_url();?>assets/js/ace-extra.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.2.1.1.min.js"></script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?=base_url();?>assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?=base_url();?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
		<script src="<?=base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
		<script src="<?=base_url();?>assets/js/date-time/bootstrap-timepicker.js"></script>
		<script src="<?=base_url();?>assets/js/bootstrap-select.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
		<script src="<?=base_url();?>assets/js/dataTables.tableTools.min.js"></script>
		<script src="<?=base_url();?>assets/js/dataTables.colVis.min.js"></script>
		<script src="<?=base_url();?>assets/js/ace-elements.min.js"></script>
		<script src="<?=base_url();?>assets/js/ace.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery-ui.custom.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="<?=base_url();?>assets/js/chosen.jquery.min.js"></script>
		<script src="<?=base_url();?>assets/js/fuelux.spinner.min.js"></script>
		<script src="<?=base_url();?>assets/js/moment.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.knob.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.autosize.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?=base_url();?>assets/js/jquery.maskedinput.min.js"></script>
		<script src="<?=base_url();?>assets/js/bootstrap-tag.min.js"></script>
		<script src="<?=base_url();?>assets/js/alert.js"></script>
		<script  src="<?=base_url();?>assets/ckeditor/ckeditor.js"></script>
		<script  src="<?=base_url();?>assets/js/jscolor.js"></script>
		
		<script type="text/javascript">
			$(document).ready( function () {
			    $('#dynamic-table').DataTable();
			   
			} );
		</script>
		

		<script type="text/javascript">
$(document).ready(function() {
    $('#checkAll').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });

  	$('#Det').change( function() {
        $('#formfE')[0].submit();
        parent.updating();
    });
   
});

$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 4
});


</script>
<script>

function cariKelas(k){ 
$(document).ready(function(){
	 	
			$('#').change( function() {
				$('#form')[0].submit();
				parent.updating();
			});

});
}


</script>

	</head>