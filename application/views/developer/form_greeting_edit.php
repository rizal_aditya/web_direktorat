<?php if(!empty($this->uri->segment(4))) { foreach($greeting as $row){?>



<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Edit Greeting</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crudgreetingdeveloper/update/<?php echo $this->uri->segment(4)?>" onsubmit="return Formgreeting(judul,status);">

					<div class="row">
						<div id="kiri" class="col-sm-70">



<table id="textField" class="table"  width="100%">
<tr>
  
<td><b>Judul</b></td>
<td><b>:</b></td>
<td>
   
<input id="judul" type="text" class="fieldStyle" name="judul" value="<?php echo $row['judul'];?>">
</td>
</tr>
<tr>
<td><b>Isi</b></td>
<td><b>:</b></td>
<td>
<textarea id="isier"  name="isi"  style="width:407px;height:200px;resize:none;"><?php echo $row['isi'];?></textarea>
</td>
</tr>



<tr >
<td><b>Kategori</b></td>
<td><b>:</b></td>
<td>
 <div id="seld">
	<input class="rdo" type="radio" name="kategori" id="satue" value="religi" <?php if($row['kategori'] == 'religi') echo "checked";?> /><div class="stse">Religi</div>
	<input class="rdo" type="radio" name="kategori" id="duae" value="budaya" <?php if($row['kategori'] == 'budaya') echo "checked";?>/><div class="stse">Budaya</div>	
	<input class="rdo" type="radio" name="kategori" id="tigae" value="nasionalisme" <?php if($row['kategori'] == 'nasionalisme') echo "checked";?>/><div class="stse">Nasionalisme</div>
	<input class="rdo" type="radio" name="kategori" id="empate" value="brithday" <?php if($row['kategori'] == 'brithday') echo "checked";?>><div class="stse">Brithday</div>
 </div>
 </td>
</tr>



<script>
$(document).ready(function(){
	$("#satue").click(function(){
		$("#agamas").fadeIn();
		$("#tanggals").fadeIn();	
	});

	$("#duae").click(function(){
		$("#agamas").fadeOut();
		$("#tanggals").fadeIn();
	});
	
	$("#tigae").click(function(){
		$("#agamas").fadeOut();
		$("#tanggals").fadeIn();
	});
	
	

	$("#empate").click(function(){
		$("#agamas").fadeOut();
		$("#tanggals").fadeOut();
			
	});
	
	
	
});
</script>


<tr id="agamas" style="display:none">
<td><b>Hari Keagamaan</b></td>
<td><b>:</b></td>
<td>
 <div id="seld">
 
  <select name="agama" class="selectpicker" data-style="btn-primary">
  
	<?php if($row['agama'] == ""){ ?>
		<option value="" selected>Pilih Agama</option>
		
		<option value="islam">- Islam -</option>
		<option value="nasrani">- Nasrani -</option>
		<option value="budha">- Budha -</option>
		<option value="hindu">- Hindu -</option>
		<option value="konghucu">- Konghucu -</option>
		
		
	<?php }else if ($row['agama'] == "islam"){	?>
	
		<option value="" >Pilih Agama</option>
		<option value="islam" selected>- Islam -</option>
		<option value="nasrani">- Nasrani -</option>
		<option value="budha">- Budha -</option>
		<option value="hindu">- Hindu -</option>
		<option value="konghucu">- Konghucu -</option>
		
		
	<?php }else if ($row['agama'] == "nasrani"){?>	
	
		<option value="" >Pilih Agama</option>
		<option value="islam" >- Islam -</option>
		<option value="nasrani" selected>- Nasrani -</option>
		<option value="budha">- Budha -</option>
		<option value="hindu">- Hindu -</option>
		<option value="konghucu">- Konghucu -</option>
	
		
	<?php }else if ($row['agama'] == "budha"){	?>	
	
		<option value="" >Pilih Agama</option>
		<option value="islam" >- Islam -</option>
		<option value="nasrani" >- Nasrani -</option>
		<option value="budha" selected>- Budha -</option>
		<option value="hindu">- Hindu -</option>
		<option value="konghucu">- Konghucu -</option>
		
		
	<?php }else if ($row['agama'] == "hindu"){	?>
	
		<option value="" >Pilih Agama</option>
		<option value="islam" >- Islam -</option>
		<option value="nasrani" >- Nasrani -</option>
		<option value="budha" >- Budha -</option>
		<option value="hindu" selected>- Hindu -</option>
		<option value="konghucu">- Konghucu -</option>
		
	
	<?php }else if ($row['agama'] == "konghucu"){	?>

		<option value="" >Pilih Agama</option>
		<option value="islam" >- Islam -</option>
		<option value="nasrani" >- Nasrani -</option>
		<option value="budha" >- Budha -</option>
		<option value="hindu" >- Hindu -</option>
		<option value="konghucu" selected>- Konghucu -</option>
		

	<?php } else{  ?>
	
		<option value="" >Pilih Agama</option>
		<option value="islam" >- Islam -</option>
		<option value="nasrani" >- Nasrani -</option>
		<option value="budha" >- Budha -</option>
		<option value="hindu" >- Hindu -</option>
		<option value="konghucu" >- Konghucu -</option>
		<option value="lainnya" selected>- Lainnya -</option>
	
	
	<?php } ?>
  </select>
 </div>
 </td>
</tr>


<tr id="tanggals" style="display:none">
<td><b>Tanggal Greeting</b></td>
<td><b>:</b></td>
<td>
 <input type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd"  name="tgl_greeting" value="<?php echo $row['tgl_greeting'];?>">   
 </td>
</tr>

<tr>
<td><b>Foto</b></td>
<td><b>:</b></td>
<td>
<input id="file" type="file" name='userfile' >
</td>
</tr>
<tr>
<td></td>
<td><b>:</b></td>
<td>
<?php if($row['photo'] ==""){?>
<img width="100" height="100" style="border:1px solid #ddd;" src="<?php echo base_url();?>assets/images/no-image.jpg">
<?php }else { ?>
<img width="100" height="100" style="border:1px solid #ddd;" src="<?php echo base_url();?>assets/upload/greeting/<?php echo $row['photo'];?>">
<?php }?>
</td>
</tr>
<tr>
    <td><b>Status</b></td>
    <td ><b>:</b></td>
    <td><div id="seld">
 <input class="rdo" value="n" name="status" type="radio" <?php if($row['status'] == 'n') echo "checked";?> /> <div class="stse">Tidak Aktif</div>
<input class="rdo" value="y" name="status" type="radio" <?php if($row['status'] == 'y') echo "checked";?> /> <div class="stse">Aktif</div>
</div>
    </td>
</tr>

<tr>
<td></td>
<td><input type="hidden" name="gbr" value="<?php echo $row['photo'];?>"></td>
<td></td>
</tr>
</table>


							
							
							
							
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- edit berita -->
<?php }}  ?>
