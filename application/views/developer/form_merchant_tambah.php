
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Input Sekolah</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crud_developer/insert" onsubmit="return FormSekolahinsert(nama_sekolah,email,telp,id_paket,status);">

					<div class="row">
						<div id="kiri" class="col-sm-70">

<table id="textField" class="table"  width="100%">
<tr>  
<td><b>Nama Sekolah</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="nama_sekolah">
</td>
</tr>
<tr>
<td><b>Email</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="email" value="">
</td>
</tr>
<tr>
<td><b>Telp</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="telp" value="">
</td>
</tr>

<tr>
<td><b>Paket</b></td>
<td><b>:</b></td>
<td>
<div id="seld">
<select name="id_paket" class="selectpicker">
<option value="">-Pilih Paket-</option>
<option value="bronze">-Bronze-</option>
<option value="silver">-Silver-</option>
<option value="platinum">-Platinum-</option>
<option value="costum">-Costum-</option>
</select>
</div>
</td>
</tr>




<tr>
<td><b>Alamat</b></td>
<td><b>:</b></td>
<td>
<textarea style="width:407px;height:200px;resize:none;" name="alamat"></textarea>
</td>
</tr>

<tr>  
<td><b>Api Key</b></td>
<td><b>:</b></td>
<td>
<input id="api_key" type="text" class="fieldStyle" name="api_key">
</td>
</tr>

<tr>
<td><b>Logo</td>
<td><b>:</b></td>
<td>
<input id="file" type="file" name='userfile' >
</td>
</tr>


<tr>
    <td><b>Status</b></td>
    <td ><b>:</b></td>
    <td><div id="seld">
 <input id="status1" class="rdo" value="n" name="status" type="radio" /> <div class="stse">Tidak Aktif</div>
  <input  id="status2" class="rdo" value="y" name="status" type="radio" /> <div class="stse">Aktif</div>
  </div>
    </td>
</tr>

</table>

						
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




