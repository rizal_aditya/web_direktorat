<?php if(!empty($this->uri->segment(4))) { foreach($merchant as $row){?>



<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Edit Sekolah</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crud_developer/update/<?php echo $this->uri->segment(4)?>" onsubmit="return FormSekolah(nama_sekolah,telp,id_paket,status);">


					<div class="row">
						<div id="kiri" class="col-sm-70">

<input type="hidden" name="gbr" value="<?php echo $row['photo'];?>" >

<table id="textField" class="table"  width="100%">
<tr>
  
<td><b>Nama Sekolah</b></td>
<td><b>:</b></td>
<td>
   
<input  type="text" class="fieldStyle" name="nama_sekolah" value="<?php echo $row['nama_merchant'];?>">
</td>
</tr>



<tr>
<td><b>Telp</b></td>
<td><b>:</b></td>
<td> 
<input  type="text" class="fieldStyle" name="telp" value="<?php echo $row['telp'];?>">
</td>
</tr>
<tr>
<td><b>Paket</b></td>
<td><b>:</b></td>
<td>
<div id="seld">
<select name="id_paket" class="selectpicker">
<?php if($row['id_paket'] == "bronze"){?>
<option value="">-Pilih Paket-</option>
<option value="bronze" selected>-Bronze-</option>
<option value="silver">-Silver-</option>
<option value="platinum">-Platinum-</option>
<option value="costum">-Costum-</option>
<?php }else if($row['id_paket'] == "silver") { ?>
<option value="">-Pilih Paket-</option>
<option value="bronze">-Bronze-</option>
<option value="silver" selected>-Silver-</option>
<option value="platinum">-Platinum-</option>
<option value="costum">-Costum-</option>
<?php }else if($row['id_paket'] == "platinum") { ?>
<option value="">-Pilih Paket-</option>
<option value="bronze">-Bronze-</option>
<option value="silver">-Silver-</option>
<option value="platinum" selected>-Platinum-</option>
<option value="costum">-Costum-</option>
<?php }else if($row['id_paket'] == "costum") { ?>
<option value="">-Pilih Paket-</option>
<option value="bronze">-Bronze-</option>
<option value="silver">-Silver-</option>
<option value="platinum" >-Platinum-</option>
<option value="costum" selected>-Costum-</option>
<?php }else{ ?>
<option value="">-Pilih Paket-</option>
<option value="bronze">-Bronze-</option>
<option value="silver">-Silver-</option>
<option value="platinum">-Platinum-</option>
<option value="costum">-Costum-</option>
<?php } ?>

</select>
</div>
</td>
</tr>
<tr>
<td><b>Alamat</b></td>
<td><b>:</b></td>
<td>
<textarea style="width:407px;height:200px;resize:none;" name="alamat"><?php echo $row['alamat'];?></textarea>
</td>
</tr>

<tr>
<td><b>Api Key</b></td>
<td><b>:</b></td>
<td>
<input  type="text" class="fieldStyle" name="api_key" value="<?php echo $row['api_key'];?>">
</td>
</tr>

<tr>
<td><b>Logo</b></td>
<td><b>:</b></td>
<td>
<input id="file" type="file" name='userfile' >
</td>
</tr>
<tr>
<td></td>
<td><b>:</b></td>
<td>
<?php if($row['photo'] ==""){?>
<img width="100" height="100" style="border:1px solid #ddd;" src="<?php echo base_url();?>assets/images/no-image.jpg">
<?php }else { ?>
<img width="100" height="100" style="border:1px solid #ddd;" src="<?php echo base_url();?>assets/upload/merchant/<?php echo $row['photo'];?>">
<?php }?>

</td>
</tr>

<tr>
    <td><b>Status</b></td>
    <td ><b>:</b></td>
    <td><div id="seld">
 <input class="rdo" value="n" name="status" type="radio" <?php if($row['active'] == 'n') echo "checked";?> /> <div class="stse">Tidak Aktif</div>
<input class="rdo" value="y" name="status" type="radio" <?php if($row['active'] == 'y') echo "checked";?> /> <div class="stse">Aktif</div>
</div>
    </td>
</tr>
</table>
							
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- edit berita -->
<?php }}  ?>
