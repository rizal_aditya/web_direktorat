
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Form Input Greeting</h5>
			</div>
			<div class="widget-body">
				<div class="widget-main no-padding" >
												
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>crudgreetingdeveloper/insert" onsubmit="return Formgreeting(judul,status);">

					<div class="row">
						<div id="kiri" class="col-sm-70">

<table id="textField" class="table"  width="100%">
<tr>
  
<td><b>Judul</b></td>
<td><b>:</b></td>
<td>
   
<input id="judul" type="text" class="fieldStyle" name="judul">
</td>
</tr>
<tr>
<td><b>Isi</b></td>
<td><b>:</b></td>
<td>
<textarea id="isier" style="width:407px;height:200px;resize:none;" name="isi"></textarea>
</td>
</tr>

<tr>
<td><b>Kategori</b></td>
<td><b>:</b></td>
<td>
 <div id="seld">
	<input class="rdo" type="radio" name="kategori" id="satu" value="religi"><div class="stse">Religi</div>
	<input class="rdo" type="radio" name="kategori" id="dua" value="budaya"><div class="stse">Budaya</div>
	<input class="rdo" type="radio" name="kategori" id="tiga" value="nasionalisme"><div class="stse">Nasionalisme</div>
	<input class="rdo" type="radio" name="kategori" id="empat" value="brithday"><div class="stse">Brithday</div>
 </div>
 </td>
</tr>

<script>
$(document).ready(function(){
	$("#satu").click(function(){
		$("#agama").fadeIn();
		$("#tanggal").fadeIn();	
	});

	$("#dua").click(function(){
		$("#agama").fadeOut();
		$("#tanggal").fadeIn();
	});
	
	$("#tiga").click(function(){
		$("#agama").fadeOut();
		$("#tanggal").fadeIn();
	});
	
	

	$("#empat").click(function(){
				$("#agama").fadeOut();
				$("#tanggal").fadeOut();
			
	});
	
	
	
});
</script>

<tr  id="agama" style="display:none">
<td><b>Hari Keagamaan</b></td>
<td><b>:</b></td>
<td>
 <div id="seld">
  <select name="agama" class="selectpicker" data-style="btn-primary">
		<option value="0">Pilih Agama</option>
		<option value="islam">- Islam -</option>
		<option value="nasrani">- Nasrani -</option>
		<option value="budha">- Budha -</option>
		<option value="hindu">- Hindu -</option>
		<option value="konghucu">- Konghucu -</option>
  </select>
 </div>
 </td>
</tr>




<tr id="tanggal" style="display:none">
<td><b>Tanggal Greeting</b></td>
<td><b>:</b></td>
<td>
 <input  type="text" class="form-control date-picker fieldStyle" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd"  name="tgl_greeting">   
 </td>
</tr>

<tr>
<td><b>Foto</b></td>
<td><b>:</b></td>
<td>
<input id="file" type="file" name='userfile' >
</td>
</tr>
<tr>
    <td><b>Status</b></td>
    <td ><b>:</b></td>
    <td><div id="seld">
 <input id="status1" class="rdo" value="n" name="status" type="radio" /> <div class="stse">Tidak Aktif</div>
  <input  id="status2" class="rdo" value="y" name="status" type="radio" /> <div class="stse">Aktif</div>
  </div>
    </td>
</tr>

</table>

						
						</div>
						
					</div>
						<div class="form-actions left">
							<button type="submit" class="btn btn-sm btn-success">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




