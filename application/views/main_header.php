

	<body class="no-skin">
		<div id="navbar" class="btn-primary navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
				<?php if(!empty($_SESSION['merchant'])){ ?>
				<a href="<?=base_url();?>merchant" class="navbar-brand"><img src="<?php echo base_url();?>assets/images/vshop.png" height="30" width="140"></a>
				<?php }else if(!empty($_SESSION['instansi'])){ ?>
				<a href="<?=base_url();?>instansi" class="navbar-brand"><img src="<?php echo base_url();?>assets/images/vshop.png" height="30" width="140"></a>
				<?php }else if(!empty($_SESSION['direktorat'])){ ?>
					<a href="<?=base_url();?>direktorat" class="navbar-brand"><img src="<?php echo base_url();?>assets/images/vshop.png" height="30" width="140"></a>
				<?php }else if(!empty($_SESSION['developer'])){ ?>
				<a href="<?=base_url();?>developer" class="navbar-brand"><img src="<?php echo base_url();?>assets/images/vshop.png" height="30" width="140"></a>
				<?php } ?>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<!-- <li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell icon-animated-bell"></i>
								<span class="badge badge-important">1</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header"><i class="ace-icon fa fa-exclamation-triangle"></i>1 Pemberitahuan</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">
										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-success fa fa-user"></i>
														Member baru
													</span>
													<span class="pull-right badge badge-success">+2</span>
												</div>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown-footer"><a href="#">Lihat semua pemberitahuan<i class="ace-icon fa fa-arrow-right"></i></a></li> 
							</ul>
						</li>

						<li class="grey">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-envelope icon-animated-vertical"></i>
								<span class="badge badge-success">1</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-envelope-o"></i>
									1 Pesan
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar">
										<li>
											<a href="#" class="clearfix">
												<img src="assets/avatars/avatar.png" class="msg-photo" alt="Alex's Avatar" />
												<span class="msg-body">
													<span class="msg-title">
														<span class="green">Ahmad:</span>
														Pesan akan muncul disini ...
													</span>

													<span class="msg-time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span>13 menit yang lalu</span>
													</span>
												</span>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown-footer">
									<a href="inbox.html">
										See all messages
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>-->

						<?php  if(!empty($_SESSION['direktorat'])){ ?>
						
						<li class="blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							
								

								<?php

								if(empty($photo)){

								echo"<img width='25' height='25' class='nav-user-photo' src='".base_url()."assets/avatars/avatar.png' alt='".$nama_merchant."'>"; 			
								}else{
								 echo"<img width='25' height='25' class='nav-user-photo' src='".base_url()."assets/upload/merchant/".$photo."' alt='".$nama_merchant."'>"; 
								}
								 ?>
						
								<span class="user-info">
									<small>Selamat Datang,</small>
									<?php echo $nama_merchant; ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li><a href="<?php echo base_url();?>direktorat/profile"><i class="ace-icon fa fa-user"></i>Profil</a></li>
								<li><a href="<?php echo base_url();?>direktorat/ubah_password"><i class="ace-icon fa fa-key"></i>Ubah Password</a></li>
								
								<li class="divider"></li>
								<li><a href="<?=base_url();?>direktorat/logout/"><i class="ace-icon fa fa-power-off"></i>Keluar</a></li>
							</ul>
						</li>
						
						
						<?php }else if(!empty($_SESSION['developer'])){ ?>
						
						
						
							<li class="blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							
								

								<?php

								if(empty($photo)){

								echo"<img class='nav-user-photo' src='".base_url()."assets/avatars/avatar.png' alt='".$user_name."'>"; 			
								}else{
								 echo"<img class='nav-user-photo' src='".base_url()."assets/upload/admin/".$photo."' alt='".$user_name."'>"; 
								}
								 ?>
						
								<span class="user-info">
									<small>Selamat Datang,</small>
									<?php echo $user_name; ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
							
								<li class="divider"></li>
								<li><a href="<?=base_url();?>developer/logout/"><i class="ace-icon fa fa-power-off"></i>Keluar</a></li>
							</ul>
						</li>
						
						<?php } ?>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>
