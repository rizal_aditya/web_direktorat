
<!DOCTYPE html>
<html>
<head>
<title>Tabel Agenda | Direktorat</title>
<meta charset='utf-8' />
<link href='<?=base_url();?>assets/tabel_agenda/fullcalendar.min.css' rel='stylesheet' />

 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href='<?=base_url();?>assets/tabel_agenda/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-select.min.css" />
		<script src='<?=base_url();?>assets/tabel_agenda/lib/moment.min.js'></script>
		<script src='<?=base_url();?>assets/tabel_agenda/lib/jquery.min.js'></script>
		<script src='<?=base_url();?>assets/tabel_agenda/fullcalendar.min.js'></script>
		<script src="<?=base_url();?>assets/js/bootstrap-select.min.js"></script>
		
<script>

	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,listWeek'
			},
			defaultDate: '<?php echo date("Y-m-d");?>',
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: 
			<?php echo json_encode($agenda); ?>
				
			
		});
		
	});
$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 4
});

function cariUnit(k){ 
    $(document).ready(function(){
            
      $('#form').change( function() {
      $('#form')[0].submit();
      parent.updating();
         
      });

    });
}
</script>
<style>

	body {
		margin: 40px 10px 50px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto 40px;
	}

</style>
<style>
.bg-1 {
    background-color: #1abc9c; /* Green */
    color: #ffffff;
	
}
.bg-2 {
    background-color: #474e5d; /* Dark Blue */
    color: #ffffff;
}
.bg-3 {
    background-color: #ffffff; /* White */
    color: #555555;
}
.cabang{
	margin: 0px auto;
width: 900px;
}

.cabang .stl{
float: left;
margin: -42px 0px 0px;
}

.cabang .der{
float: right;
margin: -42px 0px 0px;
padding: 6px 8px;
border-radius: 3px;
}

.cabang .cr{
position: relative;
top: 2px;
padding: 6px 8px;
border-radius: 3px;	
}

.cabang .der:hover{
	text-decoration:none;
	background-color: #ccc;
background-image: none;
box-shadow: inset 0 2px 4px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05);
color: #333;
font-size: 1em;
white-space: nowrap;
cursor: pointer;
}

.btn-default {
border: 1px solid;
background-color: #f5f5f5;
background-image: linear-gradient(to bottom,#fff,#e6e6e6);
background-repeat: repeat-x;
border-color: rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.25);
color: #333;
text-shadow: 0 1px 1px rgba(255,255,255,.75);
box-shadow: inset 0 1px 0 rgba(255,255,255,.2),0 1px 2px rgba(0,0,0,.05);
}
</style>
</head>
<body>
<div class="container-fluid bg-3 text-center">
 
  <h3>Tabel Agenda</h3>
	<div class="cabang">
	
	<form id="form_id" class="stl" method="post">
		<select id="id_unit" name="id_unit" class="cabang selectpicker" onchange="cariUnit(this.value);">
			<option value="">Pilih Unit</option>
			<?php foreach($divisi as $row){?>
					<?php if($id_unit == $row['id_cabang']){ ?>
						<option value="<?php echo $row['id_cabang'];?>" selected><?php echo $row['nama_cabang'];?></option>
					<?php }else{ ?>
						<option value="<?php echo $row['id_cabang'];?>"><?php echo $row['nama_cabang'];?></option>
					<?php } ?>
			
			<?php } ?>
		</select>
		
		<button class="cr fc-agendaDay-button fc-button fc-state-default" type="submit" name="cari" >Cari </button>
	
	</form>
	
	<a class="der fc-agendaDay-button fc-button fc-state-default" href="<?=base_url();?>direktorat/agenda">Kembali</a>
	
	</div>
	<div id='calendar'></div>
</div>
<script>


</script>
</body>
</html>