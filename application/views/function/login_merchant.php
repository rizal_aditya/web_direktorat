<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Sekretariat Direktorat Jendral Kebudayaan</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link href="<?php echo base_url();?>/assets/images/favicone.png" rel="shortcut icon">

		<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/font-awesome/4.2.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="http://kartuvirtual.com/assets/font-awesome/4.2.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/fonts/fonts.googleapis.com.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/ace.min.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/editor.css" />
		<link rel="stylesheet" href="<?=base_url();?>assets/css/ace-rtl.min.css" />
		<script src="<?=base_url();?>assets/js/jquery.2.1.1.min.js"></script>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?=base_url();?>assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?=base_url();?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});
		</script>
	</head>

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
			<div class="header_login" >
				<div class="logo">
					<img src="<?=base_url();?>assets/css/images/logo_depan.png" />
				</div>
								
								
			</div>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
								
								
								<div class="login-container">
							
								<!--<h1>
									<i class="ace-icon fa fa-graduation-cap blue"></i>
									<span class="blue">Direktorat</span>
									<span class="blue" id="id-text2">Kementerian Pendidikan</span>
								</h1>
								<h4 class="blue" id="id-company-text"></h4> -->
								
								
							
							
							

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-user blue"></i>
												Masukkan informasi akun
											</h4>

											<div class="space-6"></div>

											<form method="post" action="<?=base_url();?>login/masuk">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Email" name="user_login" />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Kata Sandi" name="user_pass"  />
															<input type="hidden" name="kategori" value="merchant">
															<input type="hidden" name="u_init" value="login">
															
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>
															
															
															
													<div class="space"></div>

													<div class="clearfix">

														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Masuk</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar clearfix">
											<div>
												<a href="<?=base_url();?>#" data-target="#forgot-box" class="forgot-password-link">
													<i class="ace-icon fa fa-arrow-left"></i>
													Lupa Kata Sandi
												</a>
											</div>

										<!--<div>
												<a href="<?=base_url();?>#" data-target="#signup-box" class="user-signup-link">
													Daftar Baru
													<i class="ace-icon fa fa-arrow-right"></i>
												</a>
											</div> -->
										</div> 
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

								<div id="forgot-box" class="forgot-box widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="ace-icon fa fa-key"></i>
												Setel Ulang Kata Sandi
											</h4>

											<div class="space-6"></div>
											<p>
												Masukkan Email akun dan kami akan mengirim ulang kata sandi anda
											</p>

											<form method="post" action="<?=base_url();?>merchant/lupa_password">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" class="form-control" placeholder="Email" name="email" />
															<i class="ace-icon fa fa-envelope"></i>
														</span>
													</label>

													<div class="clearfix">
														<button type="button" class="width-35 pull-right btn btn-sm btn-danger">
															<i class="ace-icon fa fa-lightbulb-o"></i>
															<span class="bigger-110">Kirim</span>
														</button>
													</div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar center">
											<a href="<?=base_url();?>#" data-target="#login-box" class="back-to-login-link">
												Kembali
												<i class="ace-icon fa fa-arrow-right"></i>
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.forgot-box -->

								<div id="signup-box" class="signup-box widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger">
												<i class="ace-icon fa fa-users blue"></i>
												Registrasi Toko Baru
											</h4>

											<div class="space-6"></div>
											<p> Masukkan data akun toko : </p>

											<form>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" class="form-control" placeholder="Email" />
															<i class="ace-icon fa fa-envelope"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Nama Toko" />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Kata Sandi" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Ulangi Kata Sandi" />
															<i class="ace-icon fa fa-retweet"></i>
														</span>
													</label>

													<label class="block">
														<input type="checkbox" class="ace" />
														<span class="lbl">
															Saya Setuju
															<a href="<?=base_url();?>#">atas Persetujuan dan Kebijakan</a>
														</span>
													</label>

													<div class="space-24"></div>

													<div class="clearfix">
														<button type="reset" class="width-30 pull-left btn btn-sm">
															<i class="ace-icon fa fa-refresh"></i>
															<span class="bigger-110">Ulang</span>
														</button>

														<button type="button" class="width-65 pull-right btn btn-sm btn-success">
															<span class="bigger-110">Daftar</span>

															<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
														</button>
													</div>
												</fieldset>
											</form>
										</div>

										<div class="toolbar center">
											<a href="<?=base_url();?>#" data-target="#login-box" class="back-to-login-link">
												<i class="ace-icon fa fa-arrow-left"></i>
												Kembali
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.signup-box -->
							</div><!-- /.position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->
	</body>
</html>
