<?php

    function rp($rp){
        return number_format($rp,0,'','.');
    }

    function cari($kolom,$tabel,$pk,$key){
        $ci=& get_instance();
        $ci->load->database(); 
        $no = $ci->db->query("SELECT ".$kolom." as hasil FROM ".$tabel." where ".$pk."='".$key."' ");
        return($no->first_row()->hasil);
    }

    function no($kolom,$tabel){
        $ci=& get_instance();
        $ci->load->database(); 
        $no = $ci->db->query("SELECT ".$kolom." as hasil FROM ".$tabel." order by ".$kolom." desc limit 1");
        if($no->num_rows()==null){
            $no = 1;    
        }
        else{
            $no = $no->first_row()->hasil+1;
        }
        return($no);
    }

    function tgl($tgl){
        return date('d-M-Y',strtotime($tgl));
    }

    function tanggal($tgl){
        return date('Y-m-d',strtotime($tgl));
    }

    function ket($x){
        if($x == 'y'){
            $ket = "Aktif";
        }
        else{
            $ket = "Non Aktif";
        }
        return $ket;
    }

?>